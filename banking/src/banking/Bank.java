package banking;

import java.util.ArrayList;

public class Bank {
	
	private String Name;
	
	private ArrayList<Branch> bankBranches = new ArrayList<Branch>();
	
	
	public Bank(String a ) {
		
		this.Name = a;
		
		
	}
	
	public Bank() {
		
		this ("Default name");
	}
	
	
	public void addBranch (String branchName) {
		
		Branch a = new Branch(branchName);
		
		bankBranches.add(a);
		
	}
	
	public Branch getBranch(int i) {
		
		return bankBranches.get(i);
		
	}
	
	public void printBranches() {
		
		for (int i = 0 ; i < bankBranches.size() ; i ++) {
			System.out.println(bankBranches.get(i).getBranchName());
		}
	}
	
	

}
