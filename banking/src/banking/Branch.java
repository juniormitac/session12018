package banking;

import java.util.ArrayList;

public class Branch {
	
	
	private ArrayList<Customer> myCustomers = new ArrayList<Customer>();
	
	private String branchName;
	
	
	
	public Branch ( String a ) {
		
		this.branchName = a;
	}
	
	
	public void addCustomer (String name, double initial) {
		
		myCustomers.add(new Customer(name, initial));
	}
	
	public String getBranchName () {
		return branchName;
	}
	
	
	public void printCustomer() {
		
		for(int i = 0 ; i < myCustomers.size() ; i ++) {
			
			System.out.println(myCustomers.get(i).getName());
		}
		
	}
	
	
	

}
