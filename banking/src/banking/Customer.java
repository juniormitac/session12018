package banking;

import java.util.ArrayList;


public class Customer {
	
	private String name;
	private String customerID;
	
	private ArrayList<Double> transactions = new ArrayList <Double>();
	
	
	
	
	public Customer ( String name , Double initial ) {
		
		this.name = name ;
		transactions.add(initial);
		
		
	}
	
	
	public String getName () {
		
		return name;
	}
	
	public String getcustomerID() {
		return customerID;
	}
	
	public void addTransaction(double a) {
		
		transactions.add(a);
	}
	
	
	public void  printTransactions () {
		
		for (int i =0 ; i < transactions.size() ; i ++) {
			
			System.out.println("Transactions for customer " + getName());
			System.out.println(transactions.get(i));
		}
		
	}
	

}
