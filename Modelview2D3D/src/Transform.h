/*
 * Tranform.h
 *
 *  Created on: 12 Mar 2015
 *      Author: Len Hamey
 */

#ifndef TRANSFORM_H_
#define TRANSFORM_H_


#include "Table.h"
#include "HomoMatrix.h"
#include <string>

// Specify 2D or 3D mode.
# define THREE_D 0
// Specify whether the full house is drawn or a simplified version for quiz sketches
# define QUIZ_SKETCH 0
// Specify whether new operations are pushed at the end (CG mode) or beginning (math mode)
# define PUSH_END 1

// Event types handled by event methods
# define MOUSE_CLICK 1
# define MOUSE_MOTION 2
# define MOUSE_PASSIVE 3
# define KEYBOARD_DOWN 4
# define SPECIAL_DOWN 5
# define MENU_ITEM 6
# define TIMER_EVENT 7

// The menu events
# define MENU_TRANSLATE 1
# define MENU_ROTATE_X 2
# define MENU_ROTATE_Y 3
# define MENU_ROTATE_Z 4
# define MENU_SCALE 5
# define MENU_SHEAR 6
# define MENU_MODELVIEW 7
# define MENU_EQUATION 10
# define MENU_HOMOGENEOUS 11
# define MENU_VECTOR 12
# define MENU_HOMOGENEOUS_EQUATION 13
# define MENU_SHOW_NUMBERS 21
# define MENU_SHOW_SYMBOLS 22
# define MENU_SHOW_DETAIL_SYMBOLS 23
# define MENU_ZOOM_IN 31
# define MENU_ZOOM_OUT 32
# define MENU_ANIMATE 33
# define MENU_ANIMATE_OSCILLATE 34
# define MENU_SHADOWS 35

# define MENU_RESET 100
# define MENU_RESET_ALL 101
# define MENU_UP 102
# define MENU_DOWN 103
# define MENU_DELETE 104
# define MENU_DELETE_ALL 105
# define MENU_COMPACT 106
# define MENU_EXPANDED 107
// Direction of constructing combined transformation
# define MENU_LEFT_TO_RIGHT 120
# define MENU_RIGHT_TO_LEFT 121

# define MENU_TARGET_NONE 200
# define MENU_TARGET_1 201
# define MENU_TARGET_2 202
# define MENU_TARGET_3 203
# define MENU_TARGET_4 204
# define MENU_TARGET_5 205
# define MENU_TARGET_6 206
# define MENU_TARGET_7 207
# define MENU_TARGET_8 208
# define MENU_TARGET_9 209
# define MENU_TARGET_11 211
# define MENU_TARGET_12 212
# define MENU_TARGET_13 213
# define MENU_TARGET_14 214

# define MENU_TARGET_FIRST 200
# define MENU_TARGET_LAST 214


# define PI 3.1415926535897932

// A class that represents a slider widget for changing a number.
// There is an assumption that the world coordinates are in 1:1 mapping to viewport
// so that the scale of text works out properly.
class Slider
{
public:
	Slider();
	// Create a slider at world position x,y with width specified.
	// minv,maxv are the range of values and value is the initial value.
	Slider(std::string label, double minv, double maxv, double tickgap, double step, double value);

	// Init the slider object (for use in constructors)
	void init(void);
	// Init an existing slider object (useful for embedded sliders)
	void init(std::string label, double minv, double maxv, double tickgap, double step, double value);

	// Set the label string.
	void setLabel(std::string _label) { label = _label; return; }

	// Respond to events. Issues glutPostRedisplay if the slider is changed.
	// Call event() when the mouse button is pressed, released or motion occurs.
	// Coordinates must be the same as the drawing coordinates for the slider world.
	// Returns true if the slider responded to the event.
	// Responds to the left mouse button when it is pressed within the region occupied by the slider.
	bool event(int event, int button, int state, double x, double y);

	// Layout the slider
	double layout(double x, double y, double w);

	// Display the slider
	void draw();

	// Return the value of the slider.
	double value(void) { return currentValue; }

	// Reset the slider to its initial value.
	void reset();

private:
	double left, right, top, bottom;
	double barLeft, barRight, barTop, barBottom; // The actual bar region
	double textBaseline; // The y value of the text label baseline
	double minValue, maxValue, tickGap, valueStep;
	double currentValue, initialValue;

	bool mouseResponding; // Track whether slider is responding to current event events or not.

	std::string mintext, label, maxtext; // Items of label text
};

// Identify constants must be small so we can use an array for counting them.
# define IDENTIFY_FIRST 1
# define IDENTIFY_TRANSLATE 1
# define IDENTIFY_ROTATE_X 2
# define IDENTIFY_ROTATE_Y 3
# define IDENTIFY_ROTATE_Z 4
# define IDENTIFY_SCALE 5
# define IDENTIFY_SHEAR 6
# define IDENTIFY_MODELVIEW 7
# define IDENTIFY_VIEW 8
# define IDENTIFY_LAST 8

// Define a base class and derived classes that represent transformations
// that can be applied to the modelview matrix in 2D
// Positions of widgets are their top-left corners.
class Transform
{
public:
	Transform();
	virtual ~Transform();

	// Apply the transform: call OpenGL to update the current matrix
	virtual void apply(void) = 0;

	// Respond to the mouse - mouse coordinates are already world coordinates
	virtual bool event(int event, int button, int state, double x, double y);

	// Layout the Transform - compute dimensions and layout of content
	virtual double layout(int mode, double x, double y, double w);

	// Draw the Transform
	virtual void draw(void);

	// Is this item currently selected (by passive mouse over)?
	bool selected(void) { return isSelected; }

	// Identify
	virtual int identify(void) = 0;

	// Use by TransformList to set whether this is the last thing in the list or not
	// The Modelview widget needs this information to correctly label LHS of equations.
	void setLast(bool last) { isLast = last; return; }

	// Return a name string for the transformation (used to construct the transformation
	// string)
	virtual std::string name(int mode) = 0;

	// Set a subscript string - used to apply subscript numbers _1 _2 etc to
	// repeat transformations with the same identification
	void setSubscript(std::string sub) {
		if (sub == "") subscript = "";
		else subscript = "_"+sub;
		return;
	}

	// Set the current transformation string (used by Modelview widget)
	virtual void setTransformation(std::string trans);

protected:
	// Utility method to decide whether reset applies to this Transform
	bool resetApplies(int event, int button) {
		if (event == MENU_ITEM) {
			return (button == MENU_RESET && selected()) || button == MENU_RESET_ALL;
		}
		return false;
	}

	double left, right, top, bottom, textBaseline; // Position of the transform widget.
	bool isSelected; // Is this transform selected (by mouse hovering over it currently)

	// Is this the last transform in the list?
	// ModelView widget uses this information to decide whether to label the output vector
	// as world coordinates (subscript w) or as just transformed coordinates (x' y')
	bool isLast;
	std::string subscript; // subscript for the name, defaults to empty string

	// Remember the showMode specified in the layout call, so it can be used in draw.
	int showMode;

	// Remember whether the display is compact or not.
	bool compactMode;
};


# define TRANSLATE_STEP 0.5
# define TRANSLATE_MIN -4.0
# define TRANSLATE_MAX 4.0

class Translate : public Transform {
public:
	// A Translate Transform at position x, y on the screen.
	Translate();
	virtual void apply(void);
	virtual bool event(int event, int button, int state, double x, double y);
	virtual double layout(int mode, double x, double y, double w);
	virtual void draw(void);
	virtual std::string name(int mode);
	virtual int identify(void) { return IDENTIFY_TRANSLATE; }

protected:
	Slider sliderX;
	Slider sliderY;
	Slider sliderZ;
};

# define ROTATE_MIN -180.0
# define ROTATE_MAX 180.0
# define ROTATE_STEP 15.0

class Rotate : public Transform {
public:
	// A rotation about an axis X, Y or Z.  (Only use Z in 2D operation)
	Rotate(char axis);
	virtual void apply();
	virtual bool event(int event, int button, int state, double x, double y);
	virtual double layout(int mode, double x, double y, double w);
	virtual void draw();
	virtual std::string name(int mode);
	virtual int identify(void) { return IDENTIFY_ROTATE_X + axis - 'x'; }

protected:
	Slider sliderA;
	char axis; // Default is 'z'
};

# define SCALE_MIN -3.0
# define SCALE_MAX 3.0
# define SCALE_STEP 0.5

class Scale : public Transform {
public:
	// Scaling in X and Y independently.
	Scale();
	virtual void apply();
	virtual bool event(int event, int button, int state, double x, double y);
	virtual double layout(int mode, double x, double y, double w);
	virtual void draw();
	virtual std::string name(int mode);
	virtual int identify(void) { return IDENTIFY_SCALE; }

protected:
	Slider sliderSX;
	Slider sliderSY;
	Slider sliderSZ;
};


# define SHEAR_MIN -2.0
# define SHEAR_MAX 2.0
# define SHEAR_STEP 0.25

class Shear : public Transform {
public:
	// Shear along X only (since shear along Y is redundant).
	Shear();
	virtual void apply();
	virtual bool event(int event, int button, int state, double x, double y);
	virtual double layout(int mode, double x, double y, double w);
	virtual void draw();
	virtual std::string name(int mode);
	virtual int identify(void) { return IDENTIFY_SHEAR; }

protected:
	Slider sliderShX;
	Slider sliderShY;
};


// Modes for contentMode
# define CONTENT_EQUATIONS 1
# define CONTENT_VECTOR 2
# define CONTENT_HOMOGENEOUS_MATRIX 3
# define CONTENT_TRANSFORMATION 4

// Modes for showMode
# define SHOW_NUMBERS 1
# define SHOW_SYMBOLS 2

// Display the modelview matrix
class ModelView : public Transform {
public:
	ModelView();
	virtual void apply();
	virtual bool event(int event, int button, int state, double x, double y);
	virtual double layout(int mode, double x, double y, double w);
	virtual void draw(void);
	virtual std::string name(int mode);
	virtual void setTransformation(std::string trans);
	virtual int identify(void) { return IDENTIFY_MODELVIEW; }

private:
	double fullMatrix[4][4];
# if THREE_D
	double displayFullMatrix[4][4];
	double displayVectorMatrix[3][3]; // Omit W
# else
	double displayFullMatrix[3][3]; // Omit Z
	double displayVectorMatrix[2][2]; // Omit W and Z
# endif
	// validMatrix is initially false.
	// matrixChanged indicates that the table needs to be updated because the
	// matrix was changed in apply().
	bool validMatrix, matrixChanged;

	// Structure of the displayed content is:
	// lvec ltext   table    rtext rvec ctext cvec
	//----------
	// [ Xm ]  =  [ 0.5 1 ]        [ Xm ] +   [ 2 ]
	// [ Ym ]     [ 1   0 ]        [ Ym ] +   [ 0 ]
	//----------
	// [ Xm ] =   [ 0.5 1 2 ]      [ Xm ]
	// [ Ym ] =   [ 1   0 0 ]      [ Ym ]
	// [ Wm ] =   [ 0   0 1 ]      [ 1  ]
	Table table;
	double tableLeft, tableTop, tableWidth, tableHeight;

	Table lvec3, lworld3, lvec2, lworld2;
	bool isWorld;
	double lvecWidth, lvecHeight, lworldWidth, lworldHeight;
	double lvecworldLeft; // Left of either the lvec or the lworld

	Table rvec3, rvec2;
	double rvecLeft, rvecWidth, rvecHeight;

	Table cvec; // Constant vector that is added on in 2x2 matrix mode
	double cvecLeft, cvecWidth, cvecHeight;

	std::string ltext, rtext, ctext;
	double ltextWidth, rtextWidth, ctextWidth;
	double ltextLeft, rtextLeft, ctextLeft;
	double midBaseline;
	void *font;

	int contentMode; // What content is to be displayed.

	std::string transformation; // The transformation equation
	double transformationWidth, transformationHeight; // Width to display it equation or table.
	Table transformationTable;
};


# if THREE_D
// View: A camera viewpoint widget for use in 3D
class View : public Transform {
public:
	View();
	virtual void apply();
	virtual bool event(int event, int button, int state, double x, double y);
	virtual double layout(int mode, double x, double y, double w);
	virtual void draw(void);
	virtual std::string name(int mode);
	virtual int identify(void) { return IDENTIFY_VIEW; }

private:
	Slider sliderA, sliderE;
	double distance;
	bool animate;
	bool oscillate;
	double oscStep; // Steps through the oscillation
	double oscPos; // Current oscillation phase (0-360)
	double oscMax; // Maximum oscillation angle in degrees
	double animStep;
	double animRotate;
};
# endif


# define MAX_TRANSFORMERS 10

// A collection of transformations
class TransformList
{
private:
	bool push_at_end;
	void swap(Transform * &p1, Transform * &p2)
	{
		Transform *temp;
		temp = p1;
		p1 = p2;
		p2 = temp;
	}

public:
	TransformList(double _x, double _y, double _w, double _h);
	TransformList();

	double layout(double x, double y, double w);
	// Re-do the layout assuming the same top-left corner and width as previously
	// This facility is needed for events that change the collection of transforms.
	double relayout();
	void draw(void);
	bool event(int event, int button, int state, double x, double y);
	void apply(void);
	bool apply_selection(void);
	void apply_view(void);

	// In Computer Graphics texts, the sequence of operations
	// is often described starting from the object and moving out.
	// This is exactly the opposite of OpenGL call sequence,
	// because OpenGL post-multiplies the modelview matrix
	// by the new transformation (see glMultMatrix).
	void push(Transform *t) {
		if (n < MAX_TRANSFORMERS) {
            if (push_at_end) {
            	list[n++] = t;
    			// If it happens that the last thing was a matrix display
    			// then push the new thing above it.
    			if (n >= 2 && list[n-2]->identify() == IDENTIFY_MODELVIEW)
    				swap(list[n-2], list[n-1]);
            }
			else {
				for (int i = n-1; i >= 0; i--)
					list[i+1] = list[i];
				n++;
				list[0] = t;
    			// If it happens that the first thing was a camera view display
    			// then push the new thing below it.
    			if (n >= 2 && list[1]->identify() == IDENTIFY_VIEW)
    				swap(list[1], list[0]);
			}
		}
	}

	// pushEnd pushes on the end always - use it for modelview widget
	void pushEnd(Transform *t) {
		if (n < MAX_TRANSFORMERS) {
			list[n++] = t;
		}
	}

private:
	Transform *list[MAX_TRANSFORMERS];
	int n;
	double left, right, top, bottom;

	int showMode; // Display symbols vs numbers.
};
#endif /* TRANSFORM_H_ */
