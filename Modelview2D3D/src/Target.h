/*
 * Target.h
 *
 *  Created on: 16 Mar 2015
 *      Author: Len Hamey
 */

#ifndef TARGET_H_
#define TARGET_H_

// A simple class object that represents a target to be achieved.
class Target
{
public:
	Target();
	Target(double _tx, double _ty, double _a, double _sx, double _sy);

	// Return true if the target is identity transformation
	bool isIdentity(void);

	// Randomise the target to some type
	void random(int type);

	// Apply the transformation of the target.
	// It is relative to the identity transformation;
	// i.e. to draw the target, load the identity matrix
	// then apply the final transformation, then draw.
	void apply(void);

	// Apply the initial transformation
	void applyInit(void);

protected:
	// Generate a random double on range min,max in increments of step
	// omitting the subrange omin, omax
	double dran(double min, double max, double step, double omin, double omax);

	// Generate a random rotation.
	// In 2D: A random non-zero rotation
	// In 3D: No less than minaxes axes are rotated and no more than maxaxes.
	void ranRotate(int minaxes, int maxaxes);

	// Generate a pair of random initial and final translate positions
	// that have the properties:
	// Both initial and final are within slider range of the origin.
	// Initial is within slider range of the final.
	void randomTranslatePair(void);

	// Translate by tx, ty, tz
	// Rotate by ay, az, ax
	// Scale by sx, sy, sz
	double tx, ty, tz, ax, ay, az, sx, sy, sz;

	// Initial transformation
	double itx, ity, ia, isx, isy;
};



#endif /* TARGET_H_ */
