/*
 * Table.h
 *
 *  Created on: 16 Mar 2015
 *      Author: Len Hamey
 */

#ifndef TABLE_H_
#define TABLE_H_

#include <string>
#include <cassert>

class TableItem
{
public:
	std::string text;
	double width;
};

class Table
{
public:
	Table(void);
	Table(std::string text[], int nc, int nr);
	~Table();
	void init(int nc, int nr);
	void init(std::string text[], int nc, int nr);
	void init(double num[], int nc, int nr);

	// Compute layout. Return width w and height h.
	// The table is laid out with a margin around it.
	void layout(double &w, double &h);

	// The draw routine accepts coordinates so that the parent can
	// centre the table or place it wherever they choose.
	void draw(double x, double y);

protected:
	TableItem &item(int column, int row);
	void deleteContent(void);

private:
	// Only for use in constructors - any other use would lead to memory leak
	void init(void);

	TableItem *data;
	double *colWidth, *colPos;
	int numcols, numrows;

	void *font;
	double colGap;
	double margin;

	double topBaseline;
};



#endif /* TABLE_H_ */
