/*
 * polyStipple.cpp
 *
 *  Created on: 24 Mar 2015
 *      Author: Len Hamey
 */

# include <stdint.h>
# include <gl/glut.h>

// Set polygon stippling to simulate an opacity level.
// Supports 0.25, 0.5, 0.75, 1.0 (no stippling)
void polyStipple(float opacity, int phase)
{
	uint32_t pattern[32];
	uint32_t p[2];
	int ix, iy;
	ix = phase&1;
	iy = (phase>>1)&1;
	p[ix] = 0xAAAAAAAA;
	p[!ix] = 0x55555555;
	if (opacity < 0.3) {
		// 0.25 pattern
		p[iy] = 0;
		for (int i = 0; i < 32; i+=2) {
			pattern[i] = p[0];
			pattern[i+1] = p[1];
		}
	}
	else if (opacity < 0.7) {
		// 0.5 pattern
		for (int i = 0; i < 32; i+=2) {
			pattern[i] = p[0];
			pattern[i+1] = p[1];
		}
	}
	else if (opacity < 0.9) {
		// 0.75 pattern
		p[iy] = 0xFFFFFFFF;
		for (int i = 0; i < 32; i+=2) {
			pattern[i] = p[0];
			pattern[i+1] = p[1];
		}
	}
	if (opacity > 0.1 && opacity < 0.9) {
		glEnable(GL_POLYGON_STIPPLE);
		glPolygonStipple((unsigned char *) pattern);
	}
	else
		glDisable(GL_POLYGON_STIPPLE);
}



