/*
 * Table.cpp
 *
 *  Created on: 16 Mar 2015
 *      Author: Len Hamey
 */

# include "Table.h"
# include "FontHeight.h"
# include <gl/glut.h>
# include <cstdio>

Table::Table(void)
{
	init();
}

Table::Table(std::string text[], int nc, int nr)
{
	init();
	init(text, nc, nr);
}

Table::~Table(void)
{

	delete data;
	data = 0;
}

void Table::deleteContent(void)
{
	if (data) {
		delete data;
		data = 0;
	}
	if (colWidth) {
		delete colWidth;
		colWidth = 0;
	}
	if (colPos) {
		delete colPos;
		colPos = 0;
	}
}

void Table::init(void)
{
	data = 0;
	colWidth = 0;
	colPos = 0;
	numrows = numcols = 0;
	//font = GLUT_BITMAP_HELVETICA_18;
	font = GLUT_BITMAP_TIMES_ROMAN_24;
	colGap = textWidth(font, " ") * 1.5; // Space between columns of text.
	margin = textWidth(font, " ") * 0.75; // Margin around the table.
}

void Table::init(int nc, int nr)
{
	deleteContent();
	data = new TableItem[nc * nr];
	colWidth = new double[nc];
	colPos = new double[nc];
	numcols = nc;
	numrows = nr;
	for (int r = 0; r < nr; r++)
		for (int c = 0; c < nc; c++)
			item(c,r).text = "";
}

void Table::init(std::string text[], int nc, int nr)
{
	init(nc, nr);
	for (int r = 0; r < nr; r++)
		for (int c = 0; c < nc; c++)
			item(c,r).text = text[r*nc + c];
}

void Table::init(double num[], int nc, int nr)
{
	// Initialise table to text derived from numbers.
	init(nc, nr);
	for (int r = 0; r < nr; r++)
		for (int c = 0; c < nc; c++) {
			char buffer[100], buffer2[100];
			std::string s1, s2;
			sprintf (buffer, "%.2f", num[r*nc + c]);
			sprintf (buffer2, "%.2g", num[r*nc + c]);
			s1 = std::string(buffer);
			s2 = std::string(buffer2);
			// Near-zero values are messy with %g format
			if (s1 == "0.00" || s1 == "-0.00") s1 = "0";
			if (s2.length() < s1.length()) s1 = s2;
			item(c,r).text = s1;
		}
}

TableItem &Table::item(int column, int row)
{
	assert (column >= 0 && column < numcols && "Table::item: Column index out of range");
	assert (row >= 0 && row < numrows && "Table::item: Row index out of range");
	return data[row*numcols + column];
}

void Table::layout(double &w, double &h)
{
	topBaseline = 0.0 - fontAbove(font) - margin;

	// Compute the width of each string in the table.
	for (int r = 0; r < numrows; r++)
		for (int c = 0; c < numcols; c++)
			item(c,r).width = textWidth(font, item(c,r).text);

	// Compute the width of each column as the maximum width of any string in the column.
	for (int c = 0; c < numcols; c++) {
		colWidth[c] = 0.0;
		for (int r = 0; r < numrows; r++)
			if (item(c,r).width > colWidth[c])
				colWidth[c] = item(c,r).width;
	}

	// Compute the relative position of each column so as to centre the text.
	colPos[0] = colWidth[0] * 0.5 + margin;
	for (int c = 1; c < numcols; c++) {
		colPos[c] = colPos[c-1] + (colWidth[c-1] + colWidth[c]) * 0.5 + colGap;
	}

	// Compute the width and height
	w = colPos[numcols-1] + colWidth[numcols-1]*0.5 + margin;
	h = margin * 2.0 + fontHeight(font) * numrows;

	return;
}

void Table::draw(double left, double top)
{
	double x, y;

	// Render the text in the table using the layout previously computed.
	for (int r = 0; r < numrows; r++) {
		y = top + topBaseline - r * fontHeight(font);
		for (int c = 0; c < numcols; c++) {
			x = left + colPos[c] - item(c,r).width * 0.5;
			drawText(font, item(c,r).text, x, y);
		}
	}
}
