/*
 * HomoMatrix.h
 *
 * Symbolic computation of homogeneous matrices
 *
 *  Created on: 20 Mar 2015
 *      Author: Len Hamey
 */

#ifndef HOMOMATRIX_H_
#define HOMOMATRIX_H_

# include <string>

void symbolicMatrix(std::string (&matrix)[3][3], std::string transformation);

#endif /* HOMOMATRIX_H_ */
