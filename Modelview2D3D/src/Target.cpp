/*
 * Target.cpp
 *
 *  Created on: 16 Mar 2015
 *      Author: Len Hamey
 */

# include "Target.h"
# include "Transform.h"
# include <gl/glut.h>
# include <cstdlib>
# include <ctime>
# include <iostream>

Target::Target()
{
	tx = ty = tz = az = ax = ay = 0.0;
	sx = sy = sz = 1.0;
	itx = ity = ia = 0.0;
	isx = isy = 1.0;
}

bool Target::isIdentity(void)
{
	return tx == 0.0 && ty == 0.0 && tz == 0.0 && az == 0.0 &&
			ax == 0.0 && ay == 0.0 && sx == 1.0 && sy == 1.0 && sz == 1.0 &&
			itx == 0.0 && ity == 0.0 && ia == 0.0 && isx == 1.0 && isy == 1.0;
}

void Target::apply(void)
{
	glTranslated(tx, ty, tz);
	// In 3D, the object coordinates are Long == X, Trans == Z, Norm == Y
	// The preferred rotation sequence is Heading (Norm), Pitch (Trans), Roll (Long)
	// So rotate about y first, then about z, then about x
	glRotated(ay, 0,1,0);
	glRotated(az, 0,0,1);
	glRotated(ax, 1,0,0);
	glScaled(sx, sy, 1.0);
}

void Target::applyInit(void)
{
	glTranslated(itx, ity, 0.0);
	glRotated(ia, 0,0,1);
	glScaled(isx, isy, 1.0);
}

double Target::dran(double minv, double maxv, double step, double ominv, double omaxv)
{
	double v;
	while (1) {
		// Generate a random value on the range [minv,maxv] then test for
		// it being in the omitted range.
		// Generating a value: If zero is in the range, then generate it from zero
		// to ensure that it gets an actual zero value.
		int nvalues = (maxv + step * 1.5 - minv) / step;
		if (maxv >= 0.0 && minv <= 0.0) {
			// Generate from zero.  There is an assumption that zero is one of the
			// actual values that should be generated.  i.e. minv should be an
			// integer multiple of step (or very close to it)
			int zeroid = (-minv + step * 0.5) / step;
			v = ((rand() % nvalues) - zeroid) * step;
		}
		else
			v = (rand() % nvalues) * step + minv;
		// Test whether it is acceptable.  To be unacceptable, it must be
		// in the range [ominv-step/3,omaxv+step/2] AND ominv must not exceed omaxv
		// because omaxv < ominv denotes no exclusions.  The extension of the range
		// [ominv,omaxv] by step/2 at each end is in case of round-off errors.
		if (! (v >= ominv - step * 0.01 && v <= omaxv + step * 0.01 && ominv <= omaxv))
			break;
	}
	return v;
}

# define TARGET_TRANSLATE_X MENU_TARGET_1
# define TARGET_TRANSLATE_XY MENU_TARGET_2
# define TARGET_ROTATE MENU_TARGET_3
# define TARGET_SCALE_XY MENU_TARGET_4
# define TARGET_ROTATE_SCALE_XY MENU_TARGET_5
# define TARGET_TRANSLATE_XY_ROTATE MENU_TARGET_6
# define TARGET_TRANSLATE_XY_SCALE_XY MENU_TARGET_7
# define TARGET_TRANSLATE_XY_ROTATE_SCALE_XY MENU_TARGET_8
# define TARGET_TRANSLATE_XY_ROTATE_SCALE_XY_MIRROR MENU_TARGET_9
# define TARGET_ROTATE_TRANSLATED MENU_TARGET_11
# define TARGET_SCALE_TRANSLATED MENU_TARGET_12

void Target::randomTranslatePair(void)
{
	// Set tx, ty and itx, ity to a random translate pair that are accessible to our slider control.
	// The initial and final positions must be within the slider range, and the initial position must
	// be within slider range of the final position.
	itx = tx = dran(TRANSLATE_MIN, TRANSLATE_MAX, TRANSLATE_STEP, -1.0, 1.0);
	ity = ty = dran(TRANSLATE_MIN, TRANSLATE_MAX, TRANSLATE_STEP, -1.0, 1.0);
	// Ensure that the initial position is within TRANSLATE_MIN to TRANSLATE_MAX of the target, but is not the same.
	// This is applied to both itx and ity, so neither can be the same.
	while (itx == tx || itx - tx > TRANSLATE_MAX || itx - tx < TRANSLATE_MIN)
		itx = dran(TRANSLATE_MIN, TRANSLATE_MAX, TRANSLATE_STEP, -1.0, 1.0);
	while (ity == ty || ity - ty > TRANSLATE_MAX || ity - ty < TRANSLATE_MIN)
		ity = dran(TRANSLATE_MIN, TRANSLATE_MAX, TRANSLATE_STEP, -1.0, 1.0);
}


void Target::ranRotate(int minaxes, int maxaxes)
{
	// Generate a random rotation involving between minaxes and maxaxes randomly
	// selected axes (when in 3D).  In 2D, generates a non-zero random rotation.
# if THREE_D
	int naxes;
	int niter = 0;

	ax = ay = az = 0.0;
	while (niter++ < 1000 &&
			((naxes = (ax != 0.0) + (ay != 0.0) + (az != 0.0)) < minaxes || naxes > maxaxes)) {
		// 50% chance of any axis being rotated
		ax = ay = az = 0.0;
		if (rand() % 2)
			ax = dran(ROTATE_MIN, ROTATE_MAX, ROTATE_STEP, 0.0, 0.0);
		if (rand() % 2)
			ay = dran(ROTATE_MIN, ROTATE_MAX, ROTATE_STEP, 0.0, 0.0);
		if (rand() % 2)
			az = dran(ROTATE_MIN, ROTATE_MAX, ROTATE_STEP, 0.0, 0.0);
	}
# else
	// Omit rotation of 0 degrees.
	az = dran(ROTATE_MIN, ROTATE_MAX, ROTATE_STEP, 0.0, 0.0);
# endif
	return;
}


void Target::random(int type)
{
	// Set up a random target of some particular type.
	itx = ity = 0.0;
	ia = 0.0;
	isx = isy = 1.0;
	tx = ty = tz = 0.0;
	ax = ay = az = 0.0;
	sx = sy = sz = 1.0;
	switch (type) {
	case MENU_TARGET_NONE:
		// Identity
		break;

	case TARGET_TRANSLATE_X:
		// Omit translations that are close to zero and overlap the original object
		tx = dran(TRANSLATE_MIN, TRANSLATE_MAX, TRANSLATE_STEP, -1.0, 1.0);
		// Stay in the XZ plane.  tz is zeroed later in 2D mode
		tz = dran(TRANSLATE_MIN, TRANSLATE_MAX, TRANSLATE_STEP, -1.0, 1.0);
		break;

	case TARGET_TRANSLATE_XY:
		tx = dran(TRANSLATE_MIN, TRANSLATE_MAX, TRANSLATE_STEP, -1.0, 1.0);
		ty = dran(TRANSLATE_MIN, TRANSLATE_MAX, TRANSLATE_STEP, -1.0, 1.0);
		// tz is zeroed later in 2D mode
		tz = dran(TRANSLATE_MIN, TRANSLATE_MAX, TRANSLATE_STEP, -1.0, 1.0);
		break;

	case TARGET_ROTATE:
		// Rotate one axis at random (in 3D)
		ranRotate(1, 1);
		break;

	case TARGET_SCALE_XY:
		// Omit negative scale factors and zero scale factor.
		sx = dran(SCALE_STEP, SCALE_MAX, SCALE_STEP, 1.0, 1.0);
		sy = dran(SCALE_STEP, SCALE_MAX, SCALE_STEP, 1.0, 1.0);
		// sz is zeroed later in 2D mode
		sz = dran(SCALE_STEP, SCALE_MAX, SCALE_STEP, 1.0, 1.0);
		break;

	case TARGET_ROTATE_SCALE_XY:
# if THREE_D
		// In 3D mode, we combine rotations but do not add scaling.
		ranRotate(2, 3);
# else
		// Omit rotation of zero degrees.
		az = dran(ROTATE_MIN, ROTATE_MAX, ROTATE_STEP, 0.0, 0.0);

		// Omit negative scale factors and zero scale factor.
		sx = dran(SCALE_STEP, SCALE_MAX, SCALE_STEP, 1.0, 1.0);
		sy = dran(SCALE_STEP, SCALE_MAX, SCALE_STEP, 1.0, 1.0);
# endif
		break;

	case TARGET_TRANSLATE_XY_ROTATE:
		tx = dran(TRANSLATE_MIN, TRANSLATE_MAX, TRANSLATE_STEP, -1.0, 1.0);
		ty = dran(TRANSLATE_MIN, TRANSLATE_MAX, TRANSLATE_STEP, -1.0, 1.0);
		// tz will be zeroed in 2D mode.
		tz = dran(TRANSLATE_MIN, TRANSLATE_MAX, TRANSLATE_STEP, -1.0, 1.0);
		ranRotate(2, 3);
		break;

	case TARGET_TRANSLATE_XY_SCALE_XY:
		tx = dran(TRANSLATE_MIN, TRANSLATE_MAX, TRANSLATE_STEP, -1.0, 1.0);
		ty = dran(TRANSLATE_MIN, TRANSLATE_MAX, TRANSLATE_STEP, -1.0, 1.0);
		// tz will be zeroed in 2D mode.
		tz = dran(TRANSLATE_MIN, TRANSLATE_MAX, TRANSLATE_STEP, -1.0, 1.0);
		sx = dran(SCALE_STEP, SCALE_MAX, SCALE_STEP, 1.0, 1.0);
		sy = dran(SCALE_STEP, SCALE_MAX, SCALE_STEP, 1.0, 1.0);
		// sz will be set to 1 below in 2D mode.
		sz = dran(SCALE_STEP, SCALE_MAX, SCALE_STEP, 1.0, 1.0);
		break;

	case TARGET_TRANSLATE_XY_ROTATE_SCALE_XY:
		tx = dran(TRANSLATE_MIN, TRANSLATE_MAX, TRANSLATE_STEP, -1.0, 1.0);
		ty = dran(TRANSLATE_MIN, TRANSLATE_MAX, TRANSLATE_STEP, -1.0, 1.0);
		// tz will be zeroed in 2D mode.
		tz = dran(TRANSLATE_MIN, TRANSLATE_MAX, TRANSLATE_STEP, -1.0, 1.0);
		ranRotate(2, 3);
		sx = dran(SCALE_STEP, SCALE_MAX, SCALE_STEP, 1.0, 1.0);
		sy = dran(SCALE_STEP, SCALE_MAX, SCALE_STEP, 1.0, 1.0);
		// sz will be set to 1 below in 2D mode.
		sz = dran(SCALE_STEP, SCALE_MAX, SCALE_STEP, 1.0, 1.0);
		break;

	case TARGET_TRANSLATE_XY_ROTATE_SCALE_XY_MIRROR:
		tx = dran(TRANSLATE_MIN, TRANSLATE_MAX, TRANSLATE_STEP, -1.0, 1.0);
		ty = dran(TRANSLATE_MIN, TRANSLATE_MAX, TRANSLATE_STEP, -1.0, 1.0);
		// tz will be zeroed in 2D mode.
		tz = dran(TRANSLATE_MIN, TRANSLATE_MAX, TRANSLATE_STEP, -1.0, 1.0);
		ranRotate(2, 3);
		// X scale will always be negative for this case
		sx = dran(SCALE_MIN, -SCALE_STEP, SCALE_STEP, 0.0, 0.0);
		sy = dran(SCALE_STEP, SCALE_MAX, SCALE_STEP, 1.0, 1.0);
		// sz will be set to 1 below in 2D mode.
		sz = dran(SCALE_STEP, SCALE_MAX, SCALE_STEP, 1.0, 1.0);
		break;

	case TARGET_ROTATE_TRANSLATED:
		// Change rotation angle from one angle to another and translate the object also
		randomTranslatePair();
		// Omit rotation of 0 degrees.
		az = dran(ROTATE_MIN, ROTATE_MAX, ROTATE_STEP, 0.0, 0.0);
		// Ensure that the initial rotation angle differs from the final rotation angle
		ia = az;
		while (ia == az)
			ia = dran(ROTATE_MIN, ROTATE_MAX, ROTATE_STEP, 0.0, 0.0);
		break;

	case TARGET_SCALE_TRANSLATED:
		// Change the scaling and translate the object also
		randomTranslatePair();
		sx = dran(SCALE_STEP, SCALE_MAX, SCALE_STEP, 1.0, 1.0);
		sy = dran(SCALE_STEP, SCALE_MAX, SCALE_STEP, 1.0, 1.0);
		// Limit the initial scales to ones that have an inverse in our system.
		// i.e. 0.5 or 2.0.
		isx = dran(0.5, 2.0, SCALE_STEP, 1.0, 1.5);
		isy = dran(0.5, 2.0, SCALE_STEP, 1.0, 1.5);
		break;
	}

# if ! THREE_D
	// Override any 3D settings - means that they can be set above without worrying.
	tz = 0.0;
	ax = ay = 0.0;
	sz = 1.0;
# endif

	return;
}
