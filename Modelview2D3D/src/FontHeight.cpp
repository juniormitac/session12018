/*
 * FontHeight.cpp
 *
 *  Created on: 12 Mar 2015
 *      Author: Len Hamey
 */

#include "FontHeight.h"
#include <string>
#include <gl/glut.h>
#include <cassert>
#include <iostream>

int fontHeight(void *font) {
	int height;
	if (font == GLUT_BITMAP_8_BY_13)
		height = HEIGHT_GLUT_BITMAP_8_BY_13;
	else if (font == GLUT_BITMAP_9_BY_15)
		height = HEIGHT_GLUT_BITMAP_9_BY_15;
	else if (font == GLUT_BITMAP_HELVETICA_10)
		height = HEIGHT_GLUT_BITMAP_HELVETICA_10;
	else if (font == GLUT_BITMAP_HELVETICA_12)
		height = HEIGHT_GLUT_BITMAP_HELVETICA_12;
	else if (font == GLUT_BITMAP_HELVETICA_18)
		height = HEIGHT_GLUT_BITMAP_HELVETICA_18;
	else if (font == GLUT_BITMAP_TIMES_ROMAN_10)
		height = HEIGHT_GLUT_BITMAP_TIMES_ROMAN_10;
	else if (font == GLUT_BITMAP_TIMES_ROMAN_24)
		height = HEIGHT_GLUT_BITMAP_TIMES_ROMAN_24;
	else
		height = 0; // Unknown font
	return height;
}

int fontAbove(void *font) {
	int above;
	if (font == GLUT_BITMAP_8_BY_13)
		above = ABOVE_GLUT_BITMAP_8_BY_13;
	else if (font == GLUT_BITMAP_9_BY_15)
		above = ABOVE_GLUT_BITMAP_9_BY_15;
	else if (font == GLUT_BITMAP_HELVETICA_10)
		above = ABOVE_GLUT_BITMAP_HELVETICA_10;
	else if (font == GLUT_BITMAP_HELVETICA_12)
		above = ABOVE_GLUT_BITMAP_HELVETICA_12;
	else if (font == GLUT_BITMAP_HELVETICA_18)
		above = ABOVE_GLUT_BITMAP_HELVETICA_18;
	else if (font == GLUT_BITMAP_TIMES_ROMAN_10)
		above = ABOVE_GLUT_BITMAP_TIMES_ROMAN_10;
	else if (font == GLUT_BITMAP_TIMES_ROMAN_24)
		above = ABOVE_GLUT_BITMAP_TIMES_ROMAN_24;
	else
		above = 0; // Unknown font
	return above;
}

int fontBelow(void *font) {
	int below;
	if (font == GLUT_BITMAP_8_BY_13)
		below = BELOW_GLUT_BITMAP_8_BY_13;
	else if (font == GLUT_BITMAP_9_BY_15)
		below = BELOW_GLUT_BITMAP_9_BY_15;
	else if (font == GLUT_BITMAP_HELVETICA_10)
		below = BELOW_GLUT_BITMAP_HELVETICA_10;
	else if (font == GLUT_BITMAP_HELVETICA_12)
		below = BELOW_GLUT_BITMAP_HELVETICA_12;
	else if (font == GLUT_BITMAP_HELVETICA_18)
		below = BELOW_GLUT_BITMAP_HELVETICA_18;
	else if (font == GLUT_BITMAP_TIMES_ROMAN_10)
		below = BELOW_GLUT_BITMAP_TIMES_ROMAN_10;
	else if (font == GLUT_BITMAP_TIMES_ROMAN_24)
		below = BELOW_GLUT_BITMAP_TIMES_ROMAN_24;
	else
		below = 0; // Unknown font
	return below;
}

void *fontSubscript(void *font) {
	if (font == GLUT_BITMAP_8_BY_13)
		return GLUT_BITMAP_8_BY_13;
	else if (font == GLUT_BITMAP_9_BY_15)
		return GLUT_BITMAP_8_BY_13;
	else if (font == GLUT_BITMAP_HELVETICA_10)
		return GLUT_BITMAP_HELVETICA_10;
	else if (font == GLUT_BITMAP_HELVETICA_12)
		return GLUT_BITMAP_HELVETICA_10;
	else if (font == GLUT_BITMAP_HELVETICA_18)
		return GLUT_BITMAP_HELVETICA_12;
	else if (font == GLUT_BITMAP_TIMES_ROMAN_10)
		return GLUT_BITMAP_TIMES_ROMAN_10;
	else if (font == GLUT_BITMAP_TIMES_ROMAN_24)
		return GLUT_BITMAP_HELVETICA_18;
	else
		return GLUT_BITMAP_8_BY_13; // Unknown font
}

int dropSubscript(void *font) {
	font = fontSubscript(font);
	return fontAbove(font) / 4;
}

// textWidth with support for Math notation subscripts (but not yet superscripts)
int textWidth(void *font, std::string text)
{
	double width = 0.0;
	char c;
	void *subfont = fontSubscript(font);
	for (unsigned int i = 0; i < text.size(); i++) {
		c = text[i];
		if (c == '_') {
			i++;
			if (i < text.size()) {
				c = text[i];
				width += glutBitmapWidth(subfont, c);
			}
		}
		else {
			width += glutBitmapWidth(font, c);
		}
	}
	return width;
}

// drawText: To render a text string in a bitmap font, with newline support
void drawText(void *font, std::string text, float x, float y, float heightScale)
{
	// Draw text using the current drawing colour.
	// Handles newline characters by subtracting heightScale * fontheight
	// from y and resetting x.
	// Note: heightScale = 1.0 (the default) is suitable when the world-to-viewport
	// transformation has no scaling - in particular when the viewport and world
	// both range from (0,0) to (w,h).
	void *subfont = fontSubscript(font);
	glRasterPos3f(x,y,0);
	for (unsigned int i = 0; i < text.size(); i++) {
		if (text[i] == '\n') {

			// *******************************
			// Newline goes to the beginning of the next line
			float height = fontHeight(font) * heightScale;
			y -= height;
			glRasterPos3f(x,y,0);
		}
		else if (text[i] == '\r') {

			// *********************************
			// Carriage return goes back to the beginning of the current line
			glRasterPos3f(x,y,0);
		}
		else if (text[i] == '_') {
			i++;
			if (i < text.size()) {
				float raster[4], newraster[4];
				// NOTE: There seems to be a bug in GL_CURRENT_RASTER_POSITION
				// If the world coordinate origin is not the same as the viewport
				// origin then the coordinates returned are translated compared to
				// the coordinates that were set.  To prevent this bug being a problem
				// (and to work correctly when it is fixed if it ever is)
				// I map the control panel exactly 1:1 to the screen coordinates.
				// See Modelview2D.cpp where that is done.
				glGetFloatv(GL_CURRENT_RASTER_POSITION, raster);
				// Keep X and drop Y down
				//std::cout << raster[0] << "," << raster[1] << "  " << dropSubscript(font) << "\n";
				float dropY = raster[1] - dropSubscript(font) * heightScale;
				glRasterPos3f(raster[0], dropY, 0);
				glutBitmapCharacter(subfont,text[i]);
				glGetFloatv(GL_CURRENT_RASTER_POSITION, newraster);
				// Keep X and restore Y back up
				glRasterPos3f(newraster[0], raster[1], 0);
			}
		}
		else {

			// *********************************
			// Ordinary characters are rendered one by one
			glutBitmapCharacter(font,text[i]);
		}
	}
}


