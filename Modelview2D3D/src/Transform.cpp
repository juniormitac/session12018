/*
 * Transform.cpp
 *
 *  Created on: 12 Mar 2015
 *      Author: Len Hamey
 */

#include "Transform.h"
#include "FontHeight.h"
#include <gl/glut.h>
#include <stdio.h>
#include <iostream>
#include <cmath>

# define SLIDER_LINE_COLOUR 0.5,0.5,0.5
# define SLIDER_POINTER_COLOUR 1.0,0.0,0.0
# define SLIDER_LABEL_COLOUR 0.0,0.0,0.0
# define SLIDER_FONT GLUT_BITMAP_HELVETICA_18
# define SLIDER_MARGIN 3
# define SLIDER_INNER_GAP 3
# define SLIDER_BAR_HEIGHT 12

void Slider::init(void)
{
	left = 0.0;
	right = 0.0;
	top = 0.0;
	bottom = 0.0;

	minValue = 0.0;
	maxValue = 1.0;
	tickGap = 0.25;
	valueStep = tickGap;
	currentValue = 0.0;
	initialValue = 0.0;

	mouseResponding = false;
	mintext = "";
	maxtext = "";
	label = "";
}

void Slider::init(std::string _label, double _minv, double _maxv,
		double _tickGap, double _grid, double _value)
{
	char buffer[200];
	init();
	minValue = _minv;
	maxValue = _maxv;
	tickGap = _tickGap;
	valueStep = _grid;
	if (valueStep <= 0.0)
		valueStep = tickGap;
	currentValue = _value;
	initialValue = _value;

	sprintf (buffer, "%g", minValue);
	mintext = std::string(buffer);;
	label = _label;
	sprintf (buffer, "%g", maxValue);
	maxtext = std::string(buffer);
}

Slider::Slider() {
	init();
}

Slider::Slider(std::string _label, double _minv, double _maxv,
		double _tickgap, double _grid, double _value)
{
	init(_label, _minv, _maxv, _tickgap, _grid, _value);
}

bool Slider::event(int event, int button, int state, double mx, double my)
{
	// If the mouse is within the region used by the slider then respond to it
	if (event == MOUSE_CLICK && state == GLUT_DOWN && button == GLUT_LEFT_BUTTON &&
			mx >= left && mx <= right && my >= textBaseline && my < top) {
		// The mouse is inside the region of the slider
		mouseResponding = true;
	}
	if (mouseResponding) {
		// Restrict range of mouse X coordinates to range of the slider itself.
		if (mx < barLeft) mx = barLeft;
		if (mx > barRight) mx = barRight;
		double newvalue = (mx - barLeft) * (maxValue - minValue) / (barRight - barLeft) + minValue;
		newvalue = (int)(newvalue / valueStep + ((newvalue >= 0.0) ? 0.5 : -0.5));
		newvalue = newvalue * valueStep;
		// If the slider value has changed then update it and request redisplay
		if (newvalue != currentValue) {
			currentValue = newvalue;
			glutPostRedisplay();
		}
		if (event == MOUSE_CLICK && button == GLUT_LEFT_BUTTON && state == GLUT_UP) {
			// Time to stop responding - button released
			mouseResponding = false;
		}
		return true; // Slider responded to this mouse event
	}
	return false;
}

double Slider::layout(double x, double y, double w)
{
	left = x;
	top = y;
	right = left + w;

	// Layout the slider bar itself
	barTop = y - SLIDER_MARGIN;
	barBottom = barTop - SLIDER_BAR_HEIGHT;
	barLeft = left + SLIDER_MARGIN;
	barRight = right - SLIDER_MARGIN;

	// Layout placement of the text labels on the slider
	textBaseline = barBottom-SLIDER_INNER_GAP-fontAbove(SLIDER_FONT); // Allow space for drop characters

	bottom = textBaseline - fontBelow(SLIDER_FONT) - SLIDER_MARGIN; // Compute the bottom of the slider and return it

	return bottom;
}

void Slider::draw(void)
{
	// Draw the parts of the slider
	// The horizontal line
	glColor3f(SLIDER_LINE_COLOUR);
	glLineWidth(1.0);
	glBegin(GL_LINES);
		glVertex2d(barLeft,(barBottom+barTop)/2.0);
		glVertex2d(barRight,(barBottom+barTop)/2.0);
	glEnd();

	// The tick marks.
	// Convert tickgap to world (screen) coordinates from slider data range
	double world_tick = tickGap * (barRight - barLeft) / (maxValue - minValue);
	glBegin(GL_LINES);
		for (double tx = barLeft; tx <= barRight * 1.0001; tx += world_tick) {
			glVertex2d(tx,barBottom);
			glVertex2d(tx,barTop);
		}
	glEnd();

	// The pointer is a thicker line in a different colour
	glColor3f(SLIDER_POINTER_COLOUR);
	glLineWidth(3.0);
	glBegin(GL_LINES);
		double px = (currentValue-minValue)*(barRight - barLeft)/(maxValue-minValue) + barLeft;
		glVertex2d(px,barBottom);
		glVertex2d(px,barTop);
	glEnd();

	// Text display of the labels
	glColor3f(SLIDER_LABEL_COLOUR);
	drawText(SLIDER_FONT, mintext, barLeft, textBaseline);
	drawText(SLIDER_FONT, label, (barLeft+barRight-textWidth(SLIDER_FONT, label))/2.0, textBaseline);
	drawText(SLIDER_FONT, maxtext, barRight-textWidth(SLIDER_FONT,maxtext), textBaseline);

	return;
}

void Slider::reset(void)
{
	currentValue = initialValue;
	glutPostRedisplay();
}

//# define TRANSFORM_FONT GLUT_BITMAP_HELVETICA_18
# define TRANSFORM_FONT GLUT_BITMAP_TIMES_ROMAN_24
# define TRANSFORM_TEXT_COLOUR 0,0,0
# define TRANSFORM_INDENT 25
# define TRANSFORM_GAP 10
# define TRANSFORM_MARGIN 10
# define TRANSFORM_BACKGROUND 0.92,0.92,0.92
# define TRANSFORM_SELECTED_BACKGROUND 1,1,1
# define TRANSFORM_SELECTED_BOX_COLOUR 0,0,1

// Fixed width for sliders.
// Or use an expression depending on w, the width of the box in which to draw
# define SLIDER_WIDTH 400.0

Transform::Transform()
{
	// Constructor
	left = 0.0;
	right = 0.0;
	top = 0.0;
	bottom = 0.0;
	textBaseline = 0.0;

	isSelected = false;
	isLast = false;
	subscript = "";

	showMode = SHOW_NUMBERS;
	compactMode = false;
}

Transform::~Transform()
{
	// Destructor
}


bool Transform::event(int event, int button, int state, double _x, double _y)
{
	// Check for passive motion affecting the object.
	// Menu items rearrange the objects, so that may change the selection also
	// The only thing is that we ignore mouse motion events because the selection
	// does not change while manipulating an object (e.g. a slider)
	if (event != MOUSE_MOTION) {
		bool select;
		//std::cout << "Check selection " << _x << " in " << left << "," << right
		//		  << "   " << _y << " in " << bottom << "," << top << "\n";
		select = _x > left && _x < right && _y < top && _y > bottom;
		if (select != isSelected) {
			//std::cout << "Selection changed to " << select << "\n";
			glutPostRedisplay();
		}
		isSelected = select;
	}

	if (event == MENU_ITEM) {
		if (button == MENU_COMPACT)  { compactMode = true; glutPostRedisplay(); }
		if (button == MENU_EXPANDED) { compactMode = false; glutPostRedisplay(); }
	}
	return false;
}

double Transform::layout(int mode, double x, double y, double w)
{
	showMode = mode; // Remember for draw call.
	left = x;
	right = x + w;
	top = y;
	// Standard content layout: the top line of text in every Transform and the gap below it.
	textBaseline = y - TRANSFORM_MARGIN - fontAbove(TRANSFORM_FONT);
	bottom = textBaseline - fontBelow(TRANSFORM_FONT) - TRANSFORM_GAP;
	return bottom; // It is up to the derived class to add content and layout details
}

void Transform::draw(void)
{
	if (isSelected)
		glColor3f(TRANSFORM_SELECTED_BACKGROUND);
	else
		glColor3f(TRANSFORM_BACKGROUND);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glRectd(left, bottom, right, top);
	if (isSelected) {
		glColor3f(TRANSFORM_SELECTED_BOX_COLOUR);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glRectd(left, bottom, right, top);
	}
	return; // It is up to the derived class to draw content inside the box
}

void Transform::setTransformation(std::string trans)
{
	return; // Does nothing for most widgets.
}

Translate::Translate() : Transform()
{
	// Slider range is TRANSLATE_MIN to TRANSLATE_MAX.  Target class must take care to
	// ensure that targets are in range of the sliders.
	sliderX.init("t_x", TRANSLATE_MIN, TRANSLATE_MAX, 1.0, TRANSLATE_STEP, 0.0);
	sliderY.init("t_y", TRANSLATE_MIN, TRANSLATE_MAX, 1.0, TRANSLATE_STEP, 0.0);
	sliderZ.init("t_z", TRANSLATE_MIN, TRANSLATE_MAX, 1.0, TRANSLATE_STEP, 0.0);
}

double Translate::layout(int mode, double x, double y, double w)
{
	y = Transform::layout(mode, x, y, w);
	x += TRANSFORM_MARGIN; // Left margin
	w -= 2*TRANSFORM_MARGIN; // Right margin
	// In compact mode, when not selected (i.e. not hovered over) omit the sliders.
	if (isSelected || ! compactMode) {
		y = sliderX.layout(x, y, SLIDER_WIDTH);
		y = sliderY.layout(x, y, SLIDER_WIDTH);
# if THREE_D
		y = sliderZ.layout(x, y, SLIDER_WIDTH);
# endif
	}
	y -= TRANSFORM_MARGIN; // Bottom margin of the TRANSFORM box
	bottom = y;
	return y;
}

void Translate::draw(void)
{
	char buffer[200];
	std::string nameText;
	Transform::draw();
	sprintf (buffer, "glTranslated(%g, %g, %g)", sliderX.value(), sliderY.value(), sliderZ.value());
	glColor3f(TRANSFORM_TEXT_COLOUR);
	drawText(TRANSFORM_FONT, std::string(buffer), left+TRANSFORM_MARGIN, textBaseline);

# if THREE_D
	sprintf (buffer, "T(%g,%g,%g)", sliderX.value(), sliderY.value(), sliderZ.value());
# else
	sprintf (buffer, "T(%g,%g)", sliderX.value(), sliderY.value());
# endif
	nameText = std::string(buffer);
	drawText(TRANSFORM_FONT, nameText, right-TRANSFORM_MARGIN-textWidth(TRANSFORM_FONT,nameText), textBaseline);

	// Note that the exact position of the label text is determined in slider draw.
	sliderX.setLabel("t"+subscript+"_x");
	sliderY.setLabel("t"+subscript+"_y");
	sliderZ.setLabel("t"+subscript+"_z");
	// In compact mode, when not selected (i.e. not hovered over) omit the sliders.
	if (isSelected || ! compactMode) {
		sliderX.draw();
		sliderY.draw();
# if THREE_D
		sliderZ.draw();
# endif
	}
}

std::string Translate::name(int mode)
{
	char buffer[200];
	if (mode == SHOW_NUMBERS) {
# if THREE_D
		sprintf (buffer, "T(%g,%g,%g)", sliderX.value(), sliderY.value(), sliderZ.value());
# else
		sprintf (buffer, "T(%g,%g)", sliderX.value(), sliderY.value());
# endif
		return std::string(buffer);
	}
# if THREE_D
	return "T(t"+subscript+"_x, t"+subscript+"_y, t"+subscript+"_z)";
# else
	return "T(t"+subscript+"_x, t"+subscript+"_y)";
# endif
}

void Translate::apply(void)
{
# if THREE_D
	glTranslated(sliderX.value(), sliderY.value(), sliderZ.value());
# else
	glTranslated(sliderX.value(), sliderY.value(), 0.0);
# endif
}

bool Translate::event(int event, int button, int state, double x, double y)
{
	Transform::event(event, button, state, x, y);

	// If in compact mode and widget is not selected, then don't pass events to sliders.
	if (! isSelected && compactMode) return false;

	// Try each mouse responding component in turn.
	sliderX.event(event, button, state, x, y);
	sliderY.event(event, button, state, x, y);
# if THREE_D
	sliderZ.event(event, button, state, x, y);
# endif
	// Sliders do not respond directly to MENU_RESET events because they should only
	// be applied if the current item is selected.
	if (resetApplies(event, button)) {
		sliderX.reset();
		sliderY.reset();
		sliderZ.reset();
	}
	return false;
}

Rotate::Rotate(char _axis) : Transform()
{
	axis = _axis;
	// Note: Rotate::draw updates the name, so it does not have to be set here
	sliderA.init("a", ROTATE_MIN, ROTATE_MAX, 90.0, ROTATE_STEP, 0.0);
}

double Rotate::layout(int mode, double x, double y, double w)
{
	y = Transform::layout(mode, x, y, w);
	x += TRANSFORM_MARGIN; // Left margin
	w -= 2*TRANSFORM_MARGIN; // Right margin
	// In compact mode, when not selected (i.e. not hovered over) omit the sliders.
	if (isSelected || ! compactMode) {
		y = sliderA.layout(x, y, SLIDER_WIDTH);
	}
	y -= TRANSFORM_MARGIN; // Bottom margin of the TRANSFORM box
	bottom = y;
	return y;
}

void Rotate::draw(void)
{
	char buffer[200];
	std::string axisSubscript = "", nameText;
	Transform::draw();
# if THREE_D
	sprintf (buffer, "_%c", axis);
	axisSubscript = std::string(buffer);
# endif
	sprintf (buffer, "glRotated(%g, %d, %d, %d)", sliderA.value(), axis == 'x', axis == 'y', axis == 'z');
	glColor3f(TRANSFORM_TEXT_COLOUR);
	drawText(TRANSFORM_FONT, std::string(buffer), left+TRANSFORM_MARGIN, textBaseline);

	sprintf (buffer, "R%s(%g)", axisSubscript.c_str(), sliderA.value());
	nameText = std::string(buffer);
	drawText(TRANSFORM_FONT, nameText, right-TRANSFORM_MARGIN-textWidth(TRANSFORM_FONT,nameText), textBaseline);

	sliderA.setLabel("a" + axisSubscript + subscript);
	// In compact mode, when not selected (i.e. not hovered over) omit the sliders.
	if (isSelected || ! compactMode) {
		sliderA.draw();
	}
}

std::string Rotate::name(int mode)
{
	char buffer[200];
	if (mode == SHOW_NUMBERS) {
# if THREE_D
		sprintf (buffer, "R_%c(%g)", axis, sliderA.value());
# else
		sprintf (buffer, "R(%g)", sliderA.value());
# endif
		return std::string(buffer);
	}
# if THREE_D
	sprintf (buffer, "R_%c(a%s)", axis, subscript.c_str());
	return std::string(buffer);
# else
	return "R(a"+subscript+")";
# endif
}

void Rotate::apply()
{
	glRotated(sliderA.value(), axis == 'x', axis == 'y', axis == 'z');
}

bool Rotate::event(int event, int button, int state, double x, double y)
{
	Transform::event(event, button, state, x, y);

	// If in compact mode and widget is not selected, then don't pass events to sliders.
	if (! isSelected && compactMode) return false;

	// Try each mouse responding component in turn.  If one responds, then don't try the others
	// in case there is some slight overlap.
	sliderA.event(event, button, state, x, y);
	if (resetApplies(event, button)) {
		sliderA.reset();
	}
	return false;
}

Scale::Scale() : Transform()
{
	sliderSX.init("s_x", SCALE_MIN, SCALE_MAX, 1.0, SCALE_STEP, 1.0);
	sliderSY.init("s_y", SCALE_MIN, SCALE_MAX, 1.0, SCALE_STEP, 1.0);
	sliderSZ.init("s_z", SCALE_MIN, SCALE_MAX, 1.0, SCALE_STEP, 1.0);
}

double Scale::layout(int mode, double x, double y, double w)
{
	y = Transform::layout(mode, x, y, w);
	x += TRANSFORM_MARGIN; // Left margin
	w -= 2*TRANSFORM_MARGIN; // Right margin
	// In compact mode, when not selected (i.e. not hovered over) omit the sliders.
	if (isSelected || ! compactMode) {
		y = sliderSX.layout(x, y, SLIDER_WIDTH);
		y = sliderSY.layout(x, y, SLIDER_WIDTH);
# if THREE_D
		y = sliderSZ.layout(x, y, SLIDER_WIDTH);
# endif
	}
	y -= TRANSFORM_MARGIN; // Bottom margin of the TRANSFORM box
	bottom = y;
	return y;
}

void Scale::draw(void)
{
	char buffer[200];
	std::string nameText = "";
	Transform::draw();
	sprintf (buffer, "glScaled(%g, %g, 1)", sliderSX.value(), sliderSY.value());
	glColor3f(TRANSFORM_TEXT_COLOUR);
	drawText(TRANSFORM_FONT, std::string(buffer), left+TRANSFORM_MARGIN, textBaseline);
	sliderSX.setLabel("s"+subscript+"_x");
	sliderSY.setLabel("s"+subscript+"_y");
	sliderSZ.setLabel("s"+subscript+"_z");

# if THREE_D
	sprintf (buffer, "S(%g,%g,%g)", sliderSX.value(), sliderSY.value(), sliderSZ.value());
# else
	sprintf (buffer, "S(%g,%g)", sliderSX.value(), sliderSY.value());
# endif
	nameText = std::string(buffer);
	drawText(TRANSFORM_FONT, nameText, right-TRANSFORM_MARGIN-textWidth(TRANSFORM_FONT,nameText), textBaseline);

	// In compact mode, when not selected (i.e. not hovered over) omit the sliders.
	if (isSelected || ! compactMode) {
		sliderSX.draw();
		sliderSY.draw();
# if THREE_D
		sliderSZ.draw();
# endif
	}

	return;
}

std::string Scale::name(int mode)
{
	char buffer[200];
	if (mode == SHOW_NUMBERS) {
# if THREE_D
		sprintf (buffer, "S(%g,%g,%g)", sliderSX.value(), sliderSY.value(), sliderSZ.value());
# else
		sprintf (buffer, "S(%g,%g)", sliderSX.value(), sliderSY.value());
# endif
		return std::string(buffer);
	}
	return "S(s"+subscript+"_x, s"+subscript+"_y)";
}

void Scale::apply()
{
# if THREE_D
	glScaled(sliderSX.value(), sliderSY.value(), sliderSZ.value());
# else
	glScaled(sliderSX.value(), sliderSY.value(), 1.0);
# endif
}

bool Scale::event(int event, int button, int state, double x, double y)
{
	Transform::event(event, button, state, x, y);

	// If in compact mode and widget is not selected, then don't pass events to sliders.
	if (! isSelected && compactMode) return false;

	// Try each mouse responding component in turn.  If one responds, then don't try the others
	// in case there is some slight overlap.
	sliderSX.event(event, button, state, x, y);
	sliderSY.event(event, button, state, x, y);
# if THREE_D
	sliderSZ.event(event, button, state, x, y);
# endif
	if (resetApplies(event, button)) {
		sliderSX.reset();
		sliderSY.reset();
		sliderSZ.reset();
	}
	return false;
}

// -------- Shear is not used in 3D mode
Shear::Shear() : Transform()
{
	sliderShX.init("b_x", SHEAR_MIN, SHEAR_MAX, 1.0, SHEAR_STEP, 0.0);
	sliderShY.init("b_y", SHEAR_MIN, SHEAR_MAX, 1.0, SHEAR_STEP, 0.0);
}

double Shear::layout(int mode, double x, double y, double w)
{
	y = Transform::layout(mode, x, y, w);
	x += TRANSFORM_MARGIN; // Left margin
	w -= 2*TRANSFORM_MARGIN; // Right margin
	// In compact mode, when not selected (i.e. not hovered over) omit the sliders.
	if (isSelected || ! compactMode) {
		y = sliderShX.layout(x, y, SLIDER_WIDTH);
		y = sliderShY.layout(x, y, SLIDER_WIDTH);
	}
	y -= TRANSFORM_MARGIN; // Bottom margin of the TRANSFORM box
	bottom = y;
	return y;
}

void Shear::draw(void)
{
	char buffer[200];
	std::string nameText = "";
	Transform::draw();
	sprintf (buffer, "Shear b_x = %g, b_y = %g", sliderShX.value(), sliderShY.value());
	glColor3f(TRANSFORM_TEXT_COLOUR);
	drawText(TRANSFORM_FONT, std::string(buffer), left+TRANSFORM_MARGIN, textBaseline);
	sliderShX.setLabel("b"+subscript+"_x");
	sliderShY.setLabel("b"+subscript+"_y");

	sprintf (buffer, "Sh(%g,%g)", sliderShX.value(), sliderShY.value());
	nameText = std::string(buffer);
	drawText(TRANSFORM_FONT, nameText, right-TRANSFORM_MARGIN-textWidth(TRANSFORM_FONT,nameText), textBaseline);

	// In compact mode, when not selected (i.e. not hovered over) omit the sliders.
	if (isSelected || ! compactMode) {
		sliderShX.draw();
		sliderShY.draw();
	}
}

std::string Shear::name(int mode)
{
	char buffer[200];
	if (mode == SHOW_NUMBERS) {
		sprintf (buffer, "Sh(%g,%g)", sliderShX.value(), sliderShY.value());
		return std::string(buffer);
	}
	return "Sh(b"+subscript+"_x, b"+subscript+"_y)";
}

void Shear::apply()
{
	double matrix[16];
	// Construct identity matrix
	for (int i = 0; i < 16; i++)
		matrix[i] = 0.0;
	matrix[0] = matrix[5] = matrix[10] = matrix[15] = 1.0;
	// Insert the value of b_x and b_y
	matrix[4] = sliderShX.value();
	matrix[1] = sliderShY.value();
	glMultMatrixd(matrix);
}

bool Shear::event(int event, int button, int state, double x, double y)
{
	Transform::event(event, button, state, x, y);

	// If in compact mode and widget is not selected, then don't pass events to sliders.
	if (! isSelected && compactMode) return false;

	// Try each mouse responding component in turn.  If one responds, then don't try the others
	// in case there is some slight overlap.
	sliderShX.event(event, button, state, x, y);
	sliderShY.event(event, button, state, x, y);
	if (resetApplies(event, button)) {
		sliderShX.reset();
		sliderShY.reset();
	}
	return false;
}



ModelView::ModelView()
{
# if THREE_D
	static std::string leftVector[] = { "x'", "y'", "z'", "w'" };
	static std::string leftWorld[] = { "x_w", "y_w", "z_w", "w_w" };
	static std::string rightVector[] = {"x_m", "y_m", "z_m", "w_m" };
	static std::string leftVVector[] = { "x'", "y'", "z'" };
	static std::string leftVWorld[] = {"x_w", "y_w", "z_w" };
	static std::string rightVVector[] = { "x_m", "y_m", "z_w" };
# else
	static std::string leftVector[] = { "x'", "y'", "w'" };
	static std::string leftWorld[] = { "x_w", "y_w", "w_w" };
	static std::string rightVector[] = {"x_m", "y_m", "w_m" };
	static std::string leftVVector[] = { "x'", "y'" };
	static std::string leftVWorld[] = {"x_w", "y_w" };
	static std::string rightVVector[] = { "x_m", "y_m" };
# endif
	validMatrix = false;
	// When a modelview matrix widget is first created we do not have the data to display
	// but we can compute the height correctly by faking it as a 3x3 matrix.
	table.init(3, 3);
	tableHeight = 0.0;
	tableWidth = 0.0;
	tableLeft = 0.0;
	tableTop = 0.0;
	// The standard layout is
	// leftVector  ltext  table  rtext  rightVector  ctext   constantVector
	ltext = "  =  ";
	rtext = "  ";
	ctext = "  +  ";
	font = GLUT_BITMAP_HELVETICA_18;
	ltextWidth = textWidth(font, ltext);
	rtextWidth = textWidth(font, rtext);
	ctextWidth = textWidth(font, ctext);
	lvec3.init(leftVector, 1, sizeof(leftVector)/sizeof(leftVector[0]));
	lvec2.init(leftVVector, 1, sizeof(leftVVector)/sizeof(leftVVector[0]));
	lworld3.init(leftWorld, 1, sizeof(leftWorld)/sizeof(leftWorld[0]));
	lworld2.init(leftVWorld, 1, sizeof(leftVWorld)/sizeof(leftVWorld[0]));
	rvec3.init(rightVector, 1, sizeof(rightVector)/sizeof(rightVector[0]));
	rvec2.init(rightVVector, 1, sizeof(rightVVector)/sizeof(rightVVector[0]));
	// It would be a disaster if the heights of the vectors were not equal

	ltextLeft = lvecworldLeft = rtextLeft = rvecLeft = midBaseline = 0.0;
	ctextLeft = cvecHeight = cvecLeft = lvecHeight = lvecWidth = 0.0;
	lworldHeight = lworldWidth = cvecWidth = rvecWidth = rvecHeight = 0.0;
	isWorld = false;
	matrixChanged = true;
	contentMode = CONTENT_EQUATIONS;
	transformation = "??"; // Must not be the empty string - must be a string that cannot be generated as the transformation
	transformationWidth = transformationHeight = 0;
}

# define MATRIX_BRACKET_WIDTH 6.0

static void matrixOutline(double left, double top, double width, double height)
{
	// Draw outline of a matrix.  Square brackets.
	glLineWidth(2.0);
	glBegin(GL_LINES);
		glVertex2d(left, top);
		glVertex2d(left+MATRIX_BRACKET_WIDTH, top);
		glVertex2d(left, top);
		glVertex2d(left, top-height);
		glVertex2d(left, top-height);
		glVertex2d(left+MATRIX_BRACKET_WIDTH, top-height);

		glVertex2d(left+width, top);
		glVertex2d(left+width-MATRIX_BRACKET_WIDTH, top);
		glVertex2d(left+width, top);
		glVertex2d(left+width, top-height);
		glVertex2d(left+width, top-height);
		glVertex2d(left+width-MATRIX_BRACKET_WIDTH, top-height);
	glEnd();
}

void ModelView::draw(void)
{
	Transform::draw();

	glColor3f(TRANSFORM_TEXT_COLOUR);
	drawText(TRANSFORM_FONT,
			(contentMode == CONTENT_EQUATIONS ? "Model Transform Equations" :
					contentMode == CONTENT_VECTOR ? "Model Transform Vector Equation" :
							contentMode == CONTENT_HOMOGENEOUS_MATRIX ? "Model Transform Homogeneous Matrix" :
									"Model Transform Homogeneous Transformation"),
			left+TRANSFORM_MARGIN, textBaseline);

	if (contentMode == CONTENT_EQUATIONS) {
		// The equations are presented as a table without the matrix outline.
		table.draw(tableLeft, tableTop);
		return;
	}

	// If content is homogeneous equations (as opposed to homogeneous matrix display) then
	// show the equation form (x,y) = T() R()... (x,y)
	// This means substituting the transformation for the table.
	// The layout has already done the calculations with substitution.
	// It now remains to draw the substituted text.
	if (contentMode == CONTENT_TRANSFORMATION) {
		drawText(font, transformation, tableLeft, midBaseline);
	}
	else if (contentMode == CONTENT_HOMOGENEOUS_MATRIX && showMode == SHOW_SYMBOLS) {
		// Draw the transformationTable instead of the main table.
		transformationTable.draw(tableLeft, tableTop);
		matrixOutline(tableLeft, tableTop, tableWidth, tableHeight);
	}
	else {
		// Draw the model view matrix which must already have been obtained.
		table.draw(tableLeft, tableTop);
		matrixOutline(tableLeft, tableTop, tableWidth, tableHeight);
	}

	if (! isWorld) {
		if (contentMode == CONTENT_VECTOR)
			lvec2.draw(lvecworldLeft, tableTop);
		else
			lvec3.draw(lvecworldLeft, tableTop);
		matrixOutline(lvecworldLeft, tableTop, lvecWidth, tableHeight);
	}
	else {
		if (contentMode == CONTENT_VECTOR)
			lworld2.draw(lvecworldLeft, tableTop);
		else
			lworld3.draw(lvecworldLeft, tableTop);
		matrixOutline(lvecworldLeft, tableTop, lworldWidth, tableHeight);
	}
	if (contentMode == CONTENT_VECTOR) {
		rvec2.draw(rvecLeft, tableTop);
		matrixOutline(rvecLeft, tableTop, rvecWidth, tableHeight);
		drawText(font, ctext, ctextLeft, midBaseline);
		cvec.draw(cvecLeft, tableTop);
		matrixOutline(cvecLeft, tableTop, cvecWidth, tableHeight);
	}
	else {
		rvec3.draw(rvecLeft, tableTop);
		matrixOutline(rvecLeft, tableTop, rvecWidth, tableHeight);
	}

	drawText(font, ltext, ltextLeft, midBaseline);
	drawText(font, rtext, rtextLeft, midBaseline);
}

std::string ModelView::name(int mode)
{
	return "";
}

// This function is used to compute the symbolic transformation matrix whenever the transformation code (sequence) is changed.
void ModelView::setTransformation(std::string trans)
{
	std::string transformationMatrix[3][3];
	if (trans == transformation) return; // No change, no work to do.
	transformation = trans;
	symbolicMatrix(transformationMatrix, transformation);
	transformationTable.init(&transformationMatrix[0][0], 3, 3);
	return;
}

// -------- Building equations like  Xw = 0.5 Xm + 2

// Join a LHS expression with a RHS expression with op as the operator between them.
// Firstly, if either LHS or RHS or both are empty, the operator disappears (except for negation)
// 		If the LHS or RHS is empty and the op is empty or space then just return the other side.
// 		If the op is + and the LHS is empty, return RHS.
// 		If the op is - and the LHS is empty, return negation of RHS (without a space)
// Secondly, the operator is interpreted
// 		If the op is completely empty, assume a comma separator
// 		If the op is space, place a space between the two sides.
static std::string join(std::string left, std::string op, std::string right)
{
	if (left == "" && (op == "" || op == " ")) return right;
	if (right == "" && (op == "" || op == " ")) return left;
	if (left == "" && op == "+") return right;
	if (left == "" && op == "-") return "-" + right;
	if (op == "") return left + ", " + right;
	if (op == " ") return left + " " + right;
	return left + " " + op + " " + right;
}

void opTerm(std::string &op, std::string &term, std::string left, double m, std::string var)
{
	char buffer[100], buffer2[100];
	std::string mul;
	bool negative = m < 0.0;
	if (negative) m = -m;
	sprintf (buffer, "%.2f", m);
	sprintf (buffer2, "%.2g", m);
	mul = strlen(buffer2) < strlen(buffer) ? std::string(buffer2) : std::string(buffer);
	if (mul == "0" || mul == "0.00") { op = ""; term = ""; return; }
	if (var == "") {
		if (negative) { op = "-"; term = mul; return; }
		op = "+"; term = mul; return;
	}
	if (mul == "1" && ! negative) { op = "+"; term = var; return; }
	if (mul == "1" && left != "" && negative) { op = "-"; term = var; return; }
	if (mul == "1" && left == "" && negative) { op = ""; term = "-"+var; return; }
	if (negative && left == "") { op = ""; term = "-"+mul + " " + var; return; }
	if (negative) { op = "-"; term = mul + " " + var; return; }
	op = "+"; term = mul + " " + var; return;
}

# if ! THREE_D
// Build an equation in 2D mode, with numeric values and symbolic names for x, y etc.
static std::string build_equation2d(std::string lhs, double m1, std::string v1,
		double m2, std::string v2, double c)
{
	std::string rhs, mult1, mult2, cons;
	std::string op, term;
	rhs = "";
	opTerm(op, term, rhs, m1, v1);
	rhs = join(rhs, op, term);
	opTerm(op, term, rhs, m2, v2);
	rhs = join(rhs, op, term);
	opTerm(op, term, rhs, c, "");
	rhs = join(rhs, op, term);
	return lhs + " = " + rhs;
}
# endif

# if THREE_D
static std::string build_equation3d(std::string lhs, double m1, std::string v1,
		double m2, std::string v2, double m3, std::string v3, double c)
{
	std::string rhs, mult1, mult2, cons;
	std::string op, term;
	rhs = "";
	opTerm(op, term, rhs, m1, v1);
	rhs = join(rhs, op, term);
	opTerm(op, term, rhs, m2, v2);
	rhs = join(rhs, op, term);
	opTerm(op, term, rhs, m3, v3);
	rhs = join(rhs, op, term);
	opTerm(op, term, rhs, c, "");
	rhs = join(rhs, op, term);
	return lhs + " = " + rhs;
}
# endif

double ModelView::layout(int mode, double x, double y, double w)
{
	double leftvectorwidth;

# if THREE_D
	// When displaying the homogeneous matrix in 3D, parameters (symbolic terms) are not supported.
	if (contentMode == CONTENT_HOMOGENEOUS_MATRIX)
		mode = SHOW_NUMBERS;
# endif

	if (contentMode == CONTENT_VECTOR) {
		lvec2.layout(lvecWidth, lvecHeight);
		lworld2.layout(lworldWidth, lworldHeight);
		rvec2.layout(rvecWidth, rvecHeight);
	}
	else {
		lvec3.layout(lvecWidth, lvecHeight);
		lworld3.layout(lworldWidth, lworldHeight);
		rvec3.layout(rvecWidth, rvecHeight);
	}

	if (contentMode == CONTENT_HOMOGENEOUS_MATRIX) {
		transformationTable.layout(transformationWidth, transformationHeight);
	}
	else if (contentMode == CONTENT_TRANSFORMATION)
		transformationWidth = textWidth(font, transformation);

	// If the matrix has been changed, then update the table
	// Also, if isWorld is changing then update it.
	if (matrixChanged || isLast != isWorld) {
		isWorld = isLast;
		if (contentMode == CONTENT_EQUATIONS) {
			// Draw as a pair (or triple in 3D) of equations as the content of the table, without matrix outline
			std::string s[3];
# if THREE_D
			s[0] = build_equation3d((isWorld ? "x_w" : "x'"),
					displayFullMatrix[0][0], "x_m", displayFullMatrix[0][1], "y_m",
					displayFullMatrix[0][2], "z_m",
					displayFullMatrix[0][3]);
			s[1] = build_equation3d((isWorld ? "y_w" : "y'"),
					displayFullMatrix[1][0], "x_m", displayFullMatrix[1][1], "y_m",
					displayFullMatrix[1][2], "z_m",
					displayFullMatrix[1][3]);
			s[2] = build_equation3d((isWorld ? "z_w" : "z'"),
					displayFullMatrix[2][0], "x_m", displayFullMatrix[2][1], "y_m",
					displayFullMatrix[2][2], "z_m",
					displayFullMatrix[2][3]);
			table.init(s, 1, 3);
# else
			s[0] = build_equation2d((isWorld ? "x_w" : "x'"),
					displayFullMatrix[0][0], "x_m", displayFullMatrix[0][1], "y_m",
					displayFullMatrix[0][2]);
			s[1] = build_equation2d((isWorld ? "y_w" : "y'"),
					displayFullMatrix[1][0], "x_m", displayFullMatrix[1][1], "y_m",
					displayFullMatrix[1][2]);
			table.init(s, 1, 2);
# endif
		}
		else {
# if THREE_D
			if (contentMode == CONTENT_VECTOR) // 3-vector mode
				table.init(&displayVectorMatrix[0][0], 3, 3);
			else // homogeneous modes
				table.init(&displayFullMatrix[0][0], 4, 4);
# else
			if (contentMode == CONTENT_VECTOR) // 2-vector mode
				table.init(&displayVectorMatrix[0][0], 2, 2);
			else // homogeneous modes
				table.init(&displayFullMatrix[0][0], 3, 3);
# endif
		}
		// Set up the constant vector for vector mode even if it is not needed
		double constant[3];
# if THREE_D
		constant[0] = displayFullMatrix[0][3];
		constant[1] = displayFullMatrix[1][3];
		constant[2] = displayFullMatrix[2][3];
		cvec.init(constant, 1, 3);
# else
		constant[0] = displayFullMatrix[0][2];
		constant[1] = displayFullMatrix[1][2];
		cvec.init(constant, 1, 2);
# endif
		matrixChanged = false;
	}

	// Layout the table for the modelview matrix which must already have been set.
	tableTop = Transform::layout(mode, x, y, w);
	table.layout(tableWidth, tableHeight);
	bottom = tableTop - tableHeight - TRANSFORM_MARGIN;
	if (contentMode == CONTENT_EQUATIONS) {
		tableLeft = x + (w - tableWidth) * 0.5;
		return bottom;
	}

	cvec.layout(cvecWidth, cvecHeight); // Layout the constant vector whether used or not

	// Layout of the left vector
	if (isWorld)
		leftvectorwidth = lworldWidth;
	else
		leftvectorwidth = lvecWidth;

	// If content is transformation then
	// show the equation form (x,y) = T() R()... (x,y)
	// This means substituting the transformation for the table.
	// The calculated left position of the table will be the calculated left position
	// of the transformation text.  The remaining positions are calculated by
	// substituting the width of the transformation text for the width of the table.
	if (contentMode == CONTENT_TRANSFORMATION) {
		tableWidth = transformationWidth;
	}
	if (contentMode == CONTENT_HOMOGENEOUS_MATRIX && mode == SHOW_SYMBOLS) {
		// Substitute the transformationTable for the main table
		tableWidth = transformationWidth;
		tableHeight = transformationHeight; // They should be the same anyhow.
	}

	double width = leftvectorwidth + ltextWidth + tableWidth + rtextWidth + rvecWidth;
	if (contentMode == CONTENT_VECTOR) // 2-vector has added constant vector term at the end
		width += ctextWidth + cvecWidth;

	lvecworldLeft = x + (w - width) * 0.5;
	ltextLeft = lvecworldLeft + leftvectorwidth;
	tableLeft = ltextLeft + ltextWidth;
	rtextLeft = tableLeft + tableWidth;
	rvecLeft = rtextLeft + rtextWidth;
	ctextLeft = rvecLeft + rvecWidth;
	cvecLeft = ctextLeft + ctextWidth;
	midBaseline = tableTop - tableHeight * 0.5 - fontAbove(font) * 0.5;
	return bottom;
}

void ModelView::apply(void)
{
	double localMatrix[4][4];

	// Apply: when apply is called for modelview it means fetch the modelview matrix ready to display it.
	glGetDoublev(GL_MODELVIEW_MATRIX, &localMatrix[0][0]);

	// Check to see whether it has changed from the last time we looked at it.
	if (validMatrix) {
		for (int r = 0; r < 4; r++) {
			for (int c = 0; c < 4; c++) {
				if (localMatrix[r][c] != fullMatrix[r][c]) {
					matrixChanged = true;
					break;
				}
			}
		}
	}
	else
		matrixChanged = true;

	if (matrixChanged) {
		for (int r = 0; r < 4; r++) {
			for (int c = 0; c < 4; c++) {
				fullMatrix[r][c] = localMatrix[r][c];
# if THREE_D
				displayFullMatrix[c][r] = fullMatrix[r][c];
				if (r < 3 && c < 3) {
					displayVectorMatrix[c][r] = fullMatrix[r][c];
				}
# else
				if (r != 2 && c != 2) {
					int dr, dc;
					dr = r < 2 ? r : r-1;
					dc = c < 2 ? c : c-1;
					// There is an explicit transpose happening here because
					// OpenGL stores the matrix in the form for post multiplying with
					// a row vector, but we display as premultiply with column vector
					displayFullMatrix[dc][dr] = fullMatrix[r][c];
				}
				if (r < 2 && c < 2) {
					displayVectorMatrix[c][r] = fullMatrix[r][c];
				}
# endif
			}
		}
	}

	validMatrix = true;
}

bool ModelView::event(int event, int button, int state, double x, double y)
{
	Transform::event(event, button, state, x, y);
	if (event == MENU_ITEM && button == MENU_EQUATION) {
		contentMode = CONTENT_EQUATIONS;
		matrixChanged = true;
		glutPostRedisplay();
	}
	if (event == MENU_ITEM && button == MENU_HOMOGENEOUS) {
		contentMode = CONTENT_HOMOGENEOUS_MATRIX;
		matrixChanged = true;
		glutPostRedisplay();
	}
	if (event == MENU_ITEM && button == MENU_VECTOR) {
		contentMode = CONTENT_VECTOR;
		matrixChanged = true;
		glutPostRedisplay();
	}
	if (event == MENU_ITEM && button == MENU_HOMOGENEOUS_EQUATION) {
		contentMode = CONTENT_TRANSFORMATION;
		matrixChanged = true;
		glutPostRedisplay();
	}
	return false;
}


# define ABS(a) ((a)<0.0?-(a):(a))

# if THREE_D
static void AedToXyz(double &x, double &y, double &z, double &upx, double &upy, double &upz,
		double az, double el, double d)
{
	// Convert azimuth and elevation to x y z.
	x = sin(az / 180.0 * PI) * cos(el / 180.0 * PI) * d;
	y = sin(el / 180.0 * PI) * d;
	z = cos(az / 180.0 * PI) * cos(el / 180.0 * PI) * d;
	// Slight round-off errors in the calculations make the view at +/-90 degrees actually work.
	// OpenGL cannot handle viewing the point (0,0,0) from (0,1,0) with up direction of (0,1,0)
	// For display purposes, zero values should be exact.
	if (ABS(x) < 0.001) x = 0.0;
	if (ABS(y) < 0.001) y = 0.0;
	if (ABS(z) < 0.001) z = 0.0;
	// Fix the up vector at 90 degrees elevation
	if (x == 0.0 && z == 0.0) {
		upy = 0.0;
		upx = -sin(az / 180.0 * PI) * y;
		upz = -cos(az / 180.0 * PI) * y;
	}
	else {
		upx = upz = 0.0;
		upy = 1.0;
	}
}

// The View object in the transform list controls the camera viewpoint for 3D viewing.
View::View()
{
	// A reasonably pleasant view is found using default azimuth and
	// elevation of 23 degrees and 21 degrees
	sliderA.init("Azimuth", ROTATE_MIN, ROTATE_MAX, 90.0, 1.0, 23.0);
	sliderE.init("Elevation", -90.0, 90.0, 45.0, 1.0, 21.0);
	distance = 7.0;
	animate = false;
	animRotate = 0.0;
	animStep = 0.2;
	oscillate = true;
	oscStep = 360.0 / 2.0 / 60.0; // One complete oscillation every 2 seconds.
	oscPos = 0.0;
	oscMax = 5.0;
	return;
}

void View::apply()
{
	double x,y,z, upx, upy, upz;
	AedToXyz(x, y, z, upx, upy, upz, sliderA.value() + animRotate, sliderE.value(), distance);
	gluLookAt(x,y,z, 0,0,0, upx, upy, upz);
	return;
}

bool View::event(int event, int button, int state, double x, double y)
{
	Transform::event(event, button, state, x, y);

	if (event == TIMER_EVENT && animate) {
		if (oscillate) {
			oscPos += oscStep;
			if (oscPos > 360.0) oscPos -= 360.0;
			animRotate = sin(oscPos / 180.0 * PI) * oscMax;
		}
		else {
			animRotate += animStep;
			if (animRotate > 360.0) animRotate -= 360.0;
		}
		glutPostRedisplay(); // Animation update
	}

	if (event == MENU_ITEM && button == MENU_ANIMATE) {
		animate = ! animate;
		animRotate = 0.0;
		oscPos = 0.0; // Reset
		glutPostRedisplay();
	}
	if (event == MENU_ITEM && button == MENU_ANIMATE_OSCILLATE) {
		if (! animate) animate = true;
		oscillate = ! oscillate;
		oscPos = 0.0;
		glutPostRedisplay();
	}

	// If in compact mode and widget is not selected, then don't pass events to sliders.
	if (! isSelected && compactMode) return false;

	// Try each mouse responding component in turn.  If one responds, then don't try the others
	// in case there is some slight overlap.
	sliderA.event(event, button, state, x, y);
	sliderE.event(event, button, state, x, y);
	if (resetApplies(event, button)) {
		sliderA.reset();
		sliderE.reset();
	}
	return false;
}

double View::layout(int mode, double x, double y, double w)
{
	y = Transform::layout(mode, x, y, w);
	x += TRANSFORM_MARGIN; // Left margin
	w -= 2*TRANSFORM_MARGIN; // Right margin
	// In compact mode, when not selected (i.e. not hovered over) omit the sliders.
	if (isSelected || ! compactMode) {
		y = sliderA.layout(x, y, SLIDER_WIDTH);
		y = sliderE.layout(x, y, SLIDER_WIDTH);
	}
	y -= TRANSFORM_MARGIN; // Bottom margin of the TRANSFORM box
	bottom = y;
	return y;
}

void View::draw(void)
{
	char buffer[200];
	double x, y, z, upx, upy, upz;
	Transform::draw();
	AedToXyz(x, y, z, upx, upy, upz, sliderA.value(), sliderE.value(), distance);
	sprintf (buffer, "gluLookAt(%.2g, %.2g, %.2g,  0, 0, 0,  %.2g, %.2g, %.2g)", x, y, z,
			upx, upy, upz);
	glColor3f(TRANSFORM_TEXT_COLOUR);
	drawText(TRANSFORM_FONT, std::string(buffer), left+TRANSFORM_MARGIN, textBaseline);
	sprintf (buffer, "Azimuth = %.3g", sliderA.value());
	sliderA.setLabel(std::string(buffer));
	sprintf (buffer, "Elevation = %.3g", sliderE.value());
	sliderE.setLabel(std::string(buffer));
	// In compact mode, when not selected (i.e. not hovered over) omit the sliders.
	if (isSelected || ! compactMode) {
		sliderA.draw();
		sliderE.draw();
	}
	return;
}

std::string View::name(int mode)
{
	return "";
}
# endif


# define TRANSFORMERS_BACKGROUND 0.3,0.3,0.3
# define TRANSFORMERS_MARGIN 10

TransformList::TransformList()
{
	n = 0;
	left = right = top = bottom = 0.0;
	showMode = SHOW_NUMBERS;
	push_at_end = true; // Default is left to right mode.

# if THREE_D
	push(new View());
# endif
}

// relayout is called every time GLUT calls display, but also every time the
// list of widgets is updated.
double TransformList::relayout()
{
	double x, y, w;
	int count[IDENTIFY_LAST+1], subscript[IDENTIFY_LAST+1];
	char str[100];
	std::string transformation = "";
	// Initialise arrays used to count instances of each type of widget, and to assign unique subscripts to them
	for (int i = IDENTIFY_FIRST; i <= IDENTIFY_LAST; i++) {
		count[i] = 0;
		subscript[i] = 0;
	}
	// Pass through the list, counting instances of each type and mark the last item in the list
	for (int i = 0; i < n; i++) {
		count[list[i]->identify()]++; // Count instances of particular identification
		list[i]->setLast(i >= n-1); // Tell item whether it is last or not.
	}
	// Pass through the list, assigning subscripts and fetching names.
	for (int i = 0; i < n; i++) {
		int id = list[i]->identify();
		if (count[id] > 1) {
			// Assign a subscript to this instance.
			subscript[id]++;
			sprintf (str, "%d", subscript[id]);
			list[i]->setSubscript(std::string(str));
		}
		else
			list[i]->setSubscript("");
		// Accumulate the name string - the transformation equation
		transformation = join(transformation, " ", list[i]->name(showMode));
		// Offer the transformation equation to the widget for display
		list[i]->setTransformation(transformation);
	}
	x = left;
	y = top;
	w = right - left;
	y -= TRANSFORMERS_MARGIN;
	x += TRANSFORMERS_MARGIN;
	w -= 2*TRANSFORMERS_MARGIN;
	for (int i = 0; i < n; i++) {
		y = list[i]->layout(showMode, x, y, w);
		y -= TRANSFORMERS_MARGIN;
	}
	bottom = y;
	return y;
}

double TransformList::layout(double x, double y, double w)
{
	left = x;
	right = x + w;
	top = y;
	return TransformList::relayout();
}

void TransformList::draw(void)
{
	// First, fill a rectangle as the background
	glColor3f(TRANSFORMERS_BACKGROUND);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glRectd(left, 0.0, right, top); // Draw all the way to the bottom of the screen even if not all used.

	for (int i = 0; i < n; i++) {
		list[i]->draw();
	}
}

bool TransformList::event(int event, int button, int state, double x, double y)
{
	bool dolayout = false;
	// Check whether the show mode is being changed
	if (event == MENU_ITEM && button == MENU_SHOW_NUMBERS) {
		showMode = SHOW_NUMBERS;
		glutPostRedisplay();
	}
	if (event == MENU_ITEM && button == MENU_SHOW_SYMBOLS) {
		showMode = SHOW_SYMBOLS;
		glutPostRedisplay();
	}
	if (event == MENU_ITEM && button == MENU_LEFT_TO_RIGHT) {
		push_at_end = true;
	}
	if (event == MENU_ITEM && button == MENU_RIGHT_TO_LEFT) {
		push_at_end = false;
	}
	// Handle menu event (or keyboard shortcut) to move the selected object in the list.
	// After the move, something will trigger changing the selection, so we make it happen
	// immediately by faking passive mouse motion.
	if (event == MENU_ITEM) {
		for (int i = 0; i < n; i++) {
			if (list[i]->selected()) {
				// Note: Swap operations may move the View widget, but that is fixed below.
				if (button == MENU_UP && i > 0) {
					swap(list[i], list[i-1]);
					dolayout = true;
					break;
				}
				else if (button == MENU_DOWN && i < n-1) {
					swap(list[i], list[i+1]);
					dolayout = true;
					break;
				}
				// Do not delete the View widget.
				else if (button == MENU_DELETE && list[i]->identify() != IDENTIFY_VIEW) {
					delete list[i];
					for ( ; i < n-1; i++) {
						list[i] = list[i+1];
					}
					n--;
					dolayout = true;
					break;
				}
			}
		}
	}
	// Handle events that are not dependent on a selection
	if (event == MENU_ITEM) {
		if (button == MENU_DELETE_ALL) {
			int newn = 0;
			for (int i = 0; i < n; i++) {
				if (list[i]->identify() == IDENTIFY_VIEW && newn == 0) {
					list[newn++] = list[i]; // Do not delete this item.
				}
				else {
					delete list[i];
				}
			}
			n = newn;
			dolayout = true;
		}
		else if (button == MENU_TRANSLATE) {
			// Add a translate operation
			push(new Translate());
			dolayout = true;
		}
		else if (button == MENU_ROTATE_X) {
			// Add a rotate operation
			push(new Rotate('x'));
			dolayout = true;
		}
		else if (button == MENU_ROTATE_Y) {
			// Add a rotate operation
			push(new Rotate('y'));
			dolayout = true;
		}
		else if (button == MENU_ROTATE_Z) {
			// Add a rotate operation
			push(new Rotate('z'));
			dolayout = true;
		}
		else if (button == MENU_SCALE) {
			// Add a scale operation
			push(new Scale());
			dolayout = true;
		}
# if ! THREE_D
		// Shear is not supported in 3D mode.
		else if (button == MENU_SHEAR) {
			// Add a shear operation
			push(new Shear());
			dolayout = true;
		}
# endif
		else if (button == MENU_MODELVIEW) {
			// Add a model view matrix viewer
			// First check whether one is already present - only allow one in the entire set
			bool found = false;
			for (int i = 0; i < n; i++)
				if (list[i]->identify() == IDENTIFY_MODELVIEW)
					found = true;
			if (! found)
				pushEnd(new ModelView());
			dolayout = true;
		}
	}

	// Fix up if it happens that the View widget has been moved from the top position.
	// Could be because swap has been applied or because push inserted at the top.
	if (n >= 2 && list[1]->identify() == IDENTIFY_VIEW) {
		swap(list[0], list[1]);
		dolayout = true;
	}

	// If the list has been updated, then update the selection properly.
	if (dolayout) {
		// The list has been changed.
		// Re-do the layout to compute the new coordinates and then
		// compute the new selection based on the computed mouse coordinates.
		relayout();
		glutPostRedisplay();
	}
	for (int i = 0; i < n; i++) {
		list[i]->event(event, button, state, x, y);
	}
	return false;
}

void TransformList::apply(void)
{
	for (int i = 0; i < n; i++) {
		// HACK for 3D mode.
		// This hack makes the ModelView widget see the transformation without the view matrix.
		if (list[i]->identify() != IDENTIFY_VIEW)
			list[i]->apply();
	}
	return;
}

bool TransformList::apply_selection(void)
{
	int selection = -1;
	// First find out whether there is a selection
	for (int i = 0; i < n; i++) {
		if (list[i]->selected() && list[i]->identify() != IDENTIFY_VIEW)
			selection = i;
	}
	if (selection < 0) return false;
	for (int i = 0; i <= selection; i++) {
		// HACK for 3D mode.
		// This hack makes the ModelView widget see the transformation without the view matrix.
		if (list[i]->identify() != IDENTIFY_VIEW)
			list[i]->apply();
	}
	return true;
}

void TransformList::apply_view(void)
{
	if (n > 0 && list[0]->identify() == IDENTIFY_VIEW)
		list[0]->apply();
	return;
}
