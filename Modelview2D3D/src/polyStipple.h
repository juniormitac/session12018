/*
 * polyStipple.h
 *
 *  Created on: 24 Mar 2015
 *      Author: Len Hamey
 */

#ifndef POLYSTIPPLE_H_
#define POLYSTIPPLE_H_

void polyStipple(float factor, int phase);



#endif /* POLYSTIPPLE_H_ */
