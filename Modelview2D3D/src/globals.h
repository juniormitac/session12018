/*
 * globals.h
 *
 *  Created on: Mar 17, 2017
 *      Author: Len Hamey
 *  This file defines the global variables for Modelview2D3D
 */

#ifndef GLOBALS_H_
#define GLOBALS_H_

// Class representing a point in 2D, using double precision.
class DoublePoint
{
public:
	double x,y;

	DoublePoint(double _x, double _y) {
		x = _x;
		y = _y;
	}

	DoublePoint() {
		x = 0;
		y = 0;
	}
};



// Global variables with initialisation and support methods.
class Globals {
public:
	// Track the screen coordinates.  This is the coordinate range of the screen window in pixels.
	DoublePoint screenBegin, screenEnd; // Screen begin and end

	// Track the world that we want to draw inside
	DoublePoint worldBegin, worldEnd; // World begin and end

	// Track the world that is being used for mouse motion
	DoublePoint mouseWorldBegin, mouseWorldEnd;
	bool useMouseWorld;

	// Track the starting point for motion of the world
	DoublePoint mouseStart;
	bool mouseResponding; // Track when mouse is responding to motion in the world

	double worldWidth; // The half-width of the world unzoomed

	// Track the amount of space required for the information panel.
	double panelWidth;

	// Menus
	int menuWorld, menuPanel, menuTarget;

	// Zoom the world
	int zoomIndex;
	double zoomFactor;

	bool shadows; // Enable/disable shadows of 3D objects.
	bool object_axes; // Enable/disable axes on 2D objects.

	Globals() {
		worldBegin = DoublePoint(-10,-10);
		worldEnd = DoublePoint(10,10);
		worldWidth = 10;
		mouseStart = DoublePoint(0,0);
		mouseWorldBegin = worldBegin;
		mouseWorldEnd = worldEnd;
		mouseResponding = false;
		panelWidth = 500;
		screenBegin = DoublePoint(0,0);
		screenEnd = DoublePoint(1000,600);
		menuTarget = menuWorld = menuPanel = -1;
		zoomIndex = 0;
		zoomFactor = 1.0;
		useMouseWorld = false;
		shadows = true;
		object_axes = true; // Default is left-to-right mode
	}

	// ***************************
	// Convert mouse coordinates to world coordinates
	// using our knowledge of world and viewport ranges
	DoublePoint mouseToWorld(int x, int yc) {
		double wx, wy;

		if (! useMouseWorld) {
			// Use the world coordinates for the transform
			mouseWorldBegin = worldBegin;
			mouseWorldEnd = worldEnd;
		}

		// x on (screenBegin.x,screenEnd.x-panelWidth) is mapped to (mouseWorldBegin.x,mouseWorldEnd.x)
		wx = ((double)x - screenBegin.x) * (mouseWorldEnd.x - mouseWorldBegin.x) / (screenEnd.x - panelWidth - screenBegin.x) + mouseWorldBegin.x;

		// y on (screenEnd.y,screenBegin.y) is mapped to (mouseWorldBegin.y,mouseWorldEnd.y)
		// Note: The upside-down use of Y in mouse coordinates is
		// included in this formula
		wy = (screenEnd.y - (double)yc) * (mouseWorldEnd.y - mouseWorldBegin.y) / (screenEnd.y - screenBegin.y) + mouseWorldBegin.y;
		return DoublePoint(wx, wy);
	}

	DoublePoint mouseToPanel(int x, int yc) {
		// Convert mouse coordinates to panel coordinates (in pixels) for accessing controls.
		double px, py;

		px = x;
		py = screenEnd.y - yc;

		return DoublePoint(px, py);
	}

	bool mouseInPanel(int x, int yc) {
		// Return whether mouse is in the panel or not
		return x >= screenEnd.x - panelWidth;
	}

	bool mouseInWorld(int x, int yc) {
		// Return whether mouse is in the world
		return x < screenEnd.x - panelWidth;
	}

	// Set up the projection matrix for the panel
	// NOTE: There seems to be a bug in glGet GL_CURRENT_RASTER_POSITION that it returns screen coordinates even though it says it
	// returns world coordinates.  So, I set up the projection matrix for the panel to map exactly to viewport coordinates.
	// That way, viewport coordinates and world coordinates are the same, so the bug makes no difference.
	void projectPanel(void) {
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluOrtho2D(screenEnd.x - panelWidth, screenEnd.x, screenBegin.y, screenEnd.y);
		glViewport(screenEnd.x-panelWidth, screenBegin.y, panelWidth, screenEnd.y - screenBegin.y);
	}

	// Set up the projection matrix to access the entire screen window
	void projectScreen(void) {
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluOrtho2D(screenBegin.x, screenEnd.x, screenBegin.y, screenEnd.y);
		glViewport(screenBegin.x, screenBegin.y, screenEnd.x-screenBegin.x, screenEnd.y-screenBegin.y);
	}

	// A margin in pixels left around the drawing of the world
# define WORLD_MARGIN 10

	// Set up the projection matrix for the world.  The viewport for the world is a square centred in the left side of the screen window.
	// A small margin of pixels is left around the world viewport for cosmetic purposes.
	void projectWorld(void) {
		double margin;
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		// The edge grid lines can disappear due to roundoff, so add a small margin inside the world.
		margin = (worldEnd.x - worldBegin.x) * 0.001;
# if THREE_D
		// We need a wide range of depth (from near to far) because zooming is implemented by making the world range
		// of X and Y smaller but we still want a range of Z sufficient to include the entire scene.
		glOrtho(worldBegin.x-margin,worldEnd.x+margin,worldBegin.y-margin,worldEnd.y+margin,-20.0*(worldEnd.x-worldBegin.x),20.0*(worldEnd.x-worldBegin.x));
# else
		gluOrtho2D(worldBegin.x-margin,worldEnd.x+margin,worldBegin.y-margin,worldEnd.y+margin);
# endif

		// The world is square, so start by finding the largest square viewport available.
		// Allow unused space WORLD_MARGIN all around the viewport, for "cosmetic" purposes
		int width = screenEnd.x - screenBegin.x - panelWidth - 2*WORLD_MARGIN;
		int height = screenEnd.y - screenBegin.y - 2*WORLD_MARGIN;
		if (height > width) {
			// The viewport is centered vertically
			// The height of the viewport is the same as the width
			int centreMargin = (height - width) / 2;
			glViewport(WORLD_MARGIN, WORLD_MARGIN + centreMargin, width, width);
		}
		else {
			// Centre the viewport horizontally
			int centreMargin = (width - height) / 2;
			glViewport(WORLD_MARGIN + centreMargin, WORLD_MARGIN, height, height);
		}
	}

} glob;




#endif /* GLOBALS_H_ */
