/*
 * drawing.h
 *
 *  Created on: 24 Mar 2015
 *      Author: Len Hamey
 */

#ifndef DRAWING_H_
#define DRAWING_H_

# include "Target.h"
# include "Transform.h"

# define STARTING_OBJECT_COLOUR 1.0,0.0,0.0
# define REFERENCE_OBJECT_COLOUR 0.0,0.0,0.0
# define TARGET_OBJECT_COLOUR 0.0,0.8,0.0
# define SELECT_OBJECT_COLOUR 0.0,0.0,1.0
# define FINAL_OBJECT_COLOUR 1.0,0.0,0.0


void house(float red, float green, float blue, int phase, bool shadow, bool axes);
void aircraft(float red, float green, float blue, int phase, bool shadow, bool axes);

// Draw the sketch in specified colour.
// Shadow option indicates that what is being drawn is a shadow not the object itself.
void sketch(float red, float green, float blue, int phase, bool shadow, bool axes);

// Draw the reference grid.
void drawGrid(TransformList trans, double width);

// Draw the reference axes.
void drawAxes(TransformList trans, double width);

// Draw the starting view of the object.
void drawStarting(Target target, TransformList trans, bool axes);

// Draw the reference object
void drawReference(TransformList trans, bool axes);

// Draw the target object
void drawTarget(Target target, TransformList trans, int phase, bool doShadows, bool axes);

// Draw the current selection view of the object
void drawSelection(Target target, TransformList trans, int phase, bool doShadows, bool axes);

// Draw the final view of the object
void drawFinal(Target target, TransformList trans, int phase, bool doShadows, bool axes);

#endif /* DRAWING_H_ */
