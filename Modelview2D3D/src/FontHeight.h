// Font heights:
// The following data is found by browsing the source code at
// http://sourcecodebrowser.com/freeglut/2.6.0/freeglut__font__data_8c_source.html
// where the height of each font can be found as below.
// Also, the Y offset of each character is recorded as below (the drop below the baseline)

// March 2015, Len Hamey

#include <gl/glut.h>
#include <string>

# define HEIGHT_GLUT_BITMAP_TIMES_ROMAN_10 14
# define HEIGHT_GLUT_BITMAP_TIMES_ROMAN_24 29
# define HEIGHT_GLUT_BITMAP_HELVETICA_10 14
# define HEIGHT_GLUT_BITMAP_HELVETICA_12 16
# define HEIGHT_GLUT_BITMAP_HELVETICA_18 23
# define HEIGHT_GLUT_BITMAP_8_BY_13 14
# define HEIGHT_GLUT_BITMAP_9_BY_15 16

# define BELOW_GLUT_BITMAP_TIMES_ROMAN_10 4
# define BELOW_GLUT_BITMAP_TIMES_ROMAN_24 7
# define BELOW_GLUT_BITMAP_HELVETICA_10 3
# define BELOW_GLUT_BITMAP_HELVETICA_12 4
# define BELOW_GLUT_BITMAP_HELVETICA_18 5
# define BELOW_GLUT_BITMAP_8_BY_13 3
# define BELOW_GLUT_BITMAP_9_BY_15 4

// value of ABOVE are computed from HEIGHT and BELOW
# define ABOVE_GLUT_BITMAP_TIMES_ROMAN_10 (HEIGHT_GLUT_BITMAP_TIMES_ROMAN_10-BELOW_GLUT_BITMAP_TIMES_ROMAN_10)
# define ABOVE_GLUT_BITMAP_TIMES_ROMAN_24 (HEIGHT_GLUT_BITMAP_TIMES_ROMAN_24-BELOW_GLUT_BITMAP_TIMES_ROMAN_24)
# define ABOVE_GLUT_BITMAP_HELVETICA_10 (HEIGHT_GLUT_BITMAP_HELVETICA_10-BELOW_GLUT_BITMAP_HELVETICA_10)
# define ABOVE_GLUT_BITMAP_HELVETICA_12 (HEIGHT_GLUT_BITMAP_HELVETICA_12-BELOW_GLUT_BITMAP_HELVETICA_12)
# define ABOVE_GLUT_BITMAP_HELVETICA_18 (HEIGHT_GLUT_BITMAP_HELVETICA_18-BELOW_GLUT_BITMAP_HELVETICA_18)
# define ABOVE_GLUT_BITMAP_8_BY_13 (HEIGHT_GLUT_BITMAP_8_BY_13-BELOW_GLUT_BITMAP_8_BY_13)
# define ABOVE_GLUT_BITMAP_9_BY_15 (HEIGHT_GLUT_BITMAP_9_BY_15-BELOW_GLUT_BITMAP_9_BY_15)

int fontHeight(void *font);
int fontAbove(void *font);
int fontBelow(void *font);
void *fontSubscript(void *font);
int dropSubscript(void *font);
int textWidth(void *font, std::string text);
void drawText(void *font, std::string text, float x, float y, float heightScale = 1.0);
