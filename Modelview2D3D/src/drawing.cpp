/*
 * drawing.cpp
 *
 *  Created on: 24 Mar 2015
 *      Author: Len Hamey
 */

# include <gl/glut.h>
# include "drawing.h"
# include "polyStipple.h"
# include "Transform.h"

void sketch(float red, float green, float blue, int phase, bool shadow, bool axes)
{
# if THREE_D
	aircraft(red, green, blue, phase, shadow, axes);
# else
	house(red, green, blue, phase, shadow, axes);
# endif
}

# define AXIS_LENGTH 1.5
# define HEAD_LENGTH 0.3
# define HEAD_WIDTH 0.25

// 2D house drawing with Axes
void house(float red, float green, float blue, int phase, bool shadow, bool axes)
{
	if (shadow) return; // No shadows drawn in 2D mode
	// The house
	glColor3f(red, green, blue);
	glLineWidth(1.0);
	glBegin(GL_LINE_LOOP);
		glVertex2d(0,0);
		glVertex2d(1,0);
		glVertex2d(1,1);
		glVertex2d(0.5,1.3);
		glVertex2d(0,1);
	glEnd();
# if !QUIZ_SKETCH
	// A door on left side
	glBegin(GL_LINE_LOOP);
		glVertex2d(0.15,0);
		glVertex2d(0.35,0);
		glVertex2d(0.35,0.6);
		glVertex2d(0.15,0.6);
	glEnd();
	// A window on the right side
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glRectd(0.5,0.3,0.8,0.6);
	glBegin(GL_LINES);
		glVertex2d(0.5,0.45);
		glVertex2d(0.8,0.45);
		glVertex2d(0.65,0.3);
		glVertex2d(0.65,0.6);
	glEnd();
	// The axes
	if (axes) {
		glLineWidth(2.0);
		glBegin(GL_LINES);
			// Y axis
			glVertex2d(0.0, 0.0);
			glVertex2d(0.0, AXIS_LENGTH);
			glVertex2d(0.0, AXIS_LENGTH);
			glVertex2d(-HEAD_WIDTH*0.5, AXIS_LENGTH-HEAD_LENGTH);
			glVertex2d(0.0, AXIS_LENGTH);
			glVertex2d(HEAD_WIDTH*0.5, AXIS_LENGTH-HEAD_LENGTH);
			// X axis
			glVertex2d(0.0, 0.0);
			glVertex2d(AXIS_LENGTH, 0.0);
			glVertex2d(AXIS_LENGTH, 0.0);
			glVertex2d(AXIS_LENGTH-HEAD_LENGTH, -HEAD_WIDTH*0.5);
			glVertex2d(AXIS_LENGTH, 0.0);
			glVertex2d(AXIS_LENGTH-HEAD_LENGTH, HEAD_WIDTH*0.5);
		glEnd();
	}
# endif
}

# define PASTEL 0.9
# define SHADOW 0.6
// Hack to make the line between tail and wing visible:
// slightly lift the tail off the wing in the Y direction.
// This also ensures that the line is not visible when
// the aircraft is upside down.
# define GAP 0.02

// aircraft drawing function.  axes argument is ignored - we don't show axes of the aircraft itself
void aircraft(float red, float green, float blue, int phase, bool shadow, bool axes)
{
	double p = PASTEL;

	if (shadow) {
		polyStipple(0.25, phase); // Allow objects to be seen through shadows.
		p = SHADOW;
	}
	// Render the aircraft as solid so that intersections of multiple positions are
	// more easily comprehended.
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glColor3f(red*(1.0-p)+p, green*(1.0-p)+p, blue*(1.0-p)+p);
	glBegin( GL_TRIANGLES);
		  glVertex3f(2.0, 0.0, 0.0);
		  glVertex3f(0.0, 0.0, -1.0);
		  glVertex3f(0.0, 0.0, 1.0); /* draw wings */
		  glVertex3f(0.0, GAP, 0.0);
		  glVertex3f(0.5, GAP, 0.0);
		  glVertex3f(0.0, 0.5, 0.0); /* draw tail piece */
	glEnd();

	polyStipple(1.0, 0);

	if (! shadow) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glColor3f(red, green, blue);
		glLineWidth(2.0);
		glBegin( GL_TRIANGLES);
			  glVertex3f(2.0, 0.0, 0.0);
			  glVertex3f(0.0, 0.0, -1.0);
			  glVertex3f(0.0, 0.0, 1.0); /* draw wings */
			  glVertex3f(0.0, GAP, 0.0);
			  glVertex3f(0.5, GAP, 0.0);
			  glVertex3f(0.0, 0.5, 0.0); /* draw tail piece */
		glEnd();
	}
}

void doGrid(double left, double right, double bottom, double top, double increment)
{
# if THREE_D
	double v;
	glBegin(GL_LINES);
		for (v = left; v < right + increment * 0.2; v += increment) {
			glVertex3d(v, 0.0, bottom);
			glVertex3d(v, 0.0, top);
		}
		for (v = bottom; v < top + increment * 0.2; v += increment) {
			glVertex3d(left, 0.0, v);
			glVertex3d(right, 0.0, v);
		}
	glEnd();
# else
	double v;
	glBegin(GL_LINES);
		for (v = left; v < right + increment * 0.2; v += increment) {
			glVertex2d(v, bottom);
			glVertex2d(v, top);
		}
		for (v = bottom; v < top + increment * 0.2; v += increment) {
			glVertex2d(left, v);
			glVertex2d(right, v);
		}
	glEnd();
# endif
}

void initView(TransformList trans, bool shadow)
{
	// Initialise the model view matrix to identity
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Apply the view transformation
	trans.apply_view();

	// If preparing to draw shadow, scale Y to 0.0 in the world coordinates
	if (shadow) {
		glScaled(1.0, 0.0, 1.0);
	}

	return;
}

# define INITIAL_AXES_COLOUR 0.5,0.5,0.5
# define GRID_COLOUR 0.7,1.0,0.7

void drawGrid(TransformList trans, double width)
{
	// Initialise the model view matrix
	initView(trans, false);

	// Draw a grid
	glColor3f(GRID_COLOUR);
	glLineWidth(1.0);
	doGrid(-width, width, -width, width, 1.0);

	return;
}

// Draw the world axes.
void drawAxes(TransformList trans, double width)
{
	// Initialise the model view matrix
	initView(trans, false);

	// Draw the axes
	glColor3f(INITIAL_AXES_COLOUR);
	glLineWidth(1.0);
	glBegin(GL_LINES);
		glVertex3d(-width,0.0,0.0);
		glVertex3d(0.0,0.0,0.0);
		glVertex3d(0.0,-width,0.0);
		glVertex3d(0.0,0.0,0.0);
		glVertex3d(0.0,0.0,-width);
		glVertex3d(0.0,0.0,0.0);
	glEnd();

	// Draw the positive X axis in red, positive Y axis in green and positive Z axis in blue.
	// The alternative would be to label the axes, but animation of label text would be untidy.
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_LINES);
		glVertex3d(0.0,0.0,0.0);
		glVertex3d(width,0.0,0.0);
	glEnd();
	glColor3f(0.0, 0.8, 0.0);
	glBegin(GL_LINES);
		glVertex3d(0.0,0.0,0.0);
		glVertex3d(0.0,width,0.0);
	glEnd();
	glColor3f(0.0, 0.0, 1.0);
	glBegin(GL_LINES);
		glVertex3d(0.0,0.0,0.0);
		glVertex3d(0.0,0.0,width);
	glEnd();
}


void drawStarting(Target target, TransformList trans, bool axes)
{
	// HACK!  This feature means that the modelview matrix will not actually
	// be the modelview matrix, it is just the user transformation when the starting
	// position is transformed from the identity.
	// It is not even the model matrix.

	// Initialise the model view matrix
	initView(trans, false);

	// Draw the sketch in the initial position
	// It is drawn red but stippled so that when the user applies transformation,
	// the original position is left behind as a "ghost".  It has no shadow in 3D.
	target.applyInit();
	glEnable(GL_LINE_STIPPLE);
	glLineStipple(1,0xf0f0);
	sketch(STARTING_OBJECT_COLOUR, 0, false, axes);
	glDisable(GL_LINE_STIPPLE);
	return;
}

void drawReference(TransformList trans, bool axes)
{
	// Initialise the model view matrix
	initView(trans, false);

	// Draw the sketch in the model position
	sketch(REFERENCE_OBJECT_COLOUR, 0, false, axes);
	return;
}

void drawTarget(Target target, TransformList trans, int phase, bool doShadows, bool axes)
{
	// Draw the target model if there is one
	if (! target.isIdentity()) {

		// Initialise the model view matrix
		initView(trans, false);
		target.apply();
		sketch(TARGET_OBJECT_COLOUR, phase, false, axes);

		if (doShadows) {
			// Now draw the shadow.
			initView(trans, true);
			target.apply();
			sketch(TARGET_OBJECT_COLOUR, phase, true, false);
		}
	}

	return;
}

void drawSelection(Target target, TransformList trans, int phase, bool doShadows, bool axes)
{
	// Draw the selection position of the user transformed sketch
	double matrix[16];

	// Initialise the model view matrix
	initView(trans, false);

# if ! THREE_D
	// HACK!  See above.
	target.applyInit(); // The user has to undo the initial transformation
# endif
	glPushMatrix();
	// Compute the user transformation relative to identity so that modelview can display it correctly.
	glLoadIdentity();
	// Try to apply transformation up to the selection
	if (trans.apply_selection()) {
		// Fetch the matrix that we computed.
		glGetDoublev(GL_MODELVIEW_MATRIX, matrix);
		glPopMatrix();
		// Multiply the matrix we computed into the transformation and draw the sketch
		glMultMatrixd(matrix);
		sketch(SELECT_OBJECT_COLOUR, phase, false, axes);

		if (doShadows) {
			// Now draw the shadow.  Use the same matrix that we computed above.
			initView(trans, true);
# if ! THREE_D
			// HACK!  See above.
			target.applyInit(); // The user has to undo the initial transformation
# endif
			glMultMatrixd(matrix);
			sketch(SELECT_OBJECT_COLOUR, phase, true, false);
		}

	}
	else
		glPopMatrix();

	return;
}

void drawFinal(Target target, TransformList trans, int phase, bool doShadows, bool axes)
{
	// Draw the final position of the object
	double matrix[16];

	// Initialise the model view matrix
	initView(trans, false);

# if ! THREE_D
	// HACK!
	target.applyInit(); // The user has to undo the initial transformation
# endif
	glPushMatrix();
	glLoadIdentity();
	// Apply the transformation to the coordinate system
	trans.apply();

	// Fetch the matrix that we computed.
	glGetDoublev(GL_MODELVIEW_MATRIX, matrix);
	glPopMatrix();
	// Multiply the matrix we computed into the transformation and draw the sketch
	glMultMatrixd(matrix);

	// Draw the sketch again - the user final position
	sketch(FINAL_OBJECT_COLOUR, phase, false, axes);

	if (doShadows) {
		// Now draw the shadow.  Use the same matrix that we computed above.
		initView(trans, true);
# if ! THREE_D
		// HACK!  See above.
		target.applyInit(); // The user has to undo the initial transformation
# endif
		glMultMatrixd(matrix);
		sketch(FINAL_OBJECT_COLOUR, phase, true, false);
	}

	return;
}
