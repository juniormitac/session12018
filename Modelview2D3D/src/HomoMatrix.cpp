/*
 * homoMatrix.cpp
 *
 * Create symbolic content of homogeneous matrix from transformation string
 *
 *  Created on: 20 Mar 2015
 *      Author: Len Hamey
 */

# include "HomoMatrix.h"

static std::string transCode(std::string transformation)
{
	char code[100];
	int n = 0;
	for (unsigned int i = 0; i < transformation.length(); i++) {
		char c = transformation[i];
		if (c == 'T' || c == 'S' || c == 'R' || c == 'h')
			code[n++] = c;
	}
	code[n++] = '\0';
	return std::string(code);
}

// The following tables are used for equation generation.
// The first string in each row of the table is a transformation code which indicates what transforms are
// present in the transform list.  The remaining strings are terms in the matrix.
// For example, "R" represents rotation, and the following parameters are the rotation matrix.
// Table 2 is the 2x2 matrices for rotations, scale and shear
// Table 3 expands the equations to include the translation terms.
// _ indicates that the next character is to be displayed as a subscript
// Table 2 is used when the transformation begins with zero, one or two T codes; the table entries must not contain T codes
// Table 3 is used when T codes appear after the beginning and represents the entire code.
static std::string table2[][5] =
{
	{ "", "1", "0", "0", "1" }, // The identity case needs to be matched, otherwise ** is the result
	{ "R", "cos(a)", "-sin(a)", "sin(a)", "cos(a)" },
	{ "Sh", "1", "b_x", "b_y", "1" },
	{ "S", "s_x", "0", "0", "s_y" },
	{ "RR", "cos(a_1+a_2)", "-sin(a_1+a_2)", "sin(a_1+a_2)", "cos(a_1+a_2)" },
	{ "ShSh", "1+b_1_xb_2_y", "b_1_x+b_2_x", "b_1_y+b_2_y", "b_1_yb_2_x+1" },
	{ "SS", "s_1_xs_2_x", "0", "0", "s_1_ys_2_y" },
	{ "RSh", "cos(a)-b_ysin(a)", "b_xcos(a)-sin(a)", "sin(a)+b_ycos(a)", "b_xsin(a)+cos(a)" },
	{ "RS", "s_xcos(a)", "-s_ysin(a)", "s_xsin(a)", "s_ycos(a)" },
	{ "ShS", "s_x", "b_xs_y", "b_ys_x", "s_y" },
	{ "RShS", "s_x(cos(a)-b_ysin(a))", "s_y(b_xcos(a)-sin(a))", "s_x(sin(a)+b_ycos(a))", "s_y(b_xsin(a)+cos(a))" }, // Fits, also TRShS; not TTRShS
	// Non-TraSHeS orderings...
	{ "SSh", "s_x", "s_xb_x", "s_yb_y", "s_y" },
	{ "SR", "s_xcos(a)", "-s_xsin(a)", "s_ysin(a)", "s_ycos(a)" },
	{ "ShR", "cos(a)+b_xsin(a)", "-sin(a)+b_xcos(a)", "b_ycos(a)+sin(a)", "-b_ysin(a)+cos(a)" },
	{ "SShR", "s_x(cos(a)+b_xsin(a))", "s_x(-sin(a)+b_xcos(a))", "s_y(b_ycos(a)+sin(a))", "s_y(-b_ysin(a)+cos(a))" }, // Fits
	{ "ShSR", "s_xcos(a)+b_xs_ysin(a)", "-s_xsin(a)+b_xs_ycos(a)", "b_ys_xcos(a)+s_ysin(a)", "-b_ys_xsin(a)+s_xcos(a)" }, // Fits
	{ "SRSh", "s_x(cos(a)-b_ysin(a))", "s_x(b_xcos(a)-sin(a))", "s_y(sin(a)+b_ycos(a))", "s_y(b_xsin(a)+cos(a)" }, // Fits
	{ "ShRS", "s_x(cos(a)+b_xsin(a))", "s_y(-sin(a)+b_xcos(a))", "s_x(b_ycos(a)+sin(a))", "s_y(-b_ysin(a)+cos(a))" }, // Fits
	{ "RSSh", "s_xcos(a)-s_yb_ysin(a)", "s_xb_xcos(a)-s_ysin(a)", "s_xsin(a)+s_yb_ycos(a)", "s_xb_xsin(a)+s_ycos(a)" }, // Fits, also TRSSh; not TTRSSh
	{ "X", "", "" , "", "" } // End marker - do not use this as a match!
};

// Some entries are too big for the space allocated on the screen by default.  In particular, RTTR!
static std::string table3[][7] =
{
	{ "RT", "cos(a)", "-sin(a)", "t_xcos(a)-t_ysin(a)", "sin(a)", "cos(a)", "t_xsin(a)+t_ycos(a)" },
	{ "RRT", "cos(a_1+a_2)", "-sin(a_1+a_2)", "t_xcos(a_1+a_2)-t_ysin(a_1+a_2)", "sin(a_1+a_2)", "cos(a_1+a_2)", "t_xsin(a_1+a_2)+t_ycos(a_1+a_2)" },
	{ "RTR", "cos(a_1+a_2)", "-sin(a_1+a_2)", "t_xcos(a_1)-t_ysin(a_1)", "sin(a_1+a_2)", "cos(a_1+a_2)", "t_xsin(a_1)+t_ycos(a_1)" },
	{ "RTTR", "cos(a_1+a_2)", "-sin(a_1+a_2)", "(t_1_x+t_2_x)cos(a_1)-(t_1_y+t_2_y)sin(a_1)",
			"sin(a_1+a_2)", "cos(a_1+a_2)", "(t_1_x+t_2_x)sin(a_1)+(t_1_y+t+2+y)cos(a_1)" },
	{ "TRT", "cos(a)", "-sin(a)", "t_1_x+t_2_xcos(a)-t_2_ysin(a)", "sin(a)", "cos(a)", "t_1_y+t_2_xsin(a)+t_2_ycos(a)" },
	{ "ST", "s_x", "0", "s_xt_x", "0", "s_y", "s_yt_y" },
	{ "STS", "s_1_xs_2_x", "0", "s_1_xt_x", "0", "s_1_ys_2_y", "s_1_yt_y" },
	{ "TST", "s_x", "0", "t_1_x+s_xt_2_x", "0", "s_y", "t_1_y+s_yt_2_y" },
	{ "X", "", "", "", "", "", "" }
};

void symbolicMatrix(std::string (&matrix)[3][3], std::string transformation)
{
	std::string code = transCode(transformation);
	std::string tcode = code; // Code with leading translations removed.

	// Initialise the matrix to identity
	matrix[0][0] = matrix[1][1] = matrix[2][2] = "1";
	matrix[0][1] = matrix[0][2] = matrix[1][0] = matrix[1][2] = matrix[2][0] = matrix[2][1] = "0";

	// Recognise translations at the start of the code.
	// If there are two Translations at the start of the code, and no others
	// (because the table does not list any later T code), then represent them easily.
	if (code.length() >= 2 && code[0] == 'T' && code[1] == 'T') {
		matrix[0][2] = "t_1_x+t_2_x";
		matrix[1][2] = "t_1_y+t_2_y";
		tcode = code.substr(2, std::string::npos);
	}
	// If there is one Translation at the start of the code, and no others, then represent it easily.
	else if (code.length() >= 1 && code[0] == 'T') {
		matrix[0][2] = "t_x";
		matrix[1][2] = "t_y";
		tcode = code.substr(1, std::string::npos);
	}

	// Recognise well known patterns for the 2x2 top-left corner
	// These patterns all assume that there is no T terms after the beginning, so
	// that the top-left corner is all that we need to update.
	for (int i = 0; table2[i][0] != "X"; i++) {
		if (tcode == table2[i][0]) {
			// Match!  This completes the matrix construction.
			matrix[0][0] = table2[i][1];
			matrix[0][1] = table2[i][2];
			matrix[1][0] = table2[i][3];
			matrix[1][1] = table2[i][4];
			return;
		}
	}

	// Recognise well known patterns for the top two rows
	for (int i = 0; table3[i][0] != "X"; i++) {
		if (code == table3[i][0]) {
			// Match!  This completes the matrix construction.
			matrix[0][0] = table3[i][1];
			matrix[0][1] = table3[i][2];
			matrix[0][2] = table3[i][3];
			matrix[1][0] = table3[i][4];
			matrix[1][1] = table3[i][5];
			matrix[1][2] = table3[i][6];
			return;
		}
	}

	// If the pattern is a top-left corner that is not recognised, put ** in the corner.
	if (tcode.find('T') == std::string::npos) {
		// There are no T's in the code after the leading T's that we stripped,
		// so it is a top-left corner matrix that we have not coded for
		matrix[0][0] = matrix[0][1] = matrix[1][0] = matrix[1][1] = "**";
		return;
	}

	// If the pattern is not recognised, then put "**" everywhere.
	matrix[0][0] = matrix[0][1] = matrix[0][2] = matrix[1][0] = matrix[1][1] = matrix[1][2] = "**";
	return;
}

