// Demonstrate modelview matrix transformations in 2D or 3D
// (compilation option in Transform.h)
// Authors: Len Hamey
// March 2015
// Update March 2016: Coloured 3D axes; quiz compile mode (in Transform.h); some tidying
// Update March 2017: Comments.
//

#include <gl/glut.h>
#include <math.h>
#include <iostream>
#include <ctime>

#include "Transform.h"
#include "Target.h"
#include "drawing.h"
#include "globals.h"

//using namespace std;


// The global list of transformations that appear in the panel on the right and are applied during drawing of the world.
TransformList trans;
Target target;

// initializations

void init(void)
{

	// sets background colour and drawing colour
	glClearColor(1.0, 1.0, 1.0, 0.0);
	glColor3f(0.0, 0.0, 0.0);

	// ensures polygons drawn in outline mode
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}



// the display callback function
void display(void)
{
	// clears entire screen window to background colour (white)
	glob.projectScreen();
# if THREE_D
	// 3D drawing uses depth buffering for clarity, so clear the depth buffer.
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
# else
	glClear(GL_COLOR_BUFFER_BIT);
# endif

	// Set world projection matrix
	glob.projectWorld();

# if ! THREE_D
	// 2D drawing does not use depth buffering.  If multiple things are drawn
	// at the same place, the last thing drawn is what remains on the screen.
	// The order of drawing here ensures that, if transformed objects occupy
	// the same location then the final transformed object overwrites
	// the current selection which in turn overwrites the
	// model reference, which in turn overwrites the starting position
	//
	// Draw the background grid.
	drawGrid(trans, glob.worldWidth);
	// Draw the axes on the grid.
	drawAxes(trans, glob.worldWidth);
	// Draw the object at a starting position that is not the origin.  This is for
	// sophisticated puzzles where the student has to undo an initial transform and
	// transform to a final position.
	drawStarting(target, trans, glob.object_axes);
	// Draw the original object model at the origin.  Always show object axes on the reference object.
	drawReference(trans, true);
	// Draw the target object when students are trying to solve puzzles
	drawTarget(target, trans, 1, glob.shadows, glob.object_axes);
	// Draw the transformation of the original object corresponding to the current selected
	// transformation in the control panel
	drawSelection(target, trans, 0, glob.shadows, glob.object_axes);
	// Draw the final position of the object
	drawFinal(target, trans, 0, glob.shadows, glob.object_axes);
# else
	// When drawing with depth buffering, draw the thing you most want to see
	// first because new things don't overwrite what is already drawn at the same place
	// with the same depth.
	// If multiple things are drawn at the same place, the first thing drawn
	// is what remains on the screen.
	// Note however that lines and filled polygons can mix in strange ways
	// and this may mean that the line appears dashed through the polygon.
	glEnable(GL_DEPTH_TEST);
	drawAxes(trans, glob.worldWidth);
	drawGrid(trans, glob.worldWidth);
	drawFinal(target, trans, 0, glob.shadows, glob.object_axes);
	// Selection object has the same shadow phase as final.
	// This ensures at least 50% see through of the combined shadow and prevents
	// the shadow looking strange when the final and selection are the same.
	drawSelection(target, trans, 0, glob.shadows, glob.object_axes);
	drawTarget(target, trans, 1, glob.shadows, glob.object_axes);
	// SHow object axes on the reference object (except that no object axes are shown for 3D ever)
	drawReference(trans, true);
	glDisable(GL_DEPTH_TEST);
# endif

	// Set projection matrix for the control panel.
	// Draw the panel after the world because trans.apply has the effect of setting
	// the modelview matrix data into the ModelView transform widget, which then displays it.
	// The panel is drawn in 2D mode.
	glob.projectPanel();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glLineWidth(2);
	// Compute the layout of the transformation information
	trans.layout(glob.screenEnd.x - glob.panelWidth, glob.screenEnd.y, glob.panelWidth);
	// Render the transformations
	trans.draw();

	glFlush();
	glutSwapBuffers();
}

void updateMenus(int x, int yc)
{
	int currentMenu;
	currentMenu = glutGetMenu();
	// Updating menus based on passive mouse motion
	if (glob.mouseInPanel(x, yc)) {
		// Select the panel context menu
		if (currentMenu != glob.menuPanel) {
			glutDetachMenu(GLUT_RIGHT_BUTTON);
			glutSetMenu(glob.menuPanel);
			glutAttachMenu(GLUT_RIGHT_BUTTON);
		}
	}
	else {
		// Select the world context menu
		if (currentMenu != glob.menuWorld) {
			glutDetachMenu(GLUT_RIGHT_BUTTON);
			glutSetMenu(glob.menuWorld);
			glutAttachMenu(GLUT_RIGHT_BUTTON);
		}
	}
}

void limitRange(double &begin, double &end, double minv, double maxv)
{
	if (begin < minv) {
		end += minv - begin;
		begin = minv;
	}
	if (end > maxv) {
		begin -= end - maxv;
		end = maxv;
	}
}

// Process events that occur in or are relevant to the world panel of the screen.
void processWorld(int event, int button, int state, double x, double y, int xm, int ym)
{
	// The zoomFactor array lists the possible zoom factors.
	static double zoomFactor[] = { 1.0, 1.5, 2.0, 3.0, 4.0, 6.0, 8.0 };

	// The menu items MENU_LEFT_TO_RIGHT and MENU_RIGHT_TO_LEFT affect both the globals
	// (for how to draw or not draw axes on transformed objects) and also the transformation list
	// (for how to push new items into the list).  This is a convention that we adopt:
	// when constructing left to right we show axes, when constructing right to left we hide them.
	// when constructing left to right we push items at the bottom, when constructing right to left
	// we push them at the top.
	if (event == MENU_ITEM && button == MENU_LEFT_TO_RIGHT) {
		glob.object_axes = true;
		glutPostRedisplay();
	}
	if (event == MENU_ITEM && button == MENU_RIGHT_TO_LEFT) {
		glob.object_axes = false;
		glutPostRedisplay();
	}
	if (event == MENU_ITEM && button >= MENU_TARGET_FIRST && button <= MENU_TARGET_LAST) {
		// Target generation - easy to handle directly
		target.random(button);
		glutPostRedisplay();
		return;
	}
	if (event == MENU_ITEM && button == MENU_SHADOWS) {
		glob.shadows = ! glob.shadows;
		glutPostRedisplay();
		return;
	}
	if (event == MENU_ITEM && (button == MENU_ZOOM_IN || button == MENU_ZOOM_OUT)) {
		double cx, cy, width;
		// Adjust the zoom.
		if (button == MENU_ZOOM_OUT) glob.zoomIndex--;
		if (button == MENU_ZOOM_IN) glob.zoomIndex++;
		// Restrict the zoom index range to the size of the zoomFactor array.
		if (glob.zoomIndex < 0) glob.zoomIndex = 0;
		if (glob.zoomIndex >= (int) (sizeof(zoomFactor)/sizeof(zoomFactor[0])))
			glob.zoomIndex = (sizeof(zoomFactor)/sizeof(zoomFactor[0]))-1;
		glob.zoomFactor = zoomFactor[glob.zoomIndex];
		// Adjust the world about its current centre.
		cx = (glob.worldBegin.x + glob.worldEnd.x) * 0.5;
		cy = (glob.worldBegin.y + glob.worldEnd.y) * 0.5;
		// Compute the half-width of the world.
		width = glob.worldWidth / glob.zoomFactor;
		// Now set the world bounds
		glob.worldBegin.x = cx - width;
		glob.worldEnd.x = cx + width;
		glob.worldBegin.y = cy - width;
		glob.worldEnd.y = cy + width;
		// limit the range as appropriate
		limitRange(glob.worldBegin.x, glob.worldEnd.x, -glob.worldWidth, glob.worldWidth);
		limitRange(glob.worldBegin.y, glob.worldEnd.y, -glob.worldWidth, glob.worldWidth);
		glutPostRedisplay();
		return;
	}
	if (event == MOUSE_CLICK && glob.mouseInWorld(xm, ym)) {
		if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
			// Press of left mouse button.  Commence moving the world around.
			glob.mouseStart.x = x;
			glob.mouseStart.y = y;
			// Coordinates for mouse to world transformation are frozen during move operation
			glob.useMouseWorld = true;
			glob.mouseWorldBegin = glob.worldBegin;
			glob.mouseWorldEnd = glob.worldEnd;
			glob.mouseResponding = true;
		}
	}
	if (glob.mouseResponding && (event == MOUSE_CLICK || event == MOUSE_MOTION)) {
		// Update world for mouse motion.
		// NOTE: If you drag the mouse to the right then you want the world begin
		// to move to the left so that the picture moves to the right with the mouse.
		glob.worldBegin.x = glob.mouseWorldBegin.x - x + glob.mouseStart.x;
		glob.worldEnd.x = glob.mouseWorldEnd.x - x + glob.mouseStart.x;
		glob.worldBegin.y = glob.mouseWorldBegin.y - y + glob.mouseStart.y;
		glob.worldEnd.y = glob.mouseWorldEnd.y - y + glob.mouseStart.y;
		limitRange(glob.worldBegin.x, glob.worldEnd.x, -glob.worldWidth, glob.worldWidth);
		limitRange(glob.worldBegin.y, glob.worldEnd.y, -glob.worldWidth, glob.worldWidth);
		glutPostRedisplay();
	}
	if (event == MOUSE_CLICK && button == GLUT_LEFT_BUTTON && state == GLUT_UP) {
		// Finished with mouse motion.
		glob.mouseResponding = false;
		// The world update is finalised so use world coordinates again.
		glob.useMouseWorld = false;
	}
}

// All GUI input events are handled through a single procedure call that is then passed to
// the event method of objects.  Each object selects which events it responds to, irrespective
// of where the events originate from.
// Keyboard shortcuts are converted to menu events for processing.
void processEvent(int event, int button, int state, int _x, int _yc)
{
	static int x, yc; // Current mouse coordinates.
	if (event != MENU_ITEM && event != TIMER_EVENT) {
		// The menu item does not provide mouse coordinates so don't update them
		// The timer event also has no mouse coordinates
		// For all other events, the mouse coordinates are updated to match the parameters of the event
		x = _x;
		yc = _yc;
	}
	DoublePoint transmouse = glob.mouseToPanel(x, yc);
	DoublePoint worldmouse = glob.mouseToWorld(x, yc);
	// Keyboard shortcuts are handled by converting the keyboard into a menu item.
	// This means that the object only has to recognise the menu event
	int action = 0;
	if (event == MENU_ITEM) action = button; // Genuine menu events
	if (event == SPECIAL_DOWN) {
		// The arrow keys are shortcuts for the menu items to move the current
		// transformation up or down in the list.
		if (button == GLUT_KEY_UP) action = MENU_UP;
		if (button == GLUT_KEY_DOWN) action = MENU_DOWN;
	}
	if (event == KEYBOARD_DOWN) {
		if (button == 127) action = MENU_DELETE; // 127 is DEL key in ASCII
		if (button == 'd') action = MENU_RESET; // Reset to default
		if (button == 'D') action = MENU_RESET_ALL;
		if (button == 'c') action = MENU_COMPACT;
		if (button == 'C') action = MENU_EXPANDED;
		if (button == 'L') action = MENU_LEFT_TO_RIGHT;
		if (button == 'R') action = MENU_RIGHT_TO_LEFT;
		if (button == 'a') action = MENU_ANIMATE;
		if (button == 'A') action = MENU_ANIMATE_OSCILLATE;
		if (button == 't') action = MENU_TRANSLATE;
# if THREE_D
		if (button == 'x') action = MENU_ROTATE_X;
		if (button == 'y') action = MENU_ROTATE_Y;
		if (button == 'z') action = MENU_ROTATE_Z;
# else
		if (button == 'r') action = MENU_ROTATE_Z;
# endif
		if (button == 's') action = MENU_SCALE;
# if ! THREE_D
		// Shear is not supported in 3D mode
		if (button == 'S') action = MENU_SHEAR;
# endif
		if (button == 'm') action = MENU_MODELVIEW;
		if (button == 'e') action = MENU_EQUATION;
		if (button == 'h') action = MENU_HOMOGENEOUS_EQUATION;
		if (button == 'H') action = MENU_HOMOGENEOUS;
		if (button == 'v') action = MENU_VECTOR;
		if (button == 'n') action = MENU_SHOW_NUMBERS;
		if (button == 'p') action = MENU_SHOW_SYMBOLS;
		if (button == '+') action = MENU_ZOOM_IN;
		if (button == '-') action = MENU_ZOOM_OUT;
		if (button == '0') action = MENU_TARGET_NONE;
		if (button == '1') action = MENU_TARGET_1;
		if (button == '2') action = MENU_TARGET_2;
		if (button == '3') action = MENU_TARGET_3;
		if (button == '4') action = MENU_TARGET_4;
		if (button == '5') action = MENU_TARGET_5;
		if (button == '6') action = MENU_TARGET_6;
		if (button == '7') action = MENU_TARGET_7;
		if (button == '8') action = MENU_TARGET_8;
		if (button == '9') action = MENU_TARGET_9;
# if ! THREE_D
		// These very difficult puzzles involve a transformed starting point
		if (button == '!') action = MENU_TARGET_11;
		if (button == '@') action = MENU_TARGET_12;
		if (button == '#') action = MENU_TARGET_13;
		if (button == '$') action = MENU_TARGET_14;
# endif
	}
	if (action != 0) {
		// The event if either a genuine menu event or a keyboard shortcut so
		// apply the conversion.
		// There is no mouse position associated with menu events but keyboard events
		// have a mouse position which will be associated with menu event.
		// Convert the event
		event = MENU_ITEM;
		button = action;
	}
	// Whole screen event handler - recognising passive motion events and updating the menus
	if (event == MOUSE_PASSIVE || event == MOUSE_MOTION) updateMenus(x, yc);
	// TransformList event handler (panel)
	trans.event(event, button, state, transmouse.x, transmouse.y);
	// World drawing event handler
	processWorld(event, button, state, worldmouse.x, worldmouse.y, x, yc);

}

// All the different event handlers call a single routine processEvent.
// This provides a common path for event processing.  All events are offered to both the
// world and the transform list for possible response.
void processPassiveMotion(int x, int yc)
{
	processEvent(MOUSE_PASSIVE, -1, -1, x, yc);
}

void processMouseMotion(int x, int yc)
{
	processEvent(MOUSE_MOTION, -1, -1, x, yc);
}

void processMouse(int button, int state, int x, int yc)
{
	processEvent(MOUSE_CLICK, button, state, x, yc);
}

void processKey(unsigned char key, int x, int yc)
{
	processEvent(KEYBOARD_DOWN, key, GLUT_DOWN, x, yc);
}

void processSpecialKey(int key, int x, int yc)
{
	processEvent(SPECIAL_DOWN, key, GLUT_DOWN, x, yc);
}

void processMenuEntry(int value)
{
	processEvent(MENU_ITEM, value, -1, -1, -1);
}

// Reshape is handled by recording the new screen size and redisplaying
void reshape(int w, int h)
{
	glob.screenEnd.x = w;
	glob.screenEnd.y = h;
	glViewport(0,0,w,h);
	glutPostRedisplay();
}

// Set up a continuous stream of timer events for animation
void timerFunc(int v)
{
	processEvent(TIMER_EVENT, -1, -1, -1, -1);
	glutTimerFunc(1000/60, timerFunc, 0);
}

// Declares initial window size and position.
// Opens window with "Rubber Rectangle ..." in its title bar.
// Registers callback functions for display and mouse events.
// Enters main loop and waits for mouse events.

int main(int argc, char** argv)
{
	srand(time(0));
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
	// The default window size is tailored to
	// Len's laptop for lecturing purposes.
	glob.screenBegin = DoublePoint(0,0);
	glob.screenEnd = DoublePoint(1300,700);
	glob.panelWidth = 600;
	// Specify the world for the drawing part
	glob.worldWidth = 7.0;
	glob.worldBegin = DoublePoint(-7,-7);
	glob.worldEnd = DoublePoint(7,7);
	glutInitWindowSize(glob.screenEnd.x,glob.screenEnd.y);
	glutInitWindowPosition(0, 0);
# if THREE_D
	glutCreateWindow ("Model Transformations in 3D");
# else
	glutCreateWindow ("Model Transformations in 2D");
# endif
	glutDisplayFunc(display);
	glutMouseFunc(processMouse);
	glutMotionFunc(processMouseMotion);
	glutPassiveMotionFunc(processPassiveMotion);
	glutSpecialFunc(processSpecialKey);
	glutKeyboardFunc(processKey);
	glutReshapeFunc(reshape);
	glutTimerFunc(1000/60, timerFunc, 0);

	// Set up the target submenu - puzzles for students to solve
	glob.menuTarget = glutCreateMenu(processMenuEntry);
	glutAddMenuEntry("0: None", MENU_TARGET_NONE);
	glutAddMenuEntry("1: Easiest", MENU_TARGET_1);
	glutAddMenuEntry("2: Easy", MENU_TARGET_2);
	glutAddMenuEntry("3: Easy", MENU_TARGET_3);
	glutAddMenuEntry("4: Easy", MENU_TARGET_4);
	glutAddMenuEntry("5: Moderate", MENU_TARGET_5);
	glutAddMenuEntry("6: Moderate", MENU_TARGET_6);
	glutAddMenuEntry("7: Moderate", MENU_TARGET_7);
	glutAddMenuEntry("8: Difficult", MENU_TARGET_8);
	glutAddMenuEntry("9: Difficult", MENU_TARGET_9);
# if 0
	// The very difficult puzzles are not enabled in the menu - they are hidden
	glutAddMenuEntry("!: Challenge", MENU_TARGET_11);
	glutAddMenuEntry("@: Challenge", MENU_TARGET_12);
# endif

	// Set up the menu for the world viewport.  It includes items that
	// create transformations.
	glob.menuWorld = glutCreateMenu(processMenuEntry);
	glutAddMenuEntry("+: Zoom in", MENU_ZOOM_IN);
	glutAddMenuEntry("-: Zoom out", MENU_ZOOM_OUT);
# if THREE_D
	glutAddMenuEntry("a: Animate (toggle)", MENU_ANIMATE);
	glutAddMenuEntry("A: Animate oscillation (toggle)", MENU_ANIMATE_OSCILLATE);
	glutAddMenuEntry("   Shadows (toggle)", MENU_SHADOWS);
# endif
	glutAddMenuEntry("t: Translate", MENU_TRANSLATE);
# if THREE_D
	glutAddMenuEntry("x: Rotate about X", MENU_ROTATE_X);
	glutAddMenuEntry("y: Rotate about Y", MENU_ROTATE_Y);
	glutAddMenuEntry("z: Rotate about Z", MENU_ROTATE_Z);
# else
	glutAddMenuEntry("r: Rotate", MENU_ROTATE_Z);
# endif
# if ! THREE_D
	// Shear is not supported in 3D mode.
	glutAddMenuEntry("S: Shear", MENU_SHEAR);
# endif
	glutAddMenuEntry("s: Scale", MENU_SCALE);
	glutAddMenuEntry("m: Model View", MENU_MODELVIEW);
	glutAddMenuEntry("  e: Affine equations", MENU_EQUATION);
	glutAddMenuEntry("  v: Vector equation", MENU_VECTOR);
	glutAddMenuEntry("  h: Homogeneous transformation", MENU_HOMOGENEOUS_EQUATION);
	glutAddMenuEntry("  H: Homogeneous matrix", MENU_HOMOGENEOUS);
	glutAddMenuEntry("  n: Show numbers", MENU_SHOW_NUMBERS);
	glutAddMenuEntry("  p: Show parameters", MENU_SHOW_SYMBOLS);
	glutAddSubMenu("Target", glob.menuTarget);

	// Set up the menu for the transformation panel.
	// It contains operations for moving panel items, deleting them,
	// and adjusting the display of panel items.
	// Note that the keyboard shortcuts work everywhere.
	// The menus are separated, however, because of the inconvenience of having
	// a very large combined menu.
	glob.menuPanel = glutCreateMenu(processMenuEntry);
	glutAddMenuEntry("UP:   Up", MENU_UP);
	glutAddMenuEntry("DOWN: Down", MENU_DOWN);
	glutAddMenuEntry("d:    Default", MENU_RESET);
	glutAddMenuEntry("D:    Default All", MENU_RESET_ALL);
	glutAddMenuEntry("DEL:  Delete", MENU_DELETE);
	glutAddMenuEntry("      Delete All", MENU_DELETE_ALL);
	glutAddMenuEntry("c:    Compact display", MENU_COMPACT);
	glutAddMenuEntry("C:    Expanded display", MENU_EXPANDED);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
	init();
	glutMainLoop();
	return 0;
}
