
/*
 * square.cpp
 * An animated square adapted from Simple Square (Scott McCallum 2014) with contributions from
 *
 * Len Hamey March 2016
 */
#include <GL/glut.h>

struct Globals {
	float centre_x, centre_y;
	float side;
	float speed_x, speed_y;
	int animating;
	int animation_step;
} globals;

void init(void)
{
	globals.centre_x = 100;
	globals.centre_y = 73;
	// Animation speed in pixels per second
	// Numbers that do not divide the size of the window make for
	// more interesting bounce patterns.
	float speed_factor = 2.0;
	globals.speed_x = speed_factor*197.0;
	globals.speed_y = speed_factor*113.0;
	globals.side = 10;
	globals.animating = 1;
	// Animation time step in msec. 300 frames per second.
	globals.animation_step = 1000/300;
	/* Define world window and set up window-to-viewport transformation */
	glMatrixMode(GL_PROJECTION); // Set the projection matrix
	glLoadIdentity();            // Initialise to identity matrix
	gluOrtho2D(0.0, 600.0, 0.0, 600.0); // Set to orthographic projection of window
}

void display(void)
{
	/* clear entire screen window to background colour (black) */
	glClear(GL_COLOR_BUFFER_BIT);

	/* draw white filled square centered at centre_x, centre_y
	*/
	glRectf(globals.centre_x-globals.side/2, globals.centre_y-globals.side/2,
		   globals.centre_x+globals.side/2, globals.centre_y+globals.side/2);

	/* flush buffer */
	glFlush();

	/* glutSwapBuffers must be called for any drawing to
	 * appear on the screen when in double buffered mode
	 */
	glutSwapBuffers();
}

float bounce(float x, float min, float max, float &speed)
{
	if (x < min) {
		x = min + (min - x);
		speed = -speed;
	}
	if (x > max) {
		x = max + (max - x);
		speed = -speed;
	}
	return x;
}

// Keyboard options to change parameters
void keyboard(unsigned char key, int x, int y)
{
	// Animation speed. 1:10fps  2:20fps  3:30fps 4:60fps  5:120fps  6:300fps
	if (key == '1') globals.animation_step = 1000/10;
	if (key == '2') globals.animation_step = 1000/20;
	if (key == '3') globals.animation_step = 1000/30;
	if (key == '4') globals.animation_step = 1000/60;
	if (key == '5') globals.animation_step = 1000/120;
	if (key == '6') globals.animation_step = 1000/300;
}

// The parameter v is the time (in msec) of the last
void timer(int v)
{
	// Computing elapsed time for smooth animation.
	// The elapsed time is in msec since the call of glutInit.
	// Since it is an integer, the timer is limited.  A 32-bit integer
	// limits the timer to roughly 2 000 000 000 seconds which is 555 hours.
	// This code would exhibit a glitch if the code ran for longer than
	// roughly 555 hours.
	int time = glutGet(GLUT_ELAPSED_TIME); // In msec
	// Set up next timer event
	glutTimerFunc(globals.animation_step, timer, time);
	if (globals.animating) {
		int delta_t = time - v;
		globals.centre_x += globals.speed_x * delta_t / 1000.0;
		globals.centre_y += globals.speed_y * delta_t / 1000.0;
		globals.centre_x = bounce(globals.centre_x, 0.0 + globals.side/2, 600.0 - globals.side/2, globals.speed_x);
		globals.centre_y = bounce(globals.centre_y, 0.0 + globals.side/2, 600.0 - globals.side/2, globals.speed_y);

		glutPostRedisplay();
	}
}

/*
 * Declare initial window size and position.
 * Open window with "hello" in its title bar.
 * Register callback function to display graphics.
 * Call init, then enter main loop and wait for termination.
 */
int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(600, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow ("Bouncing Square");
	glutDisplayFunc(display);
	glutTimerFunc(globals.animation_step, timer, 0);
	glutKeyboardFunc(keyboard);
	init();
	glutMainLoop();
	return 0;
}
