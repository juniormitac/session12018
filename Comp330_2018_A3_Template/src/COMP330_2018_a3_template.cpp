/*
COMP330 - 2018

Assignment 3 Template

Author: Peter Reeves

Date: 27 April 2018

Purpose: To help you get started with Assignment 3. Please see the Assignment 3 description (spec) document for a full
description of the requirements. We strongly recommend that you use this code template as a starting point for your work.
But it is not meant to constrain your efforts in any way, and you do not have to use it if you prefer not to do so.

Textures:
	- Stars:         http://forums.newtek.com/showthread.php?90366-Real-3D-Stars
	- Saturn Planet: https://www.solarsystemscope.com/textures/
	- Saturn Rings:  https://alpha-element.deviantart.com/art/Stock-Image-Saturn-Rings-393767006
	- Iapetus:       https://www.jpl.nasa.gov/spaceimages/details.php?id=PIA18436
	- Titan:         http://planet-texture-maps.wikia.com/wiki/Titan
	- Rhea:          http://libroesoterico.com/biblioteca/Astrologia/-%20SOFTWARE/Celestia%20Portable/App/Celestia/textures/hires/
	- Dione:         https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/Dione_PIA08413_moon_only.JPG/1024px-Dione_PIA08413_moon_only.JPG
	- Tethys:        https://www.jpl.nasa.gov/spaceimages/details.php?id=PIA14931
	- Enceladus:     http://www.unmannedspaceflight.com/lofiversion/index.php/t5575.html
	- Mimas:         https://saturn.jpl.nasa.gov/resources/7689/mimas-global-map-june-2017/
*/

#include <iostream>
#include <stdlib.h>
#include <string>
#include <stdio.h>
#include <fstream>
#include <time.h>
#include <vector>
#include <GL/glut.h>
#include <GL/glu.h>
#include <math.h>
#include "Soil.h"
#include "shapes.h"
#include "utilities.h"


const int WINDOW_INITIAL_WIDTH = 1024;
const int WINDOW_INITIAL_HEIGHT = 768;

const float HORIZONTAL_DIVISION = 0.3f;

const float FRAMES_PER_SECOND = 60.0f;
const int FRAME_MILLI_WAIT = (int) (1000.0f / FRAMES_PER_SECOND);

int windowWidth = WINDOW_INITIAL_WIDTH;
int windowHeight = WINDOW_INITIAL_HEIGHT;

int day = 0; // this creates a daily rotation using a 360 day year.


Planetoid saturn;

Planetoid iapetus;


Planetoid mimas;
Planetoid enceladus;
Planetoid tethys;
Planetoid dione;
Planetoid rhea;
Planetoid titan;

void initialize () {
	// Saturn
	saturn.texture= loadGLTexture("textures/saturn.jpg");
	saturn.radius = 1.0f;

	// Iapetus
	iapetus.orbitAngle = -0.4;
	iapetus.texture = loadGLTexture("textures/iapetus.jpg");
	iapetus.radius = 0.15f;
	iapetus.orbit_radius = 4.5f;

	//Mimas

	mimas.orbitAngle = 0.1;
	mimas.texture = loadGLTexture("textures/mimas.jpg");
	mimas.radius = 0.05f;
	mimas.orbit_radius = 2.0f;

	//Enceladus

	enceladus.orbitAngle = -0.9;
	enceladus.texture = loadGLTexture("textures/enceladus.jpg");
	enceladus.radius = 0.07f;
	enceladus.orbit_radius = 2.5f;

	//Tethys

	tethys.orbitAngle = 2.3;
	tethys.texture = loadGLTexture("textures/tethys.jpg");
	tethys.radius = 0.1f;
	tethys.orbit_radius = 2.5f;

	//Dione

	dione.orbitAngle = 0.2;
	dione.texture = loadGLTexture("textures/dione.jpg");
	dione.radius = 0.12f;
	dione.orbit_radius = 3.0f;

	//Rhea

	rhea.orbitAngle = 3.8;
	rhea.texture = loadGLTexture("textures/rhea.jpg");
	rhea.radius = 0.14f;
	rhea.orbit_radius = 3.5f;

	//Titan

	titan.orbitAngle = 1.6;
	titan.texture = loadGLTexture("textures/titan.jpg");
	titan.radius = 0.2f;
	titan.orbit_radius = 4.0f;

	glShadeModel(GL_SMOOTH);
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

	glEnable(GL_SCISSOR_TEST);
	glEnable(GL_NORMALIZE);
}

void setupRealisticView (Rect windowCoordinates) {
	const int viewportWidth = windowCoordinates.topRightX - windowCoordinates.bottomLeftX;
	const int viewportHeight = windowCoordinates.topRightY - windowCoordinates.bottomLeftY;
	glViewport(windowCoordinates.bottomLeftX, windowCoordinates.bottomLeftY, viewportWidth, viewportHeight);

	glScissor(windowCoordinates.bottomLeftX, windowCoordinates.bottomLeftY, viewportWidth, viewportHeight);

	glClearColor(0.1f, 0.0f, 0.1f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0f, (float) viewportWidth / (float) viewportHeight, 0.01f, 100.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(4.0f, 1.0f, 4.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
}

void setupOrbitView (Rect windowCoordinates) {
	const int viewportWidth = windowCoordinates.topRightX - windowCoordinates.bottomLeftX;
	const int viewportHeight = windowCoordinates.topRightY - windowCoordinates.bottomLeftY;
	glViewport(windowCoordinates.bottomLeftX, windowCoordinates.bottomLeftY, viewportWidth, viewportHeight);

	glScissor(windowCoordinates.bottomLeftX, windowCoordinates.bottomLeftY, viewportWidth, viewportHeight);

	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	const float worldWindowHalfWidth = 4.0f;
	const float aspectRatio = (float) viewportHeight / (float) viewportWidth;
	glOrtho(-worldWindowHalfWidth, worldWindowHalfWidth, -worldWindowHalfWidth * aspectRatio, worldWindowHalfWidth * aspectRatio, -1.0f, 1.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

//this sets the image background

void setupBackground(const char *image){

	glBindTexture (GL_TEXTURE_2D, loadGLTexture(image));


};

void update (const float currentTime, const float deltaTime) {
	iapetus.orbitAngle += 0.5f * deltaTime;
	 mimas.orbitAngle += 0.5 * deltaTime;
	 dione.orbitAngle += 0.5 *deltaTime;
	 enceladus.orbitAngle += 0.5 *deltaTime;
	 rhea.orbitAngle += 0.5 *deltaTime;
	 tethys.orbitAngle += 0.5 *deltaTime;
	 titan.orbitAngle += 0.5 *deltaTime;


	//Updates the current time of the day for ration purposes
	day = (day+1) %360;
}

void animateSceneTimer (int lastTime) {
	int time = glutGet(GLUT_ELAPSED_TIME);
	glutTimerFunc(FRAME_MILLI_WAIT, animateSceneTimer, time);
	const float deltaTime = ((float)(time - lastTime)) / 1000.0f;
	update(time, deltaTime);
	glutPostRedisplay();
}

void display () {
	const int dividerHeight = windowHeight * HORIZONTAL_DIVISION;
	const int firstThird = (float) windowWidth * (1.0f / 3.0f);
	const int secondThird = (float) windowWidth * (2.0f / 3.0f);

	Rect bottomRightViewport;
	bottomRightViewport.bottomLeftX = secondThird;
	bottomRightViewport.bottomLeftY = 0;
	bottomRightViewport.topRightX = windowWidth;
	bottomRightViewport.topRightY = dividerHeight;

	Rect topViewport;
	topViewport.bottomLeftX = 0;
	topViewport.bottomLeftY = dividerHeight;
	topViewport.topRightX = windowWidth;
	topViewport.topRightY = windowHeight;

	const Vector3 iapetusPosition = { iapetus.orbit_radius
			* cosf(iapetus.orbitAngle), 0.0f, iapetus.orbit_radius
			* sinf(iapetus.orbitAngle), };

	const Vector3 mimasPosition = { mimas.orbit_radius * cosf(mimas.orbitAngle),
			0.0f, mimas.orbit_radius * sinf(mimas.orbitAngle), };

	const Vector3 dionePosition = { dione.orbit_radius * cosf(dione.orbitAngle),
			0.0f, dione.orbit_radius * sinf(dione.orbitAngle), };

	const Vector3 enceladusPosition = { enceladus.orbit_radius
			* cosf(enceladus.orbitAngle), 0.0f, enceladus.orbit_radius
			* sinf(enceladus.orbitAngle), };

	const Vector3 rheaPosition = { rhea.orbit_radius * cosf(rhea.orbitAngle),
			0.0f, rhea.orbit_radius * sinf(rhea.orbitAngle), };

	const Vector3 tethysPosition = { tethys.orbit_radius
			* cosf(tethys.orbitAngle), 0.0f, tethys.orbit_radius
			* sinf(tethys.orbitAngle), };

	const Vector3 titanPosition = { titan.orbit_radius * cosf(titan.orbitAngle),
			0.0f, titan.orbit_radius * sinf(titan.orbitAngle), };



	// Realistic
	{
		setupRealisticView(topViewport);

		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glEnable(GL_LIGHTING);
		glEnable(GL_TEXTURE_2D);

		// Sunlight
		glEnable(GL_LIGHT0);
		const float sunPosition [4] = { 0.0f, 2.0f, 0.0f, 1.0f };
		glLightfv(GL_LIGHT0, GL_POSITION, sunPosition);
		const float sunAmbient [4] = { 0.0f, 0.0f, 0.0f, 1.0f };
		glLightfv(GL_LIGHT0, GL_AMBIENT, sunAmbient);
		const float sunDiffuse [4] = { 1.0f, 1.0f, 0.9f, 1.0f };
		glLightfv(GL_LIGHT0, GL_DIFFUSE, sunDiffuse);

		glColor3f(1.0f, 1.0f, 1.0f);

		// Saturn
		glPushMatrix();
			glRotatef((GLfloat)day , 0.0, 1.0, 0.0);
			glScalef(saturn.radius, saturn.radius, saturn.radius);
			glBindTexture(GL_TEXTURE_2D, saturn.texture);
			setMaterial(1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 100.0f, 0.0f, 0.0f, 0.0f);
			drawSphere();
		glPopMatrix();

		// Iapetus
		glPushMatrix();
			glTranslatef(iapetusPosition.x, iapetusPosition.y, iapetusPosition.z);
			glRotatef((GLfloat)day , 0.0, 1.0, 0.0);
			glScalef(iapetus.radius, iapetus.radius, iapetus.radius);
			glBindTexture(GL_TEXTURE_2D, iapetus.texture);
			setMaterial(1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 100.0f, 0.0f, 0.0f, 0.0f);
			drawSphere();
		glPopMatrix();

		//Mimas
		glPushMatrix();
			glTranslatef(mimasPosition.x, mimasPosition.y, mimasPosition.z);
			glRotatef((GLfloat)day , 0.0, 1.0, 0.0);
			glScalef(mimas.radius, mimas.radius, mimas.radius);
			glBindTexture(GL_TEXTURE_2D, mimas.texture);
			setMaterial(1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 100.0f, 0.0f, 0.0f, 0.0f);
			drawSphere();
		glPopMatrix();

		//Enceladus
		glPushMatrix();
		glTranslatef(enceladusPosition.x, enceladusPosition.y, enceladusPosition.z);
		glRotatef((GLfloat) day, 0.0, 1.0, 0.0);
		glScalef(enceladus.radius, enceladus.radius, enceladus.radius);
		glBindTexture(GL_TEXTURE_2D, enceladus.texture);
		setMaterial(1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 100.0f, 0.0f, 0.0f,
				0.0f);
		drawSphere();
		glPopMatrix();

		//Tehys
		glPushMatrix();
		glTranslatef(tethysPosition.x, tethysPosition.y, tethysPosition.z);
		glRotatef((GLfloat) day, 0.0, 1.0, 0.0);
		glScalef(tethys.radius, tethys.radius, tethys.radius);
		glBindTexture(GL_TEXTURE_2D, tethys.texture);
		setMaterial(1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 100.0f, 0.0f, 0.0f,
				0.0f);
		drawSphere();
		glPopMatrix();

		//Dione
		glPushMatrix();
		glTranslatef(dionePosition.x, dionePosition.y, dionePosition.z);
		glRotatef((GLfloat) day, 0.0, 1.0, 0.0);
		glScalef(dione.radius, dione.radius, dione.radius);
		glBindTexture(GL_TEXTURE_2D, dione.texture);
		setMaterial(1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 100.0f, 0.0f, 0.0f,
				0.0f);
		drawSphere();
		glPopMatrix();

		//Rhea
		glPushMatrix();
		glTranslatef(rheaPosition.x, rheaPosition.y, rheaPosition.z);
		glRotatef((GLfloat) day, 0.0, 1.0, 0.0);
		glScalef(rhea.radius, rhea.radius, rhea.radius);
		glBindTexture(GL_TEXTURE_2D, rhea.texture);
		setMaterial(1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 100.0f, 0.0f, 0.0f,
				0.0f);
		drawSphere();
		glPopMatrix();

		//Titan
		glPushMatrix();
		glTranslatef(titanPosition.x, titanPosition.y, titanPosition.z);
		glRotatef((GLfloat) day, 0.0, 1.0, 0.0);
		glScalef(titan.radius, titan.radius, titan.radius);
		glBindTexture(GL_TEXTURE_2D, titan.texture);
		setMaterial(1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 100.0f, 0.0f, 0.0f,
				0.0f);
		drawSphere();
		glPopMatrix();

	}

	// Orbit
	{
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
		glDisable(GL_LIGHTING);
		glDisable(GL_TEXTURE_2D);

		setupOrbitView(bottomRightViewport);

		glPushMatrix();
			// Vertical flip mapping XZ plane to xy screen coordinates.
			glScalef(1.0f, -1.0f, 1.0f);

			glColor3f(1.0f, 1.0f, 1.0f);

		// Saturn Planet
		glPushMatrix();
		glScalef(saturn.radius, saturn.radius, saturn.radius);
		drawCircle();
		drawString("Saturn");
		glPopMatrix();

		//iapetus
		glPushMatrix();
		glTranslatef(iapetusPosition.x, iapetusPosition.z, 0.0f);
		glScalef(iapetus.radius, iapetus.radius, iapetus.radius);
		drawCircle();
		drawString("Iapetus");
		glPopMatrix();

		//Mimas
		glPushMatrix();
		glTranslatef(mimasPosition.x, mimasPosition.z, 0.0f);
		glScalef(mimas.radius, mimas.radius, mimas.radius);
		drawCircle();
		drawString("Mimas");
		glPopMatrix();

		//Enceladus
		glPushMatrix();
		glTranslatef(enceladusPosition.x, enceladusPosition.z, 0.0f);
		glScalef(enceladus.radius, iapetus.radius, iapetus.radius);
		drawCircle();
		drawString("Iapetus");
		glPopMatrix();

		//Thethys
		glPushMatrix();
		glTranslatef(iapetusPosition.x, iapetusPosition.z, 0.0f);
		glScalef(iapetus.radius, iapetus.radius, iapetus.radius);
		drawCircle();
		drawString("Iapetus");
		glPopMatrix();

		//Dione
		glPushMatrix();
		glTranslatef(iapetusPosition.x, iapetusPosition.z, 0.0f);
		glScalef(iapetus.radius, iapetus.radius, iapetus.radius);
		drawCircle();
		drawString("Iapetus");
		glPopMatrix();

		//Rhea
		glPushMatrix();
		glTranslatef(iapetusPosition.x, iapetusPosition.z, 0.0f);
		glScalef(iapetus.radius, iapetus.radius, iapetus.radius);
		drawCircle();
		drawString("Iapetus");
		glPopMatrix();

		//Titan
		glPushMatrix();
		glTranslatef(iapetusPosition.x, iapetusPosition.z, 0.0f);
		glScalef(iapetus.radius, iapetus.radius, iapetus.radius);
		drawCircle();
		drawString("Iapetus");
		glPopMatrix();

		glPopMatrix();


	}

	glutSwapBuffers();
}

void keyboard (unsigned char key, int x, int y) {
	switch (key) {
		case '1': {

		} break;

		case 27: {
			exit(0);
		} break;
	}
}

void special_input (int key, int x, int y) {
	switch (key)	{
		case GLUT_KEY_UP: {

		} break;

		case GLUT_KEY_F1: {

		} break;
	}
}

void resize (int width, int height) {
	windowWidth = width;
	windowHeight = height;
}


void onMouseClick(int button, int state, int x, int y){};

int main (int argc, char** argv) {
	// GLUT setup
	glutInit(&argc, argv);
	glClearColor(1.0, 1.0, 1.0, 0.0);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(windowWidth, windowHeight);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("Mission Control");
	glutDisplayFunc(display);
	glutSpecialFunc(special_input);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(onMouseClick);
	glutReshapeFunc(resize);
	initialize();
	animateSceneTimer(FRAME_MILLI_WAIT);
	glutMainLoop();
	return 0;
}
