// Sierpinski.cpp
// A program to display a Sierpinski gasket

// Scott McCallum 2012

#include <GL/glut.h>
#include <stdlib.h>

struct IntPoint
{
	int x,y;
};

// performs some initializations
void init(void)
{
	// sets background color and drawing color
	glClearColor(1.0, 1.0, 1.0, 0.0);
	glColor3f(0.0, 0.0, 0.0);

   // define appropriate world window
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   gluOrtho2D(0.0, 640.0, 0.0, 480.0);
}

// returns pseudo-random integer in range 0 .. m-1
int random(int m)
{
	return rand() % m;
}

// draws dot at point (x,y)
void drawDot(int x, int y)
{
	glBegin(GL_POINTS);
	  glVertex2i(x,y);
	glEnd();
}

// the Sierpinski gasket function
void Sierpinski(void)
{
	IntPoint T[3] = {{10,10}, {300,30}, {200,300}};
	int i;
	int index = random(3);
	IntPoint point = T[index];
	drawDot(point.x, point.y);
	for (i = 0; i < 20000; i++)
	{
		index = random(3);
		point.x = (point.x + T[index].x)/2;
		point.y = (point.y + T[index].y)/2;
		drawDot(point.x, point.y);
	}
}

// the display callback function
void display(void)
{
   // clears entire screen window to background colour (black)
   glClear(GL_COLOR_BUFFER_BIT);

   Sierpinski();   // call Sierpinski


// flushes buffer
   glFlush();
}

// Declares initial window size and position.
// Opens window with "Sierpinski" in its title bar.
// Registers callback function for display event.
// Enters main loop.

int main(int argc, char** argv)
{
   glutInit(&argc, argv);
   glutInitWindowSize(640, 480);
   glutInitWindowPosition(100, 150);
   glutCreateWindow ("Sierpinski gasket");
   glutDisplayFunc(display);
   init();
   glutMainLoop();
   return 0;
}
