// Robot Arm Demo Program
// Author: Scott McCallum
// Date: April 2009 (Doc added April 2012)
// Based on: Robot Arm program from Red Book, Example 3-7

// An articulated robot arm with two segments is displayed.
// The left segment is the upper arm; the right segment is the lower arm.
// The pivot point (joint) at left of the upper arm is the shoulder;
// the pivot point (joint) at left of the lower arm is the elbow.

// The user can rotate the arm at the shoulder by pressing 's' for
// positive rotation, and 'S' for negative rotation.
// The user can rotate the lower arm at the elbow by pressing 'e' for
// positive rotation, and 'E' for negative rotation.

// The code is more structured than that in Example 3-7 of Red Book.

#include <ctype.h>
#include <GL/glut.h>

static int shoulder = 0, elbow = 0;

void init(void)
{
        glMatrixMode (GL_PROJECTION);
        glLoadIdentity ();
        glOrtho(-5.0, 5.0, -5.0, 5.0, -1.0, 1.0);
        glMatrixMode (GL_MODELVIEW);
        glLoadIdentity ();

}

void drawSegment(void)
// draws a single segment (the basic component of the robot arm)
// which is an aligned rectangular prism (2.0 by 0.4 by 1.0)
// with its leftmost face centred at the origin
{
		// ****************************************
		// Position and scale cube to draw arm segment with one end at origin
		glPushMatrix();
          glTranslatef(1.0, 0.0, 0.0);
          glScalef(2.0, 0.4, 1.0);
          glutWireCube(1.0);
        glPopMatrix();
}

void drawArm(void)
// draws the arm in "standard position",
// that is, the upper arm is in standard position
// (aligned with coord axes and with its leftmost face centred at the origin)
// the lower arm is rotated about the "elbow" through elbow degrees
{
		// ****************************************
		// Draw upper arm aligned with axes
        drawSegment();  // upper arm

        // ****************************************
        // Translate origin to end of upper arm
        // and add rotation of elbow joint
        glPushMatrix();
          glTranslatef(2.0, 0.0, 0.0);
          glRotatef((GLfloat) elbow, 0.0, 0.0, 1.0);
          drawSegment();  // lower arm
        glPopMatrix();
}

void display(void)
// clears the screen and draws the arm rotated about
// the "shoulder" through shoulder degrees
{
        glClear (GL_COLOR_BUFFER_BIT);

        // ***************************************
        // Rotate about origin for shoulder rotation
        // First save the original modelview matrix
        glPushMatrix();
          glRotatef((GLfloat) shoulder, 0.0, 0.0, 1.0);
          drawArm();
        glPopMatrix();

        /* glutSwapBuffers must be called for any drawing to
         * appear on the screen when in double buffered mode
         */
        glutSwapBuffers();
}

void keyboard (unsigned char key, int x, int y)
{
        switch (key) {
        case 's':   /*  s key rotates at shoulder  */
                shoulder = (shoulder + 5) % 360;
                glutPostRedisplay();
                break;
        case 'S':
                shoulder = (shoulder - 5) % 360;
                glutPostRedisplay();
                break;
        case 'e':  /*  e key rotates at elbow  */
                elbow = (elbow + 5) % 360;
                glutPostRedisplay();
                break;
        case 'E':
                elbow = (elbow - 5) % 360;
                glutPostRedisplay();
                break;
        default:
                break;
        }
}

int main(int argc, char** argv)
{
        glutInit(&argc, argv);
        //specify double buffering
        glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);
        glutInitWindowSize (500, 500);
        glutInitWindowPosition (0, 0);
        glutCreateWindow ("Robot Arm");
        init ();
        glutDisplayFunc(display);
        glutKeyboardFunc(keyboard);
        glutMainLoop();
        return 0;
}
