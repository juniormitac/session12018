<HTML>
    <HEAD>
      <TITLE>OpenGL Programming Guide (Addison-Wesley Publishing Company)</TITLE>
    </HEAD>
    <BODY BGCOLOR=#F9F9F9>
      <HR><H1>Appendix C<BR> OpenGL and Window Systems</h1><p>

<p>

OpenGL is available on many different platforms and works with many different window systems. OpenGL is designed to complement window systems, not duplicate their functionality. Therefore, OpenGL performs geometric and image rendering in two and three dimensions, but it does not manage windows or handle input events.<p>

However, the basic definitions of most window systems don't support a library as sophisticated as OpenGL, with its complex and diverse pixel formats, including depth, stencil, and accumulation buffers, as well as double-buffering. For most window systems, some routines are added to extend the window system to support OpenGL.<p>

This appendix introduces the extensions defined for several window and operating systems: the X Window System, the Apple Mac OS, OS/2 Warp from IBM, and Microsoft Windows NT and Windows 95. You need to have some knowledge of the window systems to fully understand this appendix. <p>

This appendix has the following major sections:<ul><li>"GLX: OpenGL Extension for the X Window System"<br><br><li>"AGL: OpenGL Extension to the Apple Macintosh"<br><br><li>"PGL: OpenGL Extension for IBM OS/2 Warp"<br><br><li>"WGL: OpenGL Extension for Microsoft Windows NT and Windows 95"</ul><HR><h2>GLX: OpenGL Extension for the X Window System</h2><p>

In the X Window System, OpenGL rendering is made available as an extension to X in the formal X sense. GLX is an extension to the X protocol (and its associated API) for communicating OpenGL commands to an extended X server. Connection and authentication are accomplished with the normal X mechanisms.<p>

As with other X extensions, there is a defined network protocol for OpenGL's rendering commands encapsulated within the X byte stream, so client-server OpenGL rendering is supported. Since performance is critical in three-dimensional rendering, the OpenGL extension to X allows OpenGL to bypass the X server's involvement in data encoding, copying, and interpretation and instead render directly to the graphics pipeline.<p>

The X Visual is the key data structure to maintain pixel format information about the OpenGL window. A variable of data type XVisualInfo keeps track of pixel information, including pixel type (RGBA or color index), single or double-buffering, resolution of colors, and presence of depth, stencil, and accumulation buffers. The standard X Visuals (for example, PseudoColor, TrueColor) do not describe the pixel format details, so each implementation must extend the number of X Visuals supported.<p>

The GLX routines are discussed in more detail in the <i>OpenGL Reference Manual</i>. Integrating OpenGL applications with the X Window System and the Motif widget set is discussed in great detail in <i>OpenGL Programming for the X Window System </i>by  Mark Kilgard (Reading, MA: Addison-Wesley Developers Press, 1996), which includes full source code examples. If you absolutely want to learn about the internals of GLX, you may want to read the GLX specification, which can be found at<PRE>ftp://sgigate.sgi.com/pub/opengl/doc/</PRE><h3>Initialization</h3><p>

Use <b>glXQueryExtension()</b> and <b>glXQueryVersion()</b> to determine whether the GLX extension is defined for an X server and, if so, which version is present. <b>glXQueryExtensionsString()</b> returns extension information about the client-server connection. <b>glXGetClientString()</b> returns information about the client library, including extensions and version number. <b>glXQueryServerString()</b> returns similar information about the server.<p>

<b>glXChooseVisual()</b> returns a pointer to an XVisualInfo structure describing the visual that meets the client's specified attributes. You can query a visual about its support of a particular OpenGL attribute with <b>glXGetConfig()</b>.<h3>Controlling Rendering</h3><p>

Several GLX routines are provided for creating and managing an OpenGL rendering context. You can use such a context to render off-screen if you want. Routines are also provided for such tasks as synchronizing execution between the X and OpenGL streams, swapping front and back buffers, and using an X font.<h4>Managing an OpenGL Rendering Context</h4><p>

An OpenGL rendering context is created with <b>glXCreateContext()</b>. One of the arguments to this routine allows you to request a direct rendering context that bypasses the X server as described previously. (Note that to do direct rendering, the X server connection must be local, and the OpenGL implementation needs to support direct rendering.) <b>glXCreateContext()</b> also allows display-list and texture-object indices and definitions to be shared by multiple rendering contexts. You can determine whether a GLX context is direct with <b>glXIsDirect()</b>.<b></b><p>

To make a rendering context current, use <b>glXMakeCurrent()</b>; <b>glXGetCurrentContext()</b> returns the current context. You can also obtain the current drawable with <b>glXGetCurrentDrawable()</b> and the current X Display with <b>glXGetCurrentDisplay()</b>. Remember that only one context can be current for any thread at any one time. If you have multiple contexts, you can copy selected groups of OpenGL state variables from one context to another with <b>glXCopyContext()</b>. When you're finished with a particular context, destroy it with <b>glXDestroyContext()</b>.<h4>Off-Screen Rendering</h4><p>

To render off-screen, first create an X Pixmap and then pass this as an argument to <b>glXCreateGLXPixmap()</b>. Once rendering is completed, <br>you can destroy the association between the X and GLX Pixmaps with <b>glXDestroyGLXPixmap()</b>. (Off-screen rendering isn't guaranteed to be supported for direct renderers.) <h4>Synchronizing Execution</h4><p>

To prevent X requests from executing until any outstanding OpenGL rendering is completed, call <b>glXWaitGL()</b>. Then, any previously issued OpenGL commands are guaranteed to be executed before any X rendering calls made after <b>glXWaitGL()</b>. Although the same result can be achieved with <b>glFinish()</b>, <b>glXWaitGL()</b> doesn't require a round trip to the server and thus is more efficient in cases where the client and server are on separate machines.<p>

To prevent an OpenGL command sequence from executing until any outstanding X requests are completed, use <b>glXWaitX()</b>. This routine guarantees that previously issued X rendering calls are executed before any OpenGL calls made after <b>glXWaitX()</b>.<h4>Swapping Buffers</h4><p>

For drawables that are double-buffered, the front and back buffers can be exchanged by calling <b>glXSwapBuffers()</b>. An implicit <b>glFlush()</b> is done as part of this routine. <h4>Using an X Font</h4><p>

A shortcut for using X fonts in OpenGL is provided with the command <b>glXUseXFont()</b>. This routine builds display lists, each of which calls <b>glBitmap()</b>, for each requested character from the specified font and font size.<h3>GLX Prototypes</h3><h4>Initialization</h4><p>

Determine whether the GLX extension is defined on the X server:<p>

       Bool <b>glXQueryExtension</b> ( Display *<var>dpy</var>, int *<var>errorBase</var>, int *<var>eventBase </var>);<p>

Query version and extension information for client and server:<p>

       Bool <b>glXQueryVersion</b> ( Display *<var>dpy</var>, int *<var>major</var>, int *<var>minor </var>); <p>

       const char* <b>glXGetClientString</b> ( Display *<var>dpy</var>, int <var>name </var>);<p>

       const char* <b>glXQueryServerString</b> ( Display *<var>dpy</var>, int <var>screen</var>, int <var>name </var>);<p>

       const char* <b>glXQueryExtensionsString</b> ( Display *<var>dpy</var>, int <var>screen </var>);<p>

Obtain the desired visual:<p>

       XVisualInfo* <b>glXChooseVisual</b> ( Display *<var>dpy</var>, int <var>screen</var>, <br>       int *<var>attribList </var>);<p>

       int <b>glXGetConfig</b> ( Display *<var>dpy</var>, XVisualInfo *<var>visual</var>, int <var>attrib</var>, <br>       int *<var>value </var>); <h4>Controlling Rendering</h4><p>

Manage or query an OpenGL rendering context:<p>

       GLXContext <b>glXCreateContext</b> ( Display <var>*dpy</var>, XVisualInfo <var>*visual</var>,<br>       GLXContext <var>shareList</var>, Bool <var>direct </var>);<p>

       void <b>glXDestroyContext</b> ( Display <var>*dpy</var>, GLXContext <var>context </var>);<p>

       void <b>glXCopyContext</b> ( Display *<var>dpy</var>, GLXContext <var>source</var>,  <br>       GLXContext <var>dest</var>, unsigned long <var>mask </var>);<p>

       Bool <b>glXIsDirect</b> ( Display *<var>dpy</var>, GLXContext <var>context </var>);<p>

       Bool <b>glXMakeCurrent</b> ( Display *<var>dpy</var>, GLXDrawable <var>draw</var>, <br>       GLXContext <var>context </var>); <p>

       GLXContext <b>glXGetCurrentContext</b> (void);<p>

       Display* <b>glXGetCurrentDisplay</b> (void);<p>

       GLXDrawable <b>glXGetCurrentDrawable</b> (void);<p>

       Perform off-screen rendering:<p>

       GLXPixmap <b>glXCreateGLXPixmap</b> ( Display *<var>dpy</var>, XVisualInfo *<var>visual</var>,<br>       Pixmap <var>pixmap </var>);<p>

       void <b>glXDestroyGLXPixmap</b> ( Display *<var>dpy</var>, GLXPixmap <var>pix </var>); <p>

Synchronize execution:<p>

       void <b>glXWaitGL</b> (void);<p>

       void <b>glXWaitX</b> (void); <p>

Exchange front and back buffers:<p>

       void <b>glXSwapBuffers</b> ( Display *<var>dpy</var>, GLXDrawable <var>drawable </var>);<p>

Use an X font:<p>

       void <b>glXUseXFont</b> ( Font <var>font</var>, int <var>first</var>, int <var>count</var>, int <var>listBase </var>);<HR><h2>AGL: OpenGL Extension to the Apple Macintosh</h2><p>

This section covers the routines defined as the OpenGL extension to the Apple Macintosh (AGL), as defined by Template Graphics Software. An understanding of the way the Macintosh handles graphics rendering (QuickDraw) is required. The <i>Macintosh Toolbox Essentials</i> and <i>Imaging With QuickDraw</i> manuals from the <i>Inside Macintosh</i> series are also useful to have at hand.<p>

For more information (including how to obtain the OpenGL software library for the Power Macintosh), you may want to check out the web site for OpenGL information at Template Graphics Software:<PRE>http://www.sd.tgs.com/Products/opengl.htm</PRE><p>

For the Macintosh, OpenGL rendering is made available as a library that is either compiled in or resident as an extension for an application that wishes to make use of it. OpenGL is implemented in software for systems that do not possess hardware acceleration. Where acceleration is available (through the QuickDraw 3D Accelerator), those capabilities that match the OpenGL pipeline are used with the remaining functionality being provided through software rendering.<p>

The data type AGLPixelFmtID (the AGL equivalent to XVisualInfo) maintains pixel information, including pixel type (RGBA or color index), single- or double-buffering, resolution of colors, and presence of depth, stencil, and accumulation buffers.<p>

In contrast to other OpenGL implementations on other systems (such as the X Window System), the client/server model is not used. However, you may still need to call <b>glFlush()</b> since some hardware accelerators buffer the OpenGL pipeline and require a flush to empty it.<h3>Initialization</h3><p>

Use <b>aglQueryVersion()</b> to determine what version of OpenGL for the Macintosh is available.<p>

The capabilities of underlying graphics devices and your requirements for rendering buffers are resolved using <b>aglChoosePixelFmt()</b>. Use <b>aglListPixelFmts()</b> to find the particular formats supported by a graphics device. Given a pixel format, you can determine which attributes are available by using <b>aglGetConfig()</b>.<h3>Rendering and Contexts</h3><p>

Several AGL routines are provided for creating and managing an OpenGL rendering context. You can use such a context to render into either a window or an off-screen graphics world. Routines are also provided that allow you to swap front and back rendering buffers, adjust buffers in response to a move, resize or graphics device change event, and use Macintosh fonts. For software rendering (and in some cases, hardware-accelerated rendering) the rendering buffers are created in your application memory space. For the application to work properly you must provide sufficient memory for these buffers in your application's SIZE resource.<h4>Managing an OpenGL Rendering Context</h4><p>

An OpenGL rendering context is created (at least one context per window being rendered into) with <b>aglCreateContext()</b>. This takes the pixel format you selected as a parameter and uses it to initialize the context.<p>

Use <b>aglMakeCurrent()</b> to make a rendering context current. Only one context can be current for a thread of control at any time. This indicates which drawable is to be rendered into and which context to use with it. It's possible for more than one context to be used (not simultaneously) with a particular drawable. Two routines allow you to determine which is the current rendering context and drawable being rendered into: <b>aglGetCurrentContext()</b> and <b>aglGetCurrentDrawable()</b>.<p>

If you have multiple contexts, you can copy selected groups of OpenGL state variables from one context to another with <b>aglCopyContext()</b>. When a particular context is finished with, it should be destroyed by calling <b>aglDestroyContext()</b>.<h4>On-screen Rendering</h4><p>

With the OpenGL extensions for the Apple Macintosh you can choose whether window clipping is performed when writing to the screen and whether the cursor is hidden during screen writing operations. This is important since these two items may affect how fast rendering can be performed. Call <b>aglSetOptions()</b> to select these options.<h4>Off-screen Rendering</h4><p>

To render off-screen, first create an off-screen graphics world in the usual way, and pass the handle into <b>aglCreateAGLPixmap()</b>. This routine returns a drawable that can be used with <b>aglMakeCurrent()</b>. Once rendering is completed, you can destroy the association with <b>aglDestroyAGLPixmap()</b>.<h4>Swapping Buffers</h4><p>

For drawables that are double-buffered (as per the pixel format of the current rendering context), call <b>aglSwapBuffers()</b> to exchange the front and back buffers. An implicit <b>glFlush()</b> is performed as part of this routine.<h4>Updating the Rendering Buffers</h4><p>

The Apple Macintosh toolbox requires you to perform your own event handling and does not provide a way for libraries to automatically hook in to the event stream. So that the drawables maintained by OpenGL can adjust to changes in drawable size, position and pixel depth, <b>aglUpdateCurrent()</b> is provided.<p>

This routine must be called by your event processing code whenever one of these events occurs in the current drawable. Ideally the scene should be rerendered after a update call to take into account the changes made to the rendering buffers.<h4>Using an Apple Macintosh Font</h4><p>

A shortcut for using Macintosh fonts is provided with <b>aglUseFont()</b>. This routine builds display lists, each of which calls <b>glBitmap()</b>, for each requested character from the specified font and font size.<h3>Error Handling</h3><p>

An error-handling mechanism is provided for the Apple Macintosh OpenGL extension. When an error occurs you can call <b>aglGetError()</b> to get a more precise description of what caused the error.<h3>AGL Prototypes</h3><h4>Initialization</h4><p>

Determine AGL version:         <p>

       GLboolean <b>aglQueryVersion </b>( int *<var>major</var>, int *<var>minor</var> );<p>

Pixel format selection, availability, and capability:         <p>

       AGLPixelFmtID <b>aglChoosePixelFmt </b>( GDHandle *<var>dev</var>, int <var>ndev</var>,<br>       int *<var>attribs</var> );<p>

       int <b>aglListPixelFmts </b>( GDHandle <var>dev</var>, AGLPixelFmtID **<var>fmts</var> );<p>

       GLboolean <b>aglGetConfig </b>( AGLPixelFmtID <var>pix</var>, int <var>attrib</var>, int *<var>value</var> );<h4>Controlling Rendering</h4><p>

Manage an OpenGL rendering context:<p>

       AGLContext <b>aglCreateContext </b>( AGLPixelFmtID <var>pix</var>, <br>       AGLContext <var>shareList</var> );<p>

       GLboolean <b>aglDestroyContext </b>( AGLContext <var>context</var> );<p>

       GLboolean <b>aglCopyContext </b>( AGLContext <var>source</var>, AGLContext <var>dest</var>,<br>       GLuint <var>mask</var> );<p>

       GLboolean <b>aglMakeCurrent </b>( AGLDrawable <var>drawable</var>, <br>       AGLContext <var>context</var> );         <p>

       GLboolean <b>aglSetOptions </b>( int <var>opts</var> );<p>

       AGLContext <b>aglGetCurrentContext </b>(void);<p>

       AGLDrawable <b>aglGetCurrentDrawable </b>(void);<p>

Perform off-screen rendering:<p>

       AGLPixmap <b>aglCreateAGLPixmap </b>( AGLPixelFmtID <var>pix</var>, <br>       GWorldPtr <var>pixmap</var> );<p>

       GLboolean <b>aglDestroyAGLPixmap </b>( AGLPixmap <var>pix</var> );<p>

Exchange front and back buffers:<p>

       GLboolean <b>aglSwapBuffers </b>( AGLDrawable <var>drawable</var> );<p>

Update the current rendering buffers:<p>

       GLboolean <b>aglUpdateCurrent </b>(void);<p>

Use a Macintosh font:<p>

       GLboolean <b>aglUseFont </b>( int <var>familyID</var>, int <var>size</var>, int <var>first</var>, int <var>count</var>, <br>       int <var>listBase</var> );<p>

Find the cause of an error:   <p>

       GLenum <b>aglGetError </b>(void); <HR><h2>PGL: OpenGL Extension for IBM OS/2 Warp</h2><p>

OpenGL rendering for IBM OS/2 Warp is accomplished by using PGL routines added to integrate OpenGL into the standard IBM Presentation Manager. OpenGL with PGL supports both a direct OpenGL context (which is often faster) and an indirect context (which allows some integration of Gpi and OpenGL rendering).<p>

The data type VISUALCONFIG (the PGL equivalent to XVisualInfo) maintains the visual configuration, including pixel type (RGBA or color index), single- or double-buffering, resolution of colors, and presence of depth, stencil, and accumulation buffers.<p>

To get more information (including how to obtain the OpenGL software library for IBM OS/2 Warp, Version 3.0), you may want to start at<PRE>http://www.austin.ibm.com/software/OpenGL/</PRE><p>

Packaged along with the software is the document, <i>OpenGL On OS/2 Warp</i>, which provides more detailed information. OpenGL support is included with the base operating system with OS/2 Warp Version 4.<h3>Initialization</h3><p>

Use <b>pglQueryCapability()</b> and <b>pglQueryVersion()</b> to determine whether the OpenGL is supported on this machine and, if so, how it is supported and which version is present. <b>pglChooseConfig()</b> returns a pointer to an VISUALCONFIG structure describing the visual configuration that best meets the client's specified attributes. A list of the particular visual configurations supported by a graphics device can be found using <b>pglQueryConfigs()</b>.<h3>Controlling Rendering</h3><p>

Several PGL routines are provided for creating and managing an OpenGL rendering context, capturing the contents of a bitmap, synchronizing execution between the Presentation Manager and OpenGL streams, swapping front and back buffers, using a color palette, and using an OS/2 logical font.<h4>Managing an OpenGL Rendering Context</h4><p>

An OpenGL rendering context is created with <b>pglCreateContext()</b>. One of the arguments to this routine allows you to request a direct rendering context that bypasses the Gpi and render to a PM window, which is generally faster. You can determine whether a OpenGL context is direct with <b>pglIsIndirect()</b>.<b></b><p>

To make a rendering context current, use <b>pglMakeCurrent()</b>; <b>pglGetCurrentContext()</b> returns the current context. You can also obtain the current window with <b>pglGetCurrentWindow()</b>. You can copy some OpenGL state variables from one context to another with <b>pglCopyContext()</b>. When you're finished with a particular context, destroy it with <b>pglDestroyContext()</b>.<h4>Access the Bitmap of the Front Buffer</h4><p>

To lock access to the bitmap representation of the contents of the front buffer, use <b>pglGrabFrontBitmap()</b>. An implicit <b>glFlush()</b> is performed, and you can read the bitmap, but its contents are effectively read-only. Immediately after access is completed, you should call <b>pglReleaseFrontBitmap()</b> to restore write access to the front buffer.<h4>Synchronizing Execution</h4><p>

To prevent Gpi rendering requests from executing until any outstanding OpenGL rendering is completed, call <b>pglWaitGL()</b>. Then, any previously issued OpenGL commands are guaranteed to be executed before any Gpi rendering calls made after <b>pglWaitGL()</b>.<b></b><p>

To prevent an OpenGL command sequence from executing until any outstanding Gpi requests are completed, use <b>pglWaitPM()</b>. This routine guarantees that previously issued Gpi rendering calls are executed before any OpenGL calls made after <b>pglWaitPM()</b>.<p>

<B>Note: </B>OpenGL and Gpi rendering can be integrated in the same window only if the OpenGL context is an indirect context.<h4>Swapping Buffers</h4><p>

For windows that are double-buffered, the front and back buffers can be exchanged by calling <b>pglSwapBuffers()</b>. An implicit <b>glFlush()</b> is done as part of this routine.<h4>Using a Color Index Palette</h4><h4>When you are running in 8-bit (256 color) mode, you have to worry about color palette management. For windows with a color index Visual Configuration, call <b></b><b>pglSelectColorIndexPalette()</b> to tell OpenGL what color-index palette you want to use with your context. A color palette must be selected before the context is initially bound to a window. In RGBA mode, OpenGL sets up a palette automatically.</h4><h4>Using an OS/2 Logical Font</h4><p>

A shortcut for using OS/2 logical fonts in OpenGL is provided with the command <b>pglUseFont()</b>. This routine builds display lists, each of which calls <b>glBitmap()</b>, for each requested character from the specified font and font size.<h3>PGL Prototypes</h3><h4>Initialization</h4><p>

Determine whether OpenGL is supported and, if so, its version number:<p>

       long <b>pglQueryCapability</b> (HAB <var>hab</var>);<p>

       void <b>pglQueryVersion</b> (HAB <var>hab</var>, int *<var>major</var>, int *<var>minor</var>); <p>

Visual configuration selection, availability and capability:<p>

       PVISUALCONFIG <b>pglChooseConfig</b> (HAB <var>hab</var>, int *<var>attribList</var>);<p>

       PVISUALCONFIG * <b>pglQueryConfigs</b> (HAB <var>hab</var>);<h4>Controlling Rendering</h4><p>

Manage or query an OpenGL rendering context:<p>

       HGC <b>pglCreateContext</b> (HAB <var>hab</var>, PVISUALCONFIG <var>pVisualConfig</var>,<br>       HGC <var>shareList</var>, Bool isD<var>irect</var>);<p>

       Bool <b>pglDestroyContext</b> (HAB <var>hab</var>, HGC <var>hgc</var>);<p>

       Bool <b>pglCopyContext</b> (HAB <var>hab</var>, HGC <var>source</var>, HGC <var>dest</var>, GLuint <var>mask</var>);<p>

       Bool <b>pglMakeCurrent</b> (HAB <var>hab</var>, HGC <var>hgc</var>, HWND <var>hwnd</var>);<p>

       long <b>pglIsIndirect</b> (HAB <var>hab</var>, HGC <var>hgc</var>);<p>

       HGC <b>pglGetCurrentContext</b> (HAB <var>hab</var>);<p>

       HWND <b>pglGetCurrentWindow</b> (HAB <var>hab</var>);<p>

Access and release the bitmap of the front buffer:<p>

       Bool <b>pglGrabFrontBitmap</b> (HAB <var>hab</var>, HPS *<var>hps</var>, HBITMAP *<var>phbitmap</var>);<p>

       Bool <b>pglReleaseFrontBitmap</b> (HAB <var>hab</var>); <p>

Synchronize execution:<p>

       HPS <b>pglWaitGL</b> (HAB <var>hab</var>);<p>

       void <b>pglWaitPM</b> (HAB <var>hab</var>); <p>

Exchange front and back buffers:<p>

       void <b>pglSwapBuffers</b> (HAB <var>hab</var>, HWND <var>hwnd</var>); <p>

Finding a color-index palette:<p>

       void <b>pglSelectColorIndexPalette</b> (HAB <var>hab</var>, HPAL, <var>hpal</var>, HGC <var>hgc</var>); <p>

Use an OS/2 logical font:<p>

       Bool <b>pglUseFont</b> (HAB <var>hab</var>, HPS <var>hps</var>, FATTRS <var>*fontAttribs</var>, <br>       long <var>logicalId</var>, int <var>first</var>, int <var>count</var>, int <var>listBase</var>); <HR><h2>WGL: OpenGL Extension for Microsoft Windows NT and Windows 95</h2><p>

OpenGL rendering is supported on systems that run Microsoft Windows NT and Windows 95. The functions and routines of the Win32 library are necessary to initialize the pixel format and control rendering for OpenGL. Some routines, which are prefixed by <b>wgl</b>, extend Win32 so that OpenGL can be fully supported.<p>

For Win32/WGL, the PIXELFORMATDESCRIPTOR is the key data structure to maintain pixel format information about the OpenGL window. A variable of data type PIXELFORMATDESCRIPTOR keeps track of pixel information, including pixel type (RGBA or color index), single- or double- buffering, resolution of colors, and presence of depth, stencil, and accumulation buffers.<p>

To get more information about WGL, you may want to start with technical articles available through the Microsoft Developer Network at<PRE>http://www.microsoft.com/msdn/</PRE><h3>Initialization</h3><p>

Use <b>GetVersion()</b> or the newer <b>GetVersionEx()</b> to determine version information. <b>ChoosePixelFormat()</b> tries to find a PIXELFORMATDESCRIPTOR with specified attributes. If a good match for the requested pixel format is found, then <b>SetPixelFormat()</b> should be called to actually use the pixel format. You should select a pixel format in the device context before calling <b>wglCreateContext()</b>.<p>

If you want to find out details about a given pixel format, use <b>DescribePixelFormat()</b> or, for overlays or underlays, <b>wglDescribeLayerPlane()</b>. <h3>Controlling Rendering</h3><p>

Several WGL routines are provided for creating and managing an OpenGL rendering context, rendering to a bitmap, swapping front and back buffers, finding a color palette, and using either bitmap or outline fonts.<h4>Managing an OpenGL Rendering Context</h4><p>

<b>wglCreateContext()</b> creates an OpenGL rendering context for drawing on the device in the selected pixel format of the device context. (To create an OpenGL rendering context for overlay or underlay windows, use <b>wglCreateLayerContext()</b> instead.) To make a rendering context current, use <b>wglMakeCurrent()</b>; <b>wglGetCurrentContext()</b> returns the current context. You can also obtain the current device context with <b>wglGetCurrentDC()</b>. You can copy some OpenGL state variables from one context to another with <b>wglCopyContext()</b> or make two contexts share the same display lists and texture objects with <b>wglShareLists()</b>. When you're finished with a particular context, destroy it with <b>wglDestroyContext()</b>.<h4>OpenGL Rendering to a Bitmap</h4><p>

Win32 has a few routines to allocate (and deallocate) bitmaps, to which you can render OpenGL directly. <b>CreateDIBitmap()</b> creates a device-dependent bitmap (DDB) from a device-independent bitmap (DIB). <b>CreateDIBSection()</b> creates a device-independent bitmap (DIB) that applications can write to directly. When finished with your bitmap, you can use <b>DeleteObject()</b> to free it up.<h4>Synchronizing Execution</h4><p>

If you want to combine GDI and OpenGL rendering, be aware there are no equivalents to functions like <b>glXWaitGL()</b>, <b>glXWaitX()</b>, or <b>pglWaitGL()</b> in Win32. Although <b>glXWaitGL()</b> has no equivalent in Win32, you can achieve the same effect by calling <b>glFinish(),</b> which waits until all pending OpenGL commands are executed, or by calling <b>GdiFlush()</b>, which waits until all GDI drawing has completed.<h4>Swapping Buffers</h4><p>

For windows that are double-buffered, the front and back buffers can be exchanged by calling <b>SwapBuffers()</b> or <b>wglSwapLayerBuffers()</b>; the latter for overlays and underlays.<h4>Finding a Color Palette</h4><p>

To access the color palette for the standard (non-layer) bitplanes, use the standard GDI functions to set the palette entries. For overlay or underlay layers, use <b>wglRealizeLayerPalette()</b>, which maps palette entries from a given color-index layer plane into the physical palette or initializes the palette of an RGBA layer plane. <b>wglGetLayerPaletteEntries()</b> is used to query the entries in palettes of layer planes.<h4>Using a Bitmap or Outline Font</h4><p>

WGL has two routines, <b>wglUseFontBitmaps()</b> and <b>wglUseFontOutlines()</b>, for converting system fonts to use with OpenGL. Both routines build a display list for each requested character from the specified font and font size.<h3>WGL Prototypes</h3><h4>Initialization</h4><p>

Determine version information:<p>

       BOOL <b>GetVersion</b> ( LPOSVERSIONINFO <var>lpVersionInformation</var> );<p>

       BOOL <b>GetVersionEx</b> ( LPOSVERSIONINFO <var>lpVersionInformation</var> );<p>

Pixel format availability, selection, and capability:<p>

       int <b>ChoosePixelFormat</b> ( HDC  <var>hdc</var>,<br>       CONST PIXELFORMATDESCRIPTOR * <var>ppfd</var> );<p>

       BOOL <b>SetPixelFormat</b> ( HDC  <var>hdc</var>, int <var>iPixelFormat</var>,<br>       CONST PIXELFORMATDESCRIPTOR * <var>ppfd</var> );<p>

       int <b>DescribePixelFormat</b> ( HDC  <var>hdc</var>, int <var>iPixelFormat</var>, UINT <var>nBytes</var>,<br>       LPPIXELFORMATDESCRIPTOR  <var>ppfd</var> );<p>

       BOOL <b>wglDescribeLayerPlane</b> ( HDC <var>hdc</var>, int <var>iPixelFormat</var>,<br>       int <var>iLayerPlane</var>, UINT <var>nBytes</var>, LPLAYERPLANEDESCRIPTOR <var>plpd</var> );<h4>Controlling Rendering</h4><p>

Manage or query an OpenGL rendering context:<p>

       HGLRC <b>wglCreateContext</b> ( HDC  <var>hdc</var> );<p>

       HGLRC <b>wglCreateLayerContext</b> ( HDC  <var>hdc</var>, int <var>iLayerPlane</var> );<p>

       BOOL <b>wglShareLists</b> ( HGLRC  <var>hglrc1</var>, HGLRC  <var>hglrc2</var> );<p>

       BOOL <b>wglDeleteContext</b> ( HGLRC  <var>hglrc</var> );<p>

       BOOL <b>wglCopyContext</b> ( HGLRC  <var>hglrcSource</var>, HGLRC  <var>hlglrcDest</var>,<br>       UINT  <var>mask</var> );<p>

       BOOL <b>wglMakeCurrent</b> ( HDC  <var>hdc</var>, HGLRC  <var>hglrc</var> );<p>

       HGLRC <b>wglGetCurrentContext</b> (VOID) ;<p>

       HDC <b>wglGetCurrentDC</b> (VOID);<p>

Access and release the bitmap of the front buffer:<p>

       HBITMAP <b>CreateDIBitmap</b> ( HDC <var>hdc</var>, <br>       CONST BITMAPINFOHEADER *<var>lpbmih</var>, DWORD <var>fdwInit</var>, <br>       CONST VOID *<var>lpbInit</var>, CONST BITMAPINFO *<var>lpbmi</var>, UINT <var>fuUsage</var> );<p>

       HBITMAP <b>CreateDIBSection</b> ( HDC <var>hdc</var>, CONST BITMAPINFO *<var>pbmi</var>, <br>       UINT <var>iUsage</var>, VOID *<var>ppvBits</var>, HANDLE <var>hSection</var>, DWORD <var>dwOffset</var> );<p>

       BOOL <b>DeleteObject</b> ( HGDIOBJ <var>hObject</var> );<p>

Exchange front and back buffers:<p>

       BOOL <b>SwapBuffers</b> ( HDC  <var>hdc</var> );<p>

       BOOL <b>wglSwapLayerBuffers</b> ( HDC  <var>hdc</var>, UINT  <var>fuPlanes</var> );<p>

Finding a color palette for overlay or underlay layers:<p>

       int <b>wglGetLayerPaletteEntries</b> ( HDC  <var>hdc</var>, int <var>iLayerPlane</var>, int <var>iStart</var>,<br>       int <var>cEntries</var>, CONST COLORREF *<var>pcr</var> );<p>

       BOOL <b>wglRealizeLayerPalette</b> ( HDC  <var>hdc</var>, int <var>iLayerPlane</var>,<br>       BOOL <var>bRealize</var> );<p>

Use a bitmap or an outline font:<p>

       BOOL <b>wglUseFontBitmaps</b> ( HDC <var>hdc</var>, DWORD <var>first</var>, DWORD <var>count</var>,<br>       DWORD <var>listBase</var> );<p>

       BOOL <b>wglUseFontOutlines</b> ( HDC <var>hdc</var>, DWORD <var>first</var>, DWORD <var>count</var>,<br>       DWORD <var>listBase</var>, FLOAT <var>deviation</var>, FLOAT <var>extrusion</var>, int <var>format</var>,<br>       LPGLYPHMETRICSFLOAT <var>lpgmf</var> );
      
      <HR>

    </BODY>
  </HTML>