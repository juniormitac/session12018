<HTML>
    <HEAD>
      <TITLE>OpenGL Programming Guide (Addison-Wesley Publishing Company)</TITLE>
    </HEAD>
    <BODY BGCOLOR=#F9F9F9>
      <HR><H1>Appendix E<BR> Calculating Normal Vectors</h1><p>

This appendix describes how to calculate normal vectors for surfaces. You need to define normals to use the OpenGL lighting facility, which is described in <A HREF="ch5.html">Chapter 5</A>. <A HREF="ch2.html">"Normal Vectors" in Chapter 2</A> introduces normals and the OpenGL command for specifying them. This appendix goes through the details of calculating them. It has the following major sections:<ul><li>"Finding Normals for Analytic Surfaces"<br><br><li>"Finding Normals from Polygonal Data"</ul><p>

Since normals are perpendicular to a surface, you can find the normal at a particular point on a surface by first finding the flat plane that just touches the surface at that point. The normal is the vector that's perpendicular to that plane. On a perfect sphere, for example, the normal at a point on the surface is in the same direction as the vector from the center of the sphere to that point. For other types of surfaces, there are other, better means for determining the normals, depending on how the surface is specified. <p>

Recall that smooth curved surfaces are approximated by a large number of small flat polygons. If the vectors perpendicular to these polygons are used as the surface normals in such an approximation, the surface appears faceted, since the normal direction is discontinuous across the polygonal boundaries. In many cases, however, an exact mathematical description exists for the surface, and true surface normals can be calculated at every point. Using the true normals improves the rendering considerably, as shown in Figure E-1. Even if you don't have a mathematical description, you can do better than the faceted look shown in the figure. The two major sections in this appendix describe how to calculate normal vectors for these two cases:<ul><li>"Finding Normals for Analytic Surfaces" explains what to do when you have a mathematical description of a surface.<br><br><li>"Finding Normals from Polygonal Data" covers the case when you have only the polygonal data to describe a surface. <p>

<p>

<IMG SRC="figures/polytrue.gif" ALT="polytrue.gif" BORDER=0 ALIGN=absmiddle><BR><p>

<B>Figure E-1 :  </B>Rendering with Polygonal Normals vs. True Normals<p>

<br></ul><HR><h2>Finding Normals for Analytic Surfaces</h2><p>

Analytic surfaces are smooth, differentiable surfaces that are described by a mathematical equation (or set of equations). In many cases, the easiest surfaces to find normals for are analytic surfaces for which you have an explicit definition in the following form: <p>

<b>V</b>(<var>s,t</var>) = [ <b>X</b>(<var>s,t</var>)  <b>Y</b>(<var>s,t</var>)  <b>Z</b>(<var>s,t</var>) ]<p>

where <i>s</i> and <i>t</i> are constrained to be in some domain, and <b>X</b>, <b>Y</b>, and <b>Z</b> are differentiable functions of two variables. To calculate the normal, find<p>

<p>

<IMG SRC="figures/eqapf01.gif" ALT="eqapf01.gif" BORDER=0 ALIGN=absmiddle><BR><p>

<p>

which are vectors tangent to the surface in the <i>s</i> and <i>t</i> directions. The cross product<p>

<p>

<IMG SRC="figures/eqapf02.gif" ALT="eqapf02.gif" BORDER=0 ALIGN=absmiddle><BR><p>

is perpendicular to both and, hence, to the surface. The following shows how to calculate the cross product of two vectors. (Watch out for the degenerate cases where the cross product has zero length!) <p>

<p>

<IMG SRC="figures/eqapf03.gif" ALT="eqapf03.gif" BORDER=0 ALIGN=absmiddle><BR><p>

You should probably normalize the resulting vector. To normalize a vector [x y z], calculate its length<p>

<p>

<IMG SRC="figures/eqapf04.gif" ALT="eqapf04.gif" BORDER=0 ALIGN=absmiddle><BR><p>

and divide each component of the vector by the length.<p>

As an example of these calculations, consider the analytic surface<p>

<b>V</b>(<var>s,t</var>) = [ <var>s</var>2<var> t</var>3 3-<var>st</var> ]<p>

From this we have<p>

<p>

<IMG SRC="figures/eqapf05.gif" ALT="eqapf05.gif" BORDER=0 ALIGN=absmiddle><BR><p>

So, for example, when<var> s</var>=1 and<var> t</var>=2, the corresponding point on the surface is (1, 8, 1), and the vector (-24, 2, 24) is perpendicular to the surface at that point. The length of this vector is 34, so the unit normal vector is (-24/34, 2/34, 24/34) = (-0.70588, 0.058823, 0.70588).<p>

For analytic surfaces that are described implicitly, as <b>F</b>(<var>x, y, z</var>) = 0, the problem is harder. In some cases, you can solve for one of the variables, say <var>z</var> = <b>G</b>(<var>x, y</var>), and put it in the explicit form given previously:<p>

<p>

<IMG SRC="figures/eqapf06.gif" ALT="eqapf06.gif" BORDER=0 ALIGN=absmiddle><BR><p>

Then continue as described earlier. <p>

If you can't get the surface equation in an explicit form, you might be able to make use of the fact that the normal vector is given by the gradient<p>

<p>

<IMG SRC="figures/eqapf07.gif" ALT="eqapf07.gif" BORDER=0 ALIGN=absmiddle><BR><p>

<p>

evaluated at a particular point (<var>x, y, z</var>). Calculating the gradient might be easy, but finding a point that lies on the surface can be difficult. As an example of an implicitly defined analytic function, consider the equation of a sphere of radius 1 centered at the origin:<p>

x2 + y2 + z2 - 1 = 0 )<p>

This means that <p>

<b>F</b> (x, y, z) = x2 + y2 + z2 - 1<p>

which can be solved for<var> z</var> to yield<p>

<p>

<IMG SRC="figures/eqapf08.gif" ALT="eqapf08.gif" BORDER=0 ALIGN=absmiddle><BR><p>

Thus, normals can be calculated from the explicit form<p>

<p>

<IMG SRC="figures/eqapf09.gif" ALT="eqapf09.gif" BORDER=0 ALIGN=absmiddle><BR><p>

as described previously.<p>

If you could not solve for <var>z</var>, you could have used the gradient<p>

<p>

<IMG SRC="figures/eqapf10.gif" ALT="eqapf10.gif" BORDER=0 ALIGN=absmiddle><BR><p>

as long as you could find a point on the surface. In this case, it's not so hard to find a point   -       for example, (2/3, 1/3, 2/3) lies on the surface. Using the gradient, the normal at this point is (4/3, 2/3, 4/3). The unit-length normal is (2/3, 1/3, 2/3), which is the same as the point on the surface, as expected.<HR><h2>Finding Normals from Polygonal Data</h2><p>

As mentioned previously, you often want to find normals for surfaces that are described with polygonal data such that the surfaces appear smooth rather than faceted. In most cases, the easiest way for you to do this (though it might not be the most efficient way) is to calculate the normal vectors for each of the polygonal facets and then to average the normals for neighboring facets. Use the averaged normal for the vertex that the neighboring facets have in common. Figure E-2 shows a surface and its polygonal approximation. (Of course, if the polygons represent the exact surface and aren't merely an approximation   -       if you're drawing a cube or a cut diamond, for example   -       don't do the averaging. Calculate the normal for each facet as described in the following paragraphs, and use that same normal for each vertex of the facet.) <p>

<p>

<IMG SRC="figures/normals.gif" ALT="normals.gif" BORDER=0 ALIGN=absmiddle><BR><p>

<B>Figure E-2 :  </B>Averaging Normal Vectors<p>

<br><p>

To find the normal for a flat polygon, take any three vertices <var>v</var>1, <var>v</var>2, and <var>v</var>3 of the polygon that do not lie in a straight line. The cross product <p>

[<var>v</var>1 - <var>v</var>2]    &times;        [<var>v</var>2 - <var>v</var>3]<p>

is perpendicular to the polygon. (Typically, you want to normalize the resulting vector.) Then you need to average the normals for adjoining facets to avoid giving too much weight to one of them. For instance, in the example shown in Figure E-2, if <var>n</var>1, <var>n</var>2, <var>n</var>3, and <var>n</var>4 are the normals for the four polygons meeting at point P, calculate <var>n</var>1+<var>n</var>2+<var>n</var>3+<var>n</var>4 and then normalize it. (You can get a better average if you weight the normals by the size of the angles at the shared intersection.) The resulting vector can be used as the normal for point P.<p>

Sometimes, you need to vary this method for particular situations. For instance, at the boundary of a surface (for example, point Q in Figure E-2), you might be able to choose a better normal based on your knowledge of what the surface should look like. Sometimes the best you can do is to average the polygon normals on the boundary as well. Similarly, some models have some smooth parts and some sharp corners (point R is on such an edge in Figure E-2). In this case, the normals on either side of the crease shouldn't be averaged. Instead, polygons on one side of the crease should be drawn with one normal, and polygons on the other side with another.
      
      <HR>

    </BODY>
  </HTML>