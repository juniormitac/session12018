<HTML>
    <HEAD>
      <TITLE>OpenGL Programming Guide (Addison-Wesley Publishing Company)</TITLE>
    </HEAD>
    <BODY BGCOLOR=#F9F9F9>
      <HR><H1>Appendix F<BR> Homogeneous Coordinates and Transformation Matrices</h1><p>

This appendix presents a brief discussion of homogeneous coordinates. It also lists the form of the transformation matrices used for rotation, scaling, translation, perspective projection, and orthographic projection. These topics are introduced and discussed in <A HREF="ch3.html">Chapter 3</A>. For a more detailed discussion of these subjects, see almost any book on three-dimensional computer graphics   -       for example, <i>Computer Graphics: Principles and Practice</i> by Foley, van Dam, Feiner, and Hughes (Reading, MA: Addison-Wesley, 1990)   -       or a text on projective geometry   -       for example, <i>The Real Projective Plane</i>, by H. S. M. Coxeter, 2nd ed. (Cambridge: Cambridge University Press, 1961). In the discussion that follows, the term homogeneous coordinates always means three-dimensional homogeneous coordinates, although projective geometries exist for all dimensions.<p>

This appendix has the following major sections:<ul><li>"Homogeneous Coordinates"<br><br><li>"Transformation Matrices"</ul><HR><h2>Homogeneous Coordinates</h2><p>

OpenGL commands usually deal with two- and three-dimensional vertices, but in fact all are treated internally as three-dimensional homogeneous vertices comprising four coordinates. Every column vector (<var>x, y, z, w</var>)T represents a homogeneous vertex if at least one of its elements is nonzero. If the real number <var>a</var> is nonzero, then (<var>x, y, z, w</var>)T and (<var>a</var>x,<var> a</var>y,<var> a</var>z,<var> a</var>w)T represent the same homogeneous vertex. (This is just like fractions: <var>x</var>/<var>y</var> = (<var>a</var>x)/(<var>a</var>y).) A three-dimensional euclidean space point (<var>x, y, z</var>)T becomes the homogeneous vertex with coordinates (<var>x, y, z</var>, 1.0)T, and the two-dimensional euclidean point (<var>x, y</var>)T becomes (<var>x, y</var>, 0.0, 1.0)T.<p>

As long as <var>w</var> is nonzero, the homogeneous vertex (<var>x, y, z, w</var>)T corresponds to the three-dimensional point (<var>x/w, y/w, z/w</var>)T. If <var>w</var> = 0.0, it corresponds to no euclidean point, but rather to some idealized "point at infinity." To understand this point at infinity, consider the point (1, 2, 0, 0), and note that the sequence of points (1, 2, 0, 1), (1, 2, 0, 0.01), and (1, 2.0, 0.0, 0.0001), corresponds to the euclidean points (1, 2), (100, 200), and (10000, 20000). This sequence represents points rapidly moving toward infinity along the line 2<var>x</var> = <var>y</var>. Thus, you can think of (1, 2, 0, 0) as the point at infinity in the direction of that line.<p>

<B>Note: </B>OpenGL might not handle homogeneous clip coordinates with <var>w</var> &lt; 0 correctly. To be sure that your code is portable to all OpenGL systems, use only nonnegative <var>w</var> values.<h3>Transforming Vertices</h3><p>

Vertex transformations (such as rotations, translations, scaling, and shearing) and projections (such as perspective and orthographic) can all be represented by applying an appropriate 4   &times;       4 matrix to the coordinates representing the vertex. If <b>v</b> represents a homogeneous vertex and <b>M</b> is a 4   &times;       4 transformation matrix, then <b>Mv</b> is the image of <b>v</b> under the transformation by <b>M</b>. (In computer-graphics applications, the transformations used are usually nonsingular   -       in other words, the matrix <b>M</b> can be inverted. This isn't required, but some problems arise with nonsingular transformations.)<p>

After transformation, all transformed vertices are clipped so that <var>x</var>, <var>y</var>, and <var>z</var> are in the range [-<var>   &ohgr;       </var>, <var>w</var>] (assuming <var>w</var> &gt; 0). Note that this range corresponds in euclidean space to [-1.0, 1.0].<h3>Transforming Normals</h3><p>

Normal vectors aren't transformed in the same way as vertices or position vectors. Mathematically, it's better to think of normal vectors not as vectors, but as planes perpendicular to those vectors. Then, the transformation rules for normal vectors are described by the transformation rules for perpendicular planes.<p>

A homogeneous plane is denoted by the row vector (<var>a, b, c, d</var>), where at least one of <var>a, b, c</var>, or <var>d</var> is nonzero. If <var>q</var> is a nonzero real number, then (<var>a, b, c, d</var>) and (<var>qa, qb, qc, qd</var>) represent the same plane. A point (<var>x, y, z, w</var>)T is on the plane (<var>a, b, c, d</var>) if <var>ax</var>+<var>by</var>+<var>cz</var>+<var>dw</var> = 0. (If <var>w</var> = 1, this is the standard description of a euclidean plane.) In order for (<var>a, b, c, d</var>) to represent a euclidean plane, at least one of <var>a</var>, <var>b</var>, or <var>c</var> must be nonzero. If they're all zero, then (0, 0, 0, <var>d</var>) represents the "plane at infinity," which contains all the "points at infinity."<p>

If <b>p</b> is a homogeneous plane and <b>v</b> is a homogeneous vertex, then the statement "<b>v</b> lies on plane <b>p</b>" is written mathematically as <b>pv</b> = 0, where <b>pv</b> is normal matrix multiplication. If <b>M</b> is a nonsingular vertex transformation (that is, a 4   &times;       4 matrix that has an inverse <b>M</b>-1), then <b>pv</b> = 0 is equivalent to <b>pM</b>-1<b>Mv</b> = 0, so <b>Mv</b> lies on the plane <b>pM</b>-1. Thus, <b>pM</b>-1 is the image of the plane under the vertex transformation <b>M</b>.<p>

If you like to think of normal vectors as vectors instead of as the planes perpendicular to them, let <b>v</b> and <b>n</b> be vectors such that <b>v</b> is perpendicular to <b>n</b>. Then, <b>n</b>T<b>v</b> = 0. Thus, for an arbitrary nonsingular transformation <b>M</b>, <b>n</b>T<b>M</b>-1<b>Mv</b> = 0, which means that nTM-1 is the transpose of the transformed normal vector. Thus, the transformed normal vector is<b> (M</b>-1<b>)</b>T<b>n</b>. In other words, normal vectors are transformed by the inverse transpose of the transformation that transforms points. Whew!<HR><h2>Transformation Matrices</h2><p>

Although any nonsingular matrix <b>M</b> represents a valid projective transformation, a few special matrices are particularly useful. These matrices are listed in the following subsections. <h3>Translation</h3><p>

The call <b>glTranslate*(</b><var>x, y, z</var><b>)</b> generates <b>T</b>, where <p>

<p>

<IMG SRC="figures/eqapg01.gif" ALT="eqapg01.gif" BORDER=0 ALIGN=absmiddle><BR><h3>Scaling</h3><p>

The call <b>glScale*(</b><var>x, y, z</var><b>)</b> generates S, where <p>

<p>

<IMG SRC="figures/eqapg02.gif" ALT="eqapg02.gif" BORDER=0 ALIGN=absmiddle><BR><p>

Notice that S-1 is defined only if <var>x</var>, <var>y</var>, and <var>z</var> are all nonzero.<h3>Rotation</h3><p>

The call <b>glRotate*(</b><var>a, x, y, z</var><b>)</b> generates R as follows: <p>

Let v = (x, y, z)T, and u = v/||v|| = (x', y', z')T. <p>

Also let<p>

<p>

<IMG SRC="figures/eqapg03.gif" ALT="eqapg03.gif" BORDER=0 ALIGN=absmiddle><BR><p>

Then <p>

<p>

<IMG SRC="figures/eqapg04.gif" ALT="eqapg04.gif" BORDER=0 ALIGN=absmiddle><BR><p>

The <b>R</b> matrix is always defined. If <var>x=y=z</var>=0, then <b>R</b> is the identity matrix. You can obtain the inverse of <b>R</b>, <b>R</b><b>-1</b>, by substituting -<var>   &agr;       </var> for <var>a</var>, or by transposition. <p>

The <b>glRotate*()</b> command generates a matrix for rotation about an arbitrary axis. Often, you're rotating about one of the coordinate axes; the corresponding matrices are as follows:<p>

<p>

<IMG SRC="figures/eqapg05.gif" ALT="eqapg05.gif" BORDER=0 ALIGN=absmiddle><BR><p>

As before, the inverses are obtained by transposition. <h3>Perspective Projection</h3><p>

The call <b>glFrustum(</b><var>l, r, b, t, n, f</var><b>)</b> generates <b>R</b>, where <p>

<p>

<IMG SRC="figures/eqapg06.gif" ALT="eqapg06.gif" BORDER=0 ALIGN=absmiddle><BR><p>

<b>R</b> is defined as long as <var>l</var>   &ne;       <var>r</var>, <var>t</var>   &ne;       <var>b</var>, and <var>n</var>   &ne;       <var>f</var>. <h3>Orthographic Projection</h3><p>

The call <b>glOrtho(</b><var>l, r, b, t, n, f</var><b> )</b> generates <b>R</b>, where <p>

<p>

<IMG SRC="figures/eqapg07.gif" ALT="eqapg07.gif" BORDER=0 ALIGN=absmiddle><BR><p>

<b>R</b> is defined as long as <var>l</var>   &ne;       <var>r</var>, <var>t</var>   &ne;       <var>b</var>, and <var>n</var>   &ne;       <var>f</var>. <p>


      
      <HR>

    </BODY>
  </HTML>