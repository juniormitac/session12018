package main;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

public class Main {

	private static String createAlphabet(String input) {

		// Creates the encrypting alphabet WORKS!

		String alphabet = "abcdefghijklmnopqrstuvwxyz";
		String newAlphabet = input.toLowerCase() + alphabet;
		char[] chars = newAlphabet.toCharArray();

		Set<Character> charSet = new LinkedHashSet<Character>();

		for (char c : chars) {
			charSet.add(c);
		}

		StringBuilder sb = new StringBuilder();
		for (Character character : charSet) {
			sb.append(character);
		}

		String output = sb.toString();

		return output;

	}

	private static int alphabetGet(char a) {

		String alphabet = "abcdefghijklmnopqrstuvwxyz";

		int output = alphabet.indexOf(a);

		return output;

	}

	private static String decrypt(String encrypted, String alphabet) {

		// System.out.println(encrypted + " " + alphabet);

		char[] myArray = new char[alphabet.length()];

		// char[] myAlphabet = new char[alphabet.length()];

		char[] output = new char[encrypted.length()];

		for (int i = 0; i < alphabet.length(); i++) {

			myArray[i] = alphabet.charAt(i);

		}

		for (int i = 0; i < encrypted.length(); i++) {
			
			output[i] = alphabet.charAt(alphabetGet(encrypted.charAt(i)));

		}

		String finalOutput = String.copyValueOf(output);

		return finalOutput;

	}

	public static void main(String[] args) {

		boolean isRunning = true;

		ArrayList<String> movies = new ArrayList<>();

		movies.add("gravity");
		movies.add("themonumentsmen");
		movies.add("thebookthief");
		movies.add("dallasbuyersclub");

		String cyphertext = "WHTKHKHTYNVOTVVTPR";

		for (int i = 0; i < movies.size(); i++) {

			System.out.println(createAlphabet(movies.get(i)));
		}
	}

}
