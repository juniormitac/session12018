
/*
 * chaser.cpp
 * An animated square that chases the mouse adapted from
 * AnimatedSquare
 *
 * Len Hamey March 2016
 */
#include <GL/glut.h>
// Math library for sqrt function
#include <math.h>

// Animation time step in msec. 300 frames per second.
# define ANIMATION_STEP (1000/300)

struct Globals {
	float centre_x, centre_y;
	float side;
	float mouse_x, mouse_y, speed;
	int animating;
} globals;

void init(void)
{
	globals.centre_x = 100;
	globals.centre_y = 73;
	// Animation speed in pixels per second
	globals.speed = 500.0;
	globals.side = 10;
	globals.animating = 1;
	/* Define world window and set up window-to-viewport transformation */
	glMatrixMode(GL_PROJECTION); // Set the projection matrix
	glLoadIdentity();            // Initialise to identity matrix
	gluOrtho2D(0.0, 600.0, 0.0, 600.0); // Set to orthographic projection of window
}

void display(void)
{
	/* clear entire screen window to background colour (black) */
	glClear(GL_COLOR_BUFFER_BIT);

	/* draw white filled square centered at centre_x, centre_y
	*/
	glRectf(globals.centre_x-globals.side/2, globals.centre_y-globals.side/2,
		   globals.centre_x+globals.side/2, globals.centre_y+globals.side/2);

	/* flush buffer */
	glFlush();

	/* glutSwapBuffers must be called for any drawing to
	 * appear on the screen when in double buffered mode
	 */
	glutSwapBuffers();
}

float limit(float x, float min, float max)
{
	if (x < min) {
		x = min;
	}
	if (x > max) {
		x = max;
	}
	return x;
}

int nearby(float x1, float y1, float x2, float y2, float distance)
{
	return (x1-x2)*(x1-x2)+(y1-y2)*(y1-y2) < distance*distance;
}

// The parameter v is the time (in msec) of the last call
void timer(int v)
{
	// Computing elapsed time for smooth animation.
	// The elapsed time is in msec since the call of glutInit.
	// Since it is an integer, the timer is limited.  A 32-bit integer
	// limits the timer to roughly 2 000 000 seconds which is 555 hours.
	// This code would exhibit a glitch if the code ran for longer than
	// roughly 555 hours.
	int time = glutGet(GLUT_ELAPSED_TIME); // In msec
	// Set up next timer event
	glutTimerFunc(ANIMATION_STEP, timer, time);
	if (globals.animating) {
		int delta_t = time - v;
		float delta_x, delta_y, length, step_size;
		// Compute vector from current location to mouse
		delta_x = globals.mouse_x - globals.centre_x;
		delta_y = globals.mouse_y - globals.centre_y;
		// Compute length of the vector
		length = sqrt (delta_x*delta_x + delta_y*delta_y);
		// If the square is close to the mouse, then no motion is required.
		// Close is here defined as a little more than half the step size,
		// so that the square will not oscillate back and forth around the mouse
		step_size = globals.speed * delta_t / 1000.0;
		if (length > step_size * 0.55) {
			// Normalise the delta vector and compute the step
			delta_x = delta_x / length * step_size;
			delta_y = delta_y / length * step_size;
			globals.centre_x += delta_x;
			globals.centre_y += delta_y;
			// Keep the square inside the world window.
			globals.centre_x = limit(globals.centre_x, 0.0 + globals.side/2, 600.0 - globals.side/2);
			globals.centre_y = limit(globals.centre_y, 0.0 + globals.side/2, 600.0 - globals.side/2);
		}

		glutPostRedisplay();
	}
}

// The parameters are screen coordinates of the mouse.
// Record them to determine the direction of motion.
void mousemotion(int x, int yc)
{
	globals.mouse_x = x;
	globals.mouse_y = 600 - yc;
}

/*
 * Declare initial window size and position.
 * Open window with "hello" in its title bar.
 * Register callback function to display graphics.
 * Call init, then enter main loop and wait for termination.
 */
int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(600, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow ("Mouse Chaser");
	glutDisplayFunc(display);
	glutTimerFunc(ANIMATION_STEP, timer, 0);
	glutPassiveMotionFunc(mousemotion);
	init();
	glutMainLoop();
	return 0;
}
