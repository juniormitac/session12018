/*
 * SimpleCylinder.h
 *
 * Class for a simple cylinder. :)
 */

#ifndef SIMPLECYLINDER_H_
#define SIMPLECYLINDER_H_

#include "BasicSceneObject.h"

class SimpleCylinder : public BasicSceneObject
{
	public:
		SimpleCylinder();

		SimpleCylinder(int b, int t, int h, int sl, int st);

		int base;
		int top;
		int height;
		int slices;
		int stacks;

	protected:

		virtual void DrawModel();
};



#endif /* SIMPLECYLINDER_H_ */
