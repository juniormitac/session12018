/*
 * Mace.h
 *
 * COMP330 Manual Modeling - 2014
 *
 * This shows a mace being rendered using sweeping planes
 * by a surface of revolution.
 *
 * by Matt Cabanag
 */

#ifndef MACE_H_
#define MACE_H_

#include "BasicSceneObject.h"


class Mace : public BasicSceneObject
{

	public:

		Mace();

		vector<Vector3> outline;

	protected:

		virtual void DrawModel();

		virtual void DrawOutline();
};



#endif /* MACE_H_ */
