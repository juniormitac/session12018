/*
 * QuadSphere.h
 *
 *  Created on: 29/04/2014
 *      Author: AVataRR
 */

#ifndef QUADSPHERE_H_
#define QUADSPHERE_H_

#include "BasicSceneObject.h"

class QuadSphere : public BasicSceneObject
{
	private:
		virtual void DrawModel();
};



#endif /* QUADSPHERE_H_ */
