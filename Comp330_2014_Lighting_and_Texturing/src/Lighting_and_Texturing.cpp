/*
COMP330 - 2014

Lighting and Texturing Demo

This program shows how to do:

1) Basic Lighting
2) Basic Texturing

And is built on top of the Manual Modeling Program.

Matt Cabanag
Tim Lambert
Scott McCallum
Nathan Tarr
+ others...
*/

#include <iostream>
#include <stdlib.h>
#include <string>
#include <stdio.h>
#include <fstream>
#include <time.h>
#include <vector>
#include <GL/glut.h>
#include <GL/glu.h>
#include "Soil.h"
#include "UtilityStructs.h"
#include "Shapes.h"
#include "BasicSceneObject.h"
#include "SpaceCrate.h"
#include "QuadCube.h"
#include "QuadSphere.h"
#include "QuadChain.h"
#include "Mace.h"

using namespace std;

#define X_RES 800
#define Y_RES 600

//scene hierarchy root used for drawing
vector <BasicSceneObject*> SceneRoot;

//intermediate scene data
vector <RawSceneData> MyRawData;

//movement speed variable for general use.
GLfloat moveSpeed = 0.25;

//variables for the main camera's location
GLfloat mainCamX = 3;
GLfloat mainCamY = 3;
GLfloat mainCamZ = 10;

//coordinates for what the camera is looking at
GLfloat mainCamLookX = 0;
GLfloat mainCamLookY = 0;
GLfloat mainCamLookZ = 0;

//variables for the main camera's "up" vector
GLfloat mainCamUpX = 0;
GLfloat mainCamUpY = 1.0;
GLfloat mainCamUpZ = 0;

//variables for the robot arm
Vector3 baseArmRotation;
Vector3 childArmRotation;

//variables for the light source
QuadSphere* myLightSphere;

Vector3 lightPos;
Vector3 lightDir;

Colour4 lightAmbient;
Colour4 lightDiffuse;
Colour4 lightSpecular;

//variables for texturing;
#define MAX_NO_TEXTURES 1
GLuint texture[MAX_NO_TEXTURES];

//Taken from Tim Lambert's "textures" example project, 2013.
int loadGLTexture(const char *fileName)                                    // Load Bitmaps And Convert To Textures
{
	string name = "textures/";
	name += fileName;
	/* load an image file from texture directory as a new OpenGL texture */
	texture[0] = SOIL_load_OGL_texture
			(
					name.c_str(),
					SOIL_LOAD_AUTO,
					SOIL_CREATE_NEW_ID,
					SOIL_FLAG_INVERT_Y
			);

	if(texture[0] == 0) {
		std::cerr << fileName << " : " << SOIL_last_result();
		return false;
	}

	// Typical Texture Generation Using Data From The Bitmap
	glBindTexture(GL_TEXTURE_2D, texture[0]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
	return true;// Return Success
}

// Axes drawing function
// credit: Scott McCallum 2012.
// modified by Matt Cabanag 2014
void axes(void)
{

	glDisable(GL_LIGHTING);
	glDisable(GL_LIGHT0);
	glBegin(GL_LINES);
		  glVertex3f(0.0, 0.0, 0.0);
		  glVertex3f(50.0, 0.0, 0.0); /* draw x-axis */
		  glVertex3f(0.0, 0.0, 0.0);
		  glVertex3f(0.0, 50.0, 0.0); /* draw y-axis */
		  glVertex3f(0.0, 0.0, 0.0);
		  glVertex3f(0.0, 0.0, 50.0); /* draw z-axis */
	glEnd();
}


//sets up the main view
void setupMainView()
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(90, 64/48.0, 0.1, 200);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(mainCamX, mainCamY, mainCamZ,
			  mainCamLookX, mainCamLookY, mainCamLookZ,
			  mainCamUpX, mainCamUpY, mainCamUpZ);

	//setup the depth buffer
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glEnable( GL_DEPTH_TEST );

	//cout<<"LightPos:"<<lightPos.x<<","<<lightPos.y<<","<<lightPos.z<<endl;
	float lp [] = {lightPos.x,lightPos.y,lightPos.z, 0.0};
	glLightfv(GL_LIGHT0, GL_POSITION, lp);

	glEnable(GL_LIGHTING);// Enable lighting calculations
	glEnable(GL_LIGHT0);// Turn on light #0.
}


//THE SCENE DRAWER!
void drawScene()
{

	for(unsigned int i = 0; i < SceneRoot.size(); i++)
	{
		SceneRoot.at(i) -> Display();
	}

}

//THE ANIMATOR!!
//called by the glut idle function.
void animateScene()
{
	for(unsigned int i = 0; i < SceneRoot.size(); i++)
	{
		SceneRoot.at(i) -> Update();
	}
}

void animateSceneTimer(int milis)
{
	animateScene();
	glutTimerFunc(milis, animateSceneTimer, milis);
}

//the actual display callback function
void display(void)
{	//clear the screen!
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	//first update the "logical model"; animate the world in other words
	//animateScene();

	//draw the axis:
	glColor3f(1,1,1);
	axes();

	//draw the main viewport
	setupMainView();

	glViewport(0, 0, X_RES, Y_RES);
	drawScene();

	//OpenGL house cleaning
	glFlush();//put everything on the pipeline
	glutPostRedisplay();//post the ready scenes.
	glutSwapBuffers();//swap the buffers

}

//keyboard handler
void keyboard( unsigned char key, int x, int y )
{
	switch ( key )
	{	//move the view around with WASD
		case 'w':
		{
			mainCamZ -= moveSpeed;
		}break;

		case 's':
		{
			mainCamZ += moveSpeed;
		}break;

		case 'a':
		{
			mainCamX -= moveSpeed;
		}break;

		case 'd':
		{
			mainCamX += moveSpeed;
		}break;

		case 'c':
		{
			mainCamY -= moveSpeed;
		}break;

		case 'e':
		{
			mainCamY += moveSpeed;
		}break;

		//reset everything!
		case 'r':
		{
			mainCamX = 5;
			mainCamY = 5;
			mainCamZ = 10;
		}break;

		case '.':
		{
			lightPos.y += moveSpeed;
		}break;

		case ',':
		{
			lightPos.y -= moveSpeed;
		}break;

		//this is the escape key
		case 27:
		{	exit(1);
		}break;
	}

	myLightSphere -> position = lightPos;

	display();
}


void SpecialInput(int key, int x, int y)
{
	switch(key)
	{
		case GLUT_KEY_UP:
		{
			lightPos.z -= moveSpeed;
		}break;

		case GLUT_KEY_DOWN:
		{
			lightPos.z += moveSpeed;
		}break;

		case GLUT_KEY_LEFT:
		{
			lightPos.x -= moveSpeed;
		}break;

		case GLUT_KEY_RIGHT:
		{
			lightPos.x += moveSpeed;
		}break;

		case GLUT_KEY_F1:
		{

		}break;
	}

	myLightSphere -> position = lightPos;

	display();
}

void LoadDefaultScene()
{
	//if these are the first textures loaded into the program
	loadGLTexture("crate.jpg");//textureID 1
	loadGLTexture("Earth.jpg");//textureID 2

	//Static Scene Objects
	SceneRoot.push_back(new SpaceCrate);
	SceneRoot.push_back(new QuadCube);
	SceneRoot.push_back(new QuadSphere);
	SceneRoot.push_back(new Mace);

	//Crate
	SceneRoot.at(0) -> colour = Colour4(1,1,1,1);
	SceneRoot.at(0) -> textureID = loadGLTexture("crate.jpg");

	//Cube
	SceneRoot.at(1) -> colour = Colour4(1,1,0,1);
	SceneRoot.at(1) -> position = Vector3(3,0,0);
	SceneRoot.at(1) -> textureID = 1;//loadGLTexture("crate.jpg");

	//Sphere
	SceneRoot.at(2) -> colour = Colour4(1,1,1,1);
	SceneRoot.at(2) -> position = Vector3(0,0,7);
	SceneRoot.at(2) -> textureID = 2;//loadGLTexture("crate.jpg");

	//Mace
	SceneRoot.at(3) -> colour = Colour4(1,1,0,1);
	SceneRoot.at(3) -> position = Vector3(0,0,2);

	//The Quad Chain
	//specify the root chain
	QuadChain* chainRoot = new QuadChain;
	chainRoot -> position = Vector3(5,0,0);
	chainRoot -> rpySpeed = Vector3(1,0,0);
	chainRoot -> colour = Colour4(1,0,1,1);

	//specify the first child
	QuadChain* child1 = new QuadChain;
	child1 -> rpySpeed = Vector3(2,0,0);
	child1 -> colour = Colour4(0,1,0,1);

	//specify the second child
	QuadChain* child2 = new QuadChain;
	child2 -> rpySpeed = Vector3(2,0,0);
	child2 -> colour = Colour4(0,0,1,1);

	//link the chains
	child1 -> AddChild(child2);
	chainRoot -> AddChild(child1);
	SceneRoot.push_back(chainRoot);

	//The Light Source Indicator
	myLightSphere = new QuadSphere;
	lightPos = Vector3(5,5,0);
	myLightSphere -> colour = Colour4(1,1,0,1);
	myLightSphere -> scale = Vector3(0.25,0.25,0.25);
	myLightSphere -> position = lightPos;

	SceneRoot.push_back(myLightSphere);
}

int main(int argc, char** argv)
{

	glutInit(&argc, argv);


	glEnable(GL_NORMALIZE);
	glClearColor(1.0, 1.0, 1.0, 0.0);
	//setup double buffering
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

	//set up the window
	glutInitWindowSize(X_RES, Y_RES);
	glutInitWindowPosition(0, 0);
	glutCreateWindow ("Spaceship Viewer");

	//assign the callback functions
	glutDisplayFunc(display);
	glutSpecialFunc(SpecialInput);
	glutKeyboardFunc(keyboard);


	LoadDefaultScene();//load the scene
	animateSceneTimer(20);//set the animation timer

	//GOOOOWWW!!
	glutMainLoop();
	return 0;
}
