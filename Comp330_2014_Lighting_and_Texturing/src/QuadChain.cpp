/*
 * QuadChain.cpp
 *
 * COMP330 Manual Modeling - 2014
 *
 * by Matt Cabanag
 */

#include "QuadChain.h"
#include "QuadCube.h"

QuadChain::QuadChain()
{
	BasicSceneObject();
	child = NULL;
	offset = Vector3(0,0,0.5);

}

void QuadChain::AddChild(QuadChain* newChild)
{
	child = newChild;
}

void QuadChain::Update()
{
	BasicSceneObject::Update();

	//traverse your 'child chain' and update them all.
	if(child != NULL)
		child -> Update();
}

void QuadChain::Display()
{
	BasicSceneObject::Display();
}

void QuadChain::DrawModel()
{
	//display yourself as a scaled QuadCube
	glTranslatef(offset.x,offset.y,offset.z);
	QuadCube myModel = QuadCube();
	myModel.scale = Vector3(0.5,0.5,1.5);
	myModel.colour = colour;
	myModel.Display();

	//but also display your children.
	if(child != NULL)
	{
		glTranslatef(child -> offset.x,child -> offset.y,child -> offset.z);
		glPushMatrix();
			child -> Display();
		glPopMatrix();
	}
}



