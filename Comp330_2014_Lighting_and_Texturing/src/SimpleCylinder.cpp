/*
 * SimpleCylinder.h
 *
 * Class for a simple cylinder. :)
 */

#include "SimpleCylinder.h"

SimpleCylinder::SimpleCylinder()
{
	int base = 1;
	int top = 1;
	int height = 3;
	int slices = 10;
	int stacks = 10;

	SimpleCylinder(base,top,height,slices,stacks);
}

SimpleCylinder::SimpleCylinder(int b, int t, int h, int sl, int st)
{
	base = b;
	top = t;
	height = h;
	slices = sl;
	stacks = st;

	BasicSceneObject();

}

//adapted from: http://stackoverflow.com/questions/20444077/opengl-cylinder
void SimpleCylinder::DrawModel()
{
	//todo:what do we do here?
}



