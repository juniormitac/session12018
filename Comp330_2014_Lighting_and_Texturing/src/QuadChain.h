/*
 * QuadChain.h
 *
 * COMP330 Manual Modeling - 2014
 *
 * by Matt Cabanag
 */

#include "BasicSceneObject.h"

#ifndef QUADCHAIN_H_
#define QUADCHAIN_H_

class QuadChain : public BasicSceneObject
{
	public:

		QuadChain();

		virtual void Update();

		virtual void AddChild(QuadChain*);

		virtual void Display();

		Vector3 offset;

	protected:
		virtual void DrawModel();

		QuadChain* child;
};


#endif /* QUADCHAIN_H_ */
