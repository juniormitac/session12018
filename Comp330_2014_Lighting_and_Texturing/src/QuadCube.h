/*
 * QuadCube.h
 *
 *  Created on: 29/04/2014
 *      Author: AVataRR
 */

#include "BasicSceneObject.h"

#ifndef QUADCUBE_H_
#define QUADCUBE_H_

class QuadCube : public BasicSceneObject
{
	private:

		virtual void DrawModel();
};


#endif /* QUADCUBE_H_ */
