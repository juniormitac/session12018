/*
 * ExtrudedHouse.h
 *
 *  Created on: 29/04/2014
 *      Author: AVataRR
 */

#ifndef EXTRUDEDHOUSE_H_
#define EXTRUDEDHOUSE_H_

#include "BasicSceneObject.h"

class ExtrudedHouse :  public BasicSceneObject
{
	private:
		virtual void DrawModel();
};



#endif /* EXTRUDEDHOUSE_H_ */
