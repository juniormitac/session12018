/*
 * Mace.cpp
 *
 * COMP330 Manual Modeling - 2014
 *
 * This shows a mace being rendered using sweeping planes
 * by a surface of revolution.
 *
 * by Matt Cabanag
 */

#include "Mace.h"

Mace::Mace()
{
	outline.push_back(Vector3(0,5,0));
	outline.push_back(Vector3(0.5,4,0));
	outline.push_back(Vector3(0.1,3,0));
	outline.push_back(Vector3(0.1,2,0));
	outline.push_back(Vector3(0.1,0,0));


	BasicSceneObject();
}

void Mace::DrawModel()
{
	//todo: draw a mace using sweeping planes.

	glColor3f(colour.r,colour.g,colour.b);
	glutSolidCube(0.5);

	for(float i = 0; i < 360; i += 1)
	{
		//glLoadIdentity();

		glPushMatrix();
			glRotatef(i,0,1,0);
			glColor3f(colour.r,colour.g,colour.b);
			DrawOutline();
		glPopMatrix();
	}


}

void Mace::DrawOutline()
{
	glBegin(GL_QUAD_STRIP);
	for(unsigned int i = 0; i < outline.size(); i++)
	{
		glVertex3f(outline.at(i).x, outline.at(i).y, outline.at(i).z);
		glVertex3f(outline.at(i).x, outline.at(i).y, outline.at(i).z - 0.01);

	}
	glEnd();
}



